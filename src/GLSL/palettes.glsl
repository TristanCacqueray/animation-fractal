// The MIT License
// https://www.youtube.com/c/InigoQuilez
// https://iquilezles.org/
// Copyright © 2015 Inigo Quilez

// A simple way to create color variation in a cheap way (yes, trigonometrics ARE cheap
// in the GPU, don't try to be smart and use a triangle wave instead).

// See https://iquilezles.org/articles/palettes for more information

vec3 pal( in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d )
{
    return a + b*cos( 6.28318*(c*t+d) );
}


// A longer palette from https://www.shadertoy.com/view/WtScDt
// Copyright © 2020 Inigo Quilez
// box-filted cos(x)
vec3 fcos( in vec3 x )
{
    vec3 w = fwidth(x);
#if 0
    return cos(x) * sin(0.5*w)/(0.5*w);       // exact
#else
    return cos(x) * smoothstep(6.2832,0.0,w); // approx
#endif
}

vec3 getColor( in float t )
{
    vec3 col = vec3(0.6,0.5,0.4);
    col += 0.14*fcos(6.2832*t*  1.0+vec3(0.0,0.5,0.6));
    col += 0.13*fcos(6.2832*t*  3.1+vec3(0.5,0.6,1.0));
    col += 0.12*fcos(6.2832*t*  5.1+vec3(0.1,0.7,1.1));
    col += 0.11*fcos(6.2832*t*  9.1+vec3(0.1,0.5,1.2));
    col += 0.10*fcos(6.2832*t* 17.1+vec3(0.0,0.3,0.9));
    col += 0.09*fcos(6.2832*t* 31.1+vec3(0.1,0.5,1.3));
    col += 0.08*fcos(6.2832*t* 65.1+vec3(0.1,0.5,1.3));
    col += 0.07*fcos(6.2832*t*131.1+vec3(0.3,0.2,0.8));
    return col;
}
