-- | A 'Variable' defines an animation value:
--  - gives a meaningfull name to Scene values,
--  - provides a controller to modify the value.
module AnimationFractal.Variable (
    Variable (..),
    renderVariable,
    renderVariables,
    lookupVariable,

    -- * Modulation
    OutVar (..),
    newOutVar,
    lookupOutVar,

    -- * Controller
    Controller (..),
    renderToggle,

    -- * history
    HistoryVar (..),
    newHistoryVar,
    renderHistoryVar,
    pushHistoryVar,

    -- * helpers
    tvarState,
    makeStateLens,
) where

import Frelude

import Data.Scientific.Lens
import Data.Sequence qualified as Seq

import DearImGui qualified

data Variable = Variable
    { name :: Text
    , controller :: Controller
    , mods :: [OutVar]
    }

lookupVariable :: Text -> [Variable] -> Maybe Variable
lookupVariable _ [] = Nothing
lookupVariable name (var : xs)
    | name == var.name = Just var
    | otherwise = lookupVariable name xs

-- | A controller defines the variable type.
--  This encoding is not great because it requires some boilerplate to pattern match each type.
--  Though this is the simplest solution. Maybe a fancier implementation would be more ergonomic?
data Controller
    = ControllerSliderFloat (StateVar Float) Float Float
    | ControllerSliderScallar (StateVar Int32) Int Int
    | ControllerFloat (TVar Scientific) (StateVar Float)
    | ControllerToggle (StateVar Bool)
    | ControllerVec2 (StateVar Vec2) (StateVar Float) (StateVar Float)
    | ControllerVec3 (StateVar Vec4) (StateVar Float) (StateVar Float) (StateVar Float) Float Float

-- | We keep 3 copies for each variable:
--  - scene is the final value, pushed to the Worker.Var scene, to be rendered.
--  - buffer is an intermediary value where the modulations happen.
--  - current is the value managed by the user through dear-imgui controller.
data OutVar = OutVar
    { name :: Text
    , hvar :: TVar HistoryVar
    , scene :: StateVar Float
    , buffer :: TVar Float
    , current :: StateVar Float
    }

newOutVar :: Text -> StateVar Float -> StateVar Float -> STM OutVar
newOutVar name current scene = do
    hvar <- newTVar newHistoryVar
    buffer <- newTVar 0
    pure OutVar{..}

lookupOutVar :: Text -> [OutVar] -> Maybe OutVar
lookupOutVar _ [] = Nothing
lookupOutVar name (ovar : xs)
    | name == ovar.name = Just ovar
    | otherwise = lookupOutVar name xs

renderVariable :: (MonadUnliftIO m) => Variable -> m Bool
renderVariable var = do
    changed <- case var.controller of
        ControllerToggle v -> DearImGui.checkbox var.name v
        ControllerFloat value output -> DearImGui.withID var.name $ renderFloatController value output
        ControllerSliderFloat v minv maxv -> DearImGui.sliderFloat var.name v minv maxv
        ControllerSliderScallar v minv maxv -> DearImGui.sliderInt var.name (scallar32Var v) minv maxv
        ControllerVec2 _ x y -> do
            c1 <- DearImGui.sliderFloat (var.name <> ".x") x -7 7
            c2 <- DearImGui.sliderFloat (var.name <> ".y") y -7 7
            pure (c1 || c2)
        ControllerVec3 _ x y z minv maxv -> do
            c1 <- DearImGui.sliderFloat (var.name <> ".x") x minv maxv
            c2 <- DearImGui.sliderFloat (var.name <> ".y") y minv maxv
            c3 <- DearImGui.sliderFloat (var.name <> ".z") z minv maxv
            pure (c1 || c2 || c3)
    traverse_ renderOutVar var.mods
    pure changed
  where
    scallar32Var v = mapStateVar fromIntegral fromIntegral v

makeStateLens :: TVar a -> Lens' a b -> StateVar b
makeStateLens value valueLens = makeStateVar getv setv
  where
    -- getv :: IO b
    getv = view valueLens <$> readTVarIO value
    -- setv :: b -> IO ()
    setv = atomically . modifyTVar' value . set valueLens

renderFloatController :: (MonadUnliftIO m) => TVar Scientific -> StateVar Float -> m Bool
renderFloatController value output = do
    let coefVar = makeStateLens value coefL
        expVar = makeStateLens value exp10L

    changed <- newTVarIO False
    -- We pack all the elements tightly on a single line
    DearImGui.setNextItemWidth 40
    prevExp <- get expVar
    whenM (DearImGui.dragInt "##exp" expVar 0.2 (-38) 38) do
        -- When the exponent value changes, we adjust the coefficient.
        newExp <- get expVar
        let adjust coef
                | -- The exp decrease, so we adjust the coef
                  newExp < prevExp =
                    coef * 10
                | -- The exp increase and the coef can be reduced
                  abs coef >= 10 =
                    coef `div` 10
                | -- otherwise, we don't touch the coef
                  otherwise =
                    coef
        coefVar $~! adjust
        changed $=! True

    DearImGui.sameLine
    DearImGui.setNextItemWidth 140
    whenM (DearImGui.dragInt "##coef" coefVar 1 minBound maxBound) do
        -- When the coefficient changes, we update the final float value.
        scientificValue <- readTVarIO value
        output $=! toRealFloat scientificValue
        changed $=! True

    DearImGui.sameLine

    DearImGui.text . mappend "value: " =<< from . show <$> get output
    readTVarIO changed

renderOutVar :: (MonadUnliftIO m) => OutVar -> m ()
renderOutVar ovar = renderHistoryVar ovar.name =<< readTVarIO ovar.hvar

renderToggle :: (MonadUnliftIO m) => Text -> TVar Bool -> m Bool
renderToggle name v = renderVariable $ Variable name (ControllerToggle (tvarState v)) []

-- | Convert a TVar into a StateVar for dear-imgui ref
tvarState :: TVar a -> StateVar a
tvarState = tvarStateF id

-- | Apply a transformation after reading and before writting the TVar.
tvarStateF :: (a -> a) -> TVar a -> StateVar a
tvarStateF f tv = makeStateVar (f <$> readTVarIO tv) (atomically . writeTVar tv . f)

data HistoryVar = HistoryVar
    { value :: Float
    , history :: Seq CFloat
    }

historySize :: Int
historySize = 42

newHistoryVar :: HistoryVar
newHistoryVar = HistoryVar 0 (Seq.replicate historySize 0)

-- | Set the variable value and update the history.
pushHistoryVar :: Float -> HistoryVar -> HistoryVar
pushHistoryVar newValue hvar = newHVar
  where
    newHVar
        | prevValue == newValue = hvar
        | otherwise = HistoryVar newValue newHistory
    newHistory
        | Seq.length hvar.history < historySize = newSeq
        | otherwise = Seq.drop 1 newSeq
    newSeq = hvar.history Seq.|> CFloat newValue
    prevValue = case hvar.history of
        _ Seq.:|> (CFloat x) -> x
        _ -> 0

renderHistoryVar :: (MonadUnliftIO m) => Text -> HistoryVar -> m ()
renderHistoryVar name hvar = do
    -- current value
    void $! DearImGui.plotLines "##" (toList hvar.history)
    -- history
    DearImGui.sameLine
    void $! DearImGui.text $ name <> ": " <> from (show hvar.value)

renderVariables :: (MonadUnliftIO m) => [Variable] -> m ()
renderVariables vars = do
    void $! DearImGui.textColored dearYellow $ "Variables:"
    traverse_ renderVariable vars
