module AnimationFractal.Scene (
    AFContext (..),
    SceneInfo (..),
    newSceneInfo,
    mkSceneInfo,
    renderSceneInfo,
    sceneMods,
    withFragPath,
    withKeyframes,

    -- * helpers
    makeSceneStateVar,
    newFloatVar,
    newSciVar,
    newIntVar,
    newBoolVar,
    newVec2Var,
    newVec3Var,

    -- * save/load
    loadSave,
    saveSceneInfo,
    applyClip,

    -- * re-export
    Scene (..),
    ModulationOP (..),
    FilterConfig (..),
    FilterType (..),
    newScene,
    set0binding1,
    module AnimationFractal.Clip,
) where

import Data.Vector.Unboxed qualified as Unboxed
import Engine.Worker qualified as Worker
import Frelude

import Data.Aeson qualified as Aeson
import Geomancy.Lens

import AnimationFractal.Clip
import AnimationFractal.Context
import AnimationFractal.Input
import AnimationFractal.Input.AudioFilter (FilterConfig (..), FilterType (..))
import AnimationFractal.Modulation
import AnimationFractal.Player (MediaPlayer)
import AnimationFractal.Script
import AnimationFractal.Time
import AnimationFractal.Variable
import Render.AnimationFractal.Model (Scene (..), newScene, set0binding1)

import DearImGui qualified

data SceneInfo = SceneInfo
    { -- static attributes
      name :: Text
    , frag :: Code
    , mFragPath :: Maybe FilePath
    , midiTimeScale :: Maybe Double
    , medias :: [FilePath]
    , vars :: [Variable]
    , defaultClips :: [Clip]
    , keyFrames :: [Frame]
    , mMediaPlayer :: Maybe MediaPlayer
    , -- dynamic attributes
      state :: Worker.Var Scene
    , inputs :: TVar [Input]
    , modulations :: ModulationGroup
    , sequenceName :: TVar Text
    , clips :: TVar [Clip]
    , -- runtime attributes
      sceneTime :: TVar MSec
    , sceneFrame :: TVar Frame
    , clipTime :: TVar MSec
    , clipNext :: TVar (Maybe (MSec, Clip))
    , running :: TVar Bool
    }
    deriving (Generic)

newSceneInfo :: Text -> Code -> [Clip] -> [Worker.Var Scene -> STM Variable] -> STM SceneInfo
newSceneInfo name frag defaultClips mkVars = do
    state <- newTVar (Worker.Versioned (Unboxed.singleton 0) newScene)
    vars <- traverse (\mkVar -> mkVar state) mkVars
    inputs <- newTVar []
    modulations <- newModulationGroup []
    sequenceName <- newTVar "default"
    sceneTime <- newTVar 0
    clipTime <- newTVar 0
    clipNext <- newTVar Nothing
    sceneFrame <- newTVar 0
    clips <- newTVar []
    running <- newTVar True
    let mMediaPlayer = Nothing
        mFragPath = Nothing
        keyFrames = []
        medias = []
        midiTimeScale = Nothing
    pure SceneInfo{..}

sceneMods :: SceneInfo -> [OutVar]
sceneMods sceneInfo = concatMap (.mods) sceneInfo.vars

mkSceneInfo :: (MonadUnliftIO m) => Text -> Code -> [Clip] -> [Worker.Var Scene -> STM Variable] -> (Text, m SceneInfo)
mkSceneInfo name frag defaultClips' mkVars =
    ( name
    , do
        let defaultClips = map (#name %~ mappend "* ") defaultClips'
        atomically $ newSceneInfo name frag defaultClips mkVars
    )

withFragPath :: (Monad m) => FilePath -> (Text, m SceneInfo) -> (Text, m SceneInfo)
withFragPath fp (name, mkScene) = (name, (#mFragPath .~ Just ("src/Demo/" <> fp)) <$> mkScene)

withKeyframes :: (MonadUnliftIO m) => [Frame] -> (Text, m SceneInfo) -> (Text, m SceneInfo)
withKeyframes xs (name, mkScene) = (name, (#keyFrames .~ xs) <$> mkScene)

loadSave :: (MonadUnliftIO m) => AFContext -> SceneInfo -> Maybe Text -> m ()
loadSave ctx sceneInfo clipM = do
    let fp = "data" </> from sceneInfo.name </> "current.json"
        loadFile = do
            putStrLn $ "Loading clips from " <> fp
            liftIO $ decodeFileStrict @[Clip] fp
        loadFileM = do
            fileExist <- doesFileExist fp
            if fileExist
                then loadFile
                else Just <$> readTVarIO sceneInfo.clips
        startingScene = fromMaybe "* default" clipM
    clipsM <- loadFileM
    forM_ clipsM $ \clips -> do
        atomically $ modifyTVar' sceneInfo.clips (mappend clips)
        case filter (\clip -> clip.name == startingScene) (sceneInfo.defaultClips <> clips) of
            defaultClip : _ -> do
                putTextLn $ "Applying default clip: " <> showClip defaultClip
                applyClip ctx sceneInfo defaultClip
            _ -> pure ()

renderSceneInfo :: (MonadUnliftIO m, MonadReader env m, HasLogFunc env) => AFContext -> SceneInfo -> m ()
renderSceneInfo ctx sceneInfo = do
    let nameFlag =
            DearImGui.ImGuiInputTextFlags_CharsNoBlank
                .|. DearImGui.ImGuiInputTextFlags_AutoSelectAll
                .|. DearImGui.ImGuiInputTextFlags_EnterReturnsTrue
    DearImGui.withItemWidth 100 do
        whenM (dearImGui'inputTextFlags "##seq-name" sceneInfo.sequenceName 80 nameFlag) do
            putStrLn "save?"
    DearImGui.sameLine

    whenM (DearImGui.button "save") do
        -- void $ saveClip sceneInfo
        saveSceneInfo sceneInfo
    DearImGui.sameLine

    whenM (DearImGui.button "delete") do
        -- void $ saveClip sceneInfo
        DearImGui.openPopup "delete-clip"
    DearImGui.sameLine

    whenM (DearImGui.beginPopupModal "delete-clip") do
        DearImGui.text "Are you sure?"
        whenM (DearImGui.button "OK") do
            putStrLn "delete!"
            DearImGui.closeCurrentPopup
        DearImGui.sameLine
        whenM (DearImGui.button "Cancel") do
            DearImGui.closeCurrentPopup
        DearImGui.endPopup

    DearImGui.withItemWidth 200 do
        whenM (DearImGui.beginCombo "##load-seq" "Load a sequence") do
            whenM (DearImGui.selectable "new") do
                timeName <- from . formatTime defaultTimeLocale "%F_%T" <$> getCurrentTime
                applyClip ctx sceneInfo (newClip timeName)
            clips <- readTVarIO sceneInfo.clips
            forM_ (sceneInfo.defaultClips <> clips) $ \clip -> do
                whenM (DearImGui.selectable clip.name) do
                    putTextLn $ "loading: " <> clip.name
                    current <- exportClip sceneInfo
                    running <- readTVarIO sceneInfo.running
                    let clip'
                            | running = mkTransition current clip
                            | otherwise = clip
                    -- putTextLn $ unsafeInto @Text $ encode clip'
                    applyClip ctx sceneInfo clip'
            DearImGui.endCombo

makeSceneStateVar :: Lens' Scene a -> Worker.Var Scene -> StateVar a
makeSceneStateVar sceneLens sceneState = makeStateVar getv setv
  where
    getv = view sceneLens <$> Worker.readVar sceneState
    setv v = Worker.pushInput sceneState $ sceneLens .~ v

sceneOutVar :: Text -> Lens' Scene a -> Lens' a Float -> Worker.Var Scene -> TVar a -> STM OutVar
sceneOutVar name sceneLens valueLens sceneState controllerState =
    newOutVar name controllerVar sceneVar
  where
    controllerVar = tvarStateLens valueLens controllerState
    sceneVar = makeSceneStateVar (sceneLens . valueLens) sceneState

newFloatVar :: Text -> Maybe (Float, Float) -> Lens' Scene Float -> Worker.Var Scene -> STM Variable
newFloatVar name rangeM sceneLens sceneState = do
    current <- tvarState <$> newTVar 0
    ovar <- newOutVar name current (makeSceneStateVar sceneLens sceneState)
    pure $ Variable name (ControllerSliderFloat current minv maxv) [ovar]
  where
    (minv, maxv) = case rangeM of
        Just x -> x
        Nothing -> (-12, 12)

newSciVar :: Text -> Lens' Scene Float -> Worker.Var Scene -> STM Variable
newSciVar name sceneLens sceneState = do
    value <- newTVar 0
    current <- tvarState <$> newTVar 0
    ovar <- newOutVar name current (makeSceneStateVar sceneLens sceneState)
    pure $ Variable name (ControllerFloat value current) [ovar]

-- | roundLens adapts a Int32 to be modified by a modulation float
roundLens :: Lens' Int32 Float
roundLens = lens getv setv
  where
    getv :: Int32 -> Float
    getv = fromIntegral
    setv :: Int32 -> Float -> Int32
    setv _ = round

newIntVar :: Text -> Lens' Scene Int32 -> Worker.Var Scene -> STM Variable
newIntVar name sceneLens sceneState = do
    current <- newTVar 0
    ovar <- sceneOutVar name sceneLens roundLens sceneState current
    pure $ Variable name (ControllerSliderScallar (tvarState current) 0 255) [ovar]

-- | toggleLens adapts a Bool to be modified by a float.
toggleLens :: Lens' Bool Float
toggleLens = lens getv setv
  where
    getv :: Bool -> Float
    getv = bool 0 1
    setv :: Bool -> Float -> Bool
    setv _ f
        | f < 0.5 = False
        | otherwise = True

newBoolVar :: Text -> Lens' Scene Bool -> Worker.Var Scene -> STM Variable
newBoolVar name sceneLens sceneState = do
    current <- newTVar False
    ovar <- sceneOutVar name sceneLens toggleLens sceneState current
    pure $ Variable name (ControllerToggle (tvarState current)) [ovar]

tvarStateLens :: Lens' a b -> TVar a -> StateVar b
tvarStateLens l current = makeStateVar getv setv
  where
    getv = view l <$> readTVarIO current
    setv v = atomically $ modifyTVar' current (l .~ v)

tvar2StateVar :: TVar a -> StateVar a
tvar2StateVar t = makeStateVar (readTVarIO t) (atomically . writeTVar t)

newVec2Var :: Text -> Lens' Scene Vec2 -> Worker.Var Scene -> STM Variable
newVec2Var name sceneLens sceneState = do
    current <- newTVar 0
    mx <- sceneOutVar (name <> ".x") sceneLens v2x sceneState current
    my <- sceneOutVar (name <> ".y") sceneLens v2y sceneState current
    pure $ Variable name (ControllerVec2 (tvar2StateVar current) mx.current my.current) [mx, my]

newVec3Var :: Text -> Maybe (Float, Float) -> Lens' Scene Vec4 -> Worker.Var Scene -> STM Variable
newVec3Var name mRange sceneLens sceneState = do
    let (minv, maxv) = fromMaybe (-7, 7) mRange
    current <- newTVar 0
    mx <- sceneOutVar (name <> ".x") sceneLens v4x sceneState current
    my <- sceneOutVar (name <> ".y") sceneLens v4y sceneState current
    mz <- sceneOutVar (name <> ".z") sceneLens v4z sceneState current
    pure $ Variable name (ControllerVec3 (tvar2StateVar current) mx.current my.current mz.current minv maxv) [mx, my, mz]

-- import/export
baseDir :: SceneInfo -> FilePath
baseDir fi = "data" </> from fi.name

applyClip :: (MonadIO m) => AFContext -> SceneInfo -> Clip -> m ()
applyClip ctx sceneInfo (Clip name vars mods inputs next) = do
    putStrLn $ "Loading clip " <> from name <> ", mods: " <> show (length mods)
    -- stop current input
    currentInputs <- readTVarIO sceneInfo.inputs
    forM_ currentInputs $ \input -> liftIO input.stop
    atomically $ writeTVar sceneInfo.inputs []

    traverse_ (importVariable sceneInfo.vars) vars
    atomically $ writeTVar sceneInfo.sequenceName name
    let ModulationGroup mg = sceneInfo.modulations
    atomically do
        newMods <- traverse (importModulation ctx.modulations (sceneMods sceneInfo)) mods
        writeTVar mg (catMaybes newMods)

    newInputs <- traverse (importInput ctx.inputs sceneInfo.modulations) inputs
    atomically do
        writeTVar sceneInfo.inputs (catMaybes newInputs)
        writeTVar sceneInfo.clipTime 0
        writeTVar sceneInfo.clipNext next

exportClip :: (MonadIO m) => SceneInfo -> m Clip
exportClip sceneInfo = do
    -- serialize the scene status
    vars <- traverse exportVariable sceneInfo.vars

    -- serialize the modulations
    mods <- atomically do
        traverse exportModulation =<< modulationGroupList sceneInfo.modulations

    -- serialize the inputs
    inps <- atomically do
        traverse exportInput =<< readTVar sceneInfo.inputs

    name <- readTVarIO sceneInfo.sequenceName
    pure $ Clip{..}
  where
    next = Nothing
saveClip :: (MonadIO m) => SceneInfo -> m [Clip]
saveClip sceneInfo = do
    currentClip <- exportClip sceneInfo
    atomically do
        allClips <- readTVar sceneInfo.clips
        let updateOrAppend [] acc = currentClip : acc
            updateOrAppend (x : xs) acc
                | x.name == currentClip.name = currentClip : acc
                | otherwise = updateOrAppend xs (x : acc)
            newClips = reverse $ updateOrAppend allClips []
        writeTVar sceneInfo.clips newClips
        pure newClips

saveSceneInfo :: (MonadIO m, MonadReader env m, HasLogFunc env) => SceneInfo -> m ()
saveSceneInfo sceneInfo = do
    let fp = baseDir sceneInfo </> "current.json"
    logInfo $ "Saving scene to " <> displayShow fp
    createDirectoryIfMissing True (baseDir sceneInfo)
    clipsExport <- saveClip sceneInfo
    liftIO $ Aeson.encodeFile fp clipsExport
