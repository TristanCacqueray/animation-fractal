module AnimationFractal.Player where

import Frelude

import Data.List (isSuffixOf, partition)
import Data.Map.Strict qualified as Map
import DearImGui qualified
import Engine.AudioDecoder (Samples, decodeFile)
import Engine.AudioPlayer
import Engine.MidiDecoder as Midi (MidiEvents, decodeMidiFile, emptyMidiEvents, getFrameEvents)

import AnimationFractal.Time
import Data.Vector.Storable qualified as SV

data MediaPlayer = MediaPlayer
    { samples :: Samples
    , mediaFile :: FilePath
    , extraSamples :: Map Text Samples
    , midiEvents :: MidiEvents
    , player :: AudioPlayer
    , run :: Word -> IO ()
    }
    deriving (Generic)

stopMediaPlayer :: (MonadIO m) => MediaPlayer -> m ()
stopMediaPlayer mp = stopPlayer mp.player

playFrameSample :: (MonadIO m) => MediaPlayer -> Frame -> m ()
playFrameSample ps (Frame position) = do
    when (position `mod` 2 == 0) do
        -- putStrLn $ "Sending frame: " <> show position
        let frameSize' = frameSize * 2
        let startPos = (fromIntegral position `div` 2) * frameSize'
        when (startPos + frameSize' <= SV.length ps.samples) do
            let buf = SV.slice startPos frameSize' ps.samples
            playFrame ps.player buf
    liftIO $ ps.run position

getFrameSamples :: MediaPlayer -> Text -> Frame -> Samples
getFrameSamples ps name (Frame position) = case Map.lookup name ps.extraSamples of
    Nothing -> mempty
    Just samples -> SV.slice (fromIntegral position * frameSize) frameSize samples

frameSize :: Int
frameSize = 44100 `div` 60

getMediaLength :: MediaPlayer -> Frame
getMediaLength mp = from (1 + SV.length mp.samples `div` frameSize)

data StandalonePlayer = StandalonePlayer
    { mediaPlayer :: MediaPlayer
    , running :: TVar Bool
    , position :: TVar Frame
    }

newMediaPlayer :: forall m. (MonadFail m, MonadUnliftIO m) => Maybe Double -> FilePath -> [FilePath] -> m MediaPlayer
newMediaPlayer mMidiTimeScale fp extras = do
    let (mfps, rest) = partition (\s -> isSuffixOf ".mid" s || isSuffixOf ".MID" s) extras
    (samples : extrasSamples) <- mapConcurrently decodeFile (fp : rest)
    midiEvents <- case mfps of
        [mfp] -> decodeMidiFile mMidiTimeScale mfp
        [] -> pure emptyMidiEvents
        _ -> error "Only one or zero .mid file are supported"
    extraSamples <- foldM loadExtras mempty (zip rest extrasSamples)
    unless (extraSamples == mempty) do
        putStrLn $ "Loaded: " <> show (Map.keys extraSamples)
    player <- createPlayer
    let run = debugMidi midiEvents
        mediaFile = fp
    pure $ MediaPlayer{..}
  where
    loadExtras :: Map Text Samples -> (FilePath, Samples) -> m (Map Text Samples)
    loadExtras acc (sfp, samples) = do
        let name = from (takeBaseName sfp)
        pure $ Map.insert name samples acc

    debugMidi midiEvents position = do
        case Midi.getFrameEvents (fromIntegral position) midiEvents of
            [] -> pure ()
            xs -> putTextLn $ into @Text (show position) <> ": " <> from (show xs)

newStandalonePlayer :: (MonadFail m, MonadUnliftIO m) => FilePath -> [FilePath] -> m StandalonePlayer
newStandalonePlayer fp extras = do
    mediaPlayer <- newMediaPlayer Nothing fp extras
    running <- newTVarIO True
    position <- newTVarIO 0
    pure $ StandalonePlayer{..}

drawStandalonePlayer :: (MonadIO m) => StandalonePlayer -> m ()
drawStandalonePlayer sp = do
    whenM (DearImGui.button "<|") do
        atomically $ writeTVar sp.position 0
    DearImGui.sameLine

    running <- readTVarIO sp.running
    position <- readTVarIO sp.position

    let playBtn = bool "start" "pause" running
    whenM (DearImGui.button playBtn) do
        atomically $ modifyTVar' sp.running not
        when running do
            stopPlayer sp.mediaPlayer.player
    DearImGui.sameLine

    DearImGui.text (from $ show position)

    whenM (readTVarIO sp.running) do
        playFrameSample sp.mediaPlayer position
        atomically (modifyTVar' sp.position (+ 1))
