module AnimationFractal.Modulation.Event where

import AnimationFractal.Modulation
import AnimationFractal.Time
import AnimationFractal.Variable
import Frelude

import Control.Varying.Core qualified as Varying
import Control.Varying.Tween qualified as Varying

followMod :: STM Modulation
followMod = do
    speedV <- newTVar 500
    let speedController = Variable "speed (ms)" (ControllerSliderFloat (tvarState speedV) 5 5000) []

    modulation <- newModulation "follow" 0 []

    pure
        $ modulation
            { trigger = Just $ \v -> atomically do
                current <- (.value) <$> readTVar modulation.hvar
                speed <- readTVar speedV
                writeTVar modulation.source (linear (MSec $ from speed) current v)
            , controllers = [speedController]
            }

soMod :: Bool -> STM Modulation
soMod accum = do
    let accumV
            | accum = accumVar
            | otherwise = pure
    curV <- newTVar 0
    let so = audioTrack
    varV <- accumV =<< secondOrder so curV

    fV <- newTVar (double2Float so.f)
    zV <- newTVar (double2Float so.z)
    rV <- newTVar (double2Float so.r)
    let controllers =
            [ Variable "freq" (ControllerSliderFloat (tvarState fV) 0 10) []
            , Variable "zeta" (ControllerSliderFloat (tvarState zV) 0 5) []
            , Variable "resp" (ControllerSliderFloat (tvarState rV) (-5) 5) []
            ]

    modulation <- newModulation "s-o" varV []
    pure
        $ modulation
            { trigger = Just \(float2Double -> v) -> do
                atomically do
                    writeTVar curV v
            , controllers
            , controllersChanged = do
                f <- float2Double <$> readTVar fV
                z <- float2Double <$> readTVar zV
                r <- float2Double <$> readTVar rV
                newSource <- accumV =<< secondOrder (SecondOrder f z r) curV
                writeTVar modulation.source newSource
            , reset = do
                writeTVar curV 0
                newSource <- accumV =<< secondOrder so curV
                writeTVar modulation.source newSource
            }

fastIncrMod :: Maybe Value -> STM Modulation
fastIncrMod mParams = do
    let initialDecay :: Float
        initialDecay = case fromJSON <$> mParams of
            Nothing -> 60
            Just (Success [v]) -> v
            Just (Success xs) -> error $ "Invalid value: " <> show xs
            Just (Error v) -> error $ "Invalid speed value: " <> show v

    decayV <- newTVar initialDecay
    let controllers =
            [ Variable "decay" (ControllerSliderFloat (tvarState decayV) 1 500) []
            ]

    modulation <- newModulation "fincr" 0 []

    prevV <- newTVar 0
    maxV <- newTVar 0

    pure
        $ modulation
            { trigger = Just $ \v -> atomically do
                prev <- readTVar prevV
                decay <- readTVar decayV

                newV <-
                    if v > prev
                        then do
                            writeTVar maxV v
                            pure v
                        else do
                            prevMax <- readTVar maxV
                            pure $ min v (prevMax / decay)

                writeTVar prevV newV

                current <- (.value) <$> readTVar modulation.hvar
                writeTVar modulation.source (pure (current + newV))
            , controllers
            , exportParams = do
                decay <- readTVar decayV
                pure (Just $ toJSON decay)
            }

incrMod :: Maybe Value -> STM Modulation
incrMod mParams = do
    let initialSpeed :: Float
        initialSpeed = case fromJSON <$> mParams of
            Nothing -> 600
            Just (Success [v]) -> v
            Just (Success xs) -> error $ "Invalid value: " <> show xs
            Just (Error v) -> error $ "Invalid speed value: " <> show v

    speedV <- newTVar initialSpeed
    let speedController = Variable "speed (ms)" (ControllerSliderFloat (tvarState speedV) 5 5000) []

    modulation <- newModulation "incr" 0 []

    pure
        $ modulation
            { trigger = Just $ \v -> atomically do
                current <- (.value) <$> readTVar modulation.hvar
                speed <- readTVar speedV
                writeTVar modulation.source (linear (MSec $ from speed) current (current + v))
            , controllers = [speedController]
            , exportParams = pure (Just $ toJSON [initialSpeed])
            }

sinMod :: Maybe Value -> STM Modulation
sinMod _ = do
    speedV <- newTVar 500
    let speedController = Variable "speed (ms)" (ControllerSliderFloat (tvarState speedV) 5 5000) []

    counterV <- newTVar 0
    modulation <- newModulation "sin" (sinSpeed 0 1 counterV) []

    pure
        $ modulation
            { trigger = Just $ \v -> atomically do
                counter <- readTVar counterV
                (current, _) <- Varying.runVarT counter 0
                speed <- readTVar speedV
                writeTVar counterV (linear (MSec $ from speed) current (current + v))
            , controllers = [speedController]
            }

pulseMod :: Maybe Value -> STM Modulation
pulseMod mParams = do
    let (initialAttack, initialDecay) = case fromJSON @[Int32] <$> mParams of
            Nothing -> (50, 800)
            Just (Success [a, d]) -> (a, d)
            Just (Success xs) -> error $ "Invalid value: " <> show xs
            Just (Error v) -> error $ "Invalid pulse params: " <> show v

    attackV <- newTVar initialAttack
    decayV <- newTVar initialDecay
    modulation <- newModulation "pulseAD" 0 []
    pure
        $ modulation
            { controllers =
                [ Variable "attack" (ControllerSliderScallar (tvarState attackV) 10 5000) []
                , Variable "decay" (ControllerSliderScallar (tvarState decayV) 10 5000) []
                ]
            , trigger = Just $ \v -> atomically do
                attack <- MSec . fromIntegral <$> readTVar attackV
                decay <- MSec . fromIntegral <$> readTVar decayV
                writeTVar modulation.source (Varying.tweenStream (tweenAD v attack decay) 0)
            }

linearMod :: Maybe Value -> STM Modulation
linearMod mParams = do
    let initialDuration = case fromJSON @[Int32] <$> mParams of
            Nothing -> 500
            Just (Success [v]) -> v
            Just (Success xs) -> error $ "Invalid value: " <> show xs
            Just (Error v) -> error $ "invalid duration value: " <> show v

    durationV <- newTVar initialDuration
    let durationController = Variable "duration (ms)" (ControllerSliderScallar (tvarState durationV) 10 5000) []
    modulation <- newModulation "linear" 0 []
    pure
        $ modulation
            { controllers = [durationController]
            , trigger = Just $ \v -> do
                atomically do
                    duration <- readTVar durationV
                    writeTVar modulation.source (linear (MSec $ fromIntegral duration) v 0)
            }

data ADSR = ADSR
    { attack, decay, sustain, release :: Float
    }
    deriving (Generic, FromJSON)

adsrMod :: Maybe Value -> STM Modulation
adsrMod mParams = do
    let initialAdsr = case fromJSON @ADSR <$> mParams of
            Just (Success v) -> v
            _ -> ADSR 160 500 0.8 1500

    tvAdsr <- newTVar initialAdsr
    let controllers =
            [ Variable "attack (ms)" (ControllerSliderFloat (makeStateLens tvAdsr #attack) 0 1000) []
            , Variable "decay (ms)" (ControllerSliderFloat (makeStateLens tvAdsr #decay) 0 1000) []
            , Variable "sustain (gain)" (ControllerSliderFloat (makeStateLens tvAdsr #sustain) 0 2) []
            , Variable "release (ms)" (ControllerSliderFloat (makeStateLens tvAdsr #release) 0 1000) []
            ]
    modulation <- newModulation "adsr" 0 []
    pure
        $ modulation
            { controllers
            , trigger = Just $ \v -> do
                atomically do
                    current <- (.value) <$> readTVar modulation.hvar
                    adsr <- readTVar tvAdsr
                    let stream
                            | v > 0 = do
                                at <- Varying.tween Varying.easeInExpo current v (MSec $ float2Double adsr.attack)
                                Varying.tween Varying.easeOutExpo at (adsr.sustain * v) (MSec $ float2Double adsr.decay)
                            | otherwise = Varying.tween Varying.easeOutExpo current 0 (MSec $ float2Double adsr.release)
                    writeTVar modulation.source (Varying.tweenStream stream current)
            }

{-
decayVar :: MSec -> Float -> VaryingVar
decayVar duration value = Varying.tweenStream tween 0
  where
    tween = Varying.tween ease value 0 duration
    ease = Varying.easeOutExpo
-}

tweenDecay :: Float -> MSec -> Tween
tweenDecay v = Varying.tween Varying.easeOutExpo v 0

tweenAttack :: Float -> MSec -> Tween
tweenAttack = Varying.tween Varying.easeInExpo 0

tweenAD :: Float -> MSec -> MSec -> Tween
tweenAD velocity attack decay = do
    x <- tweenAttack velocity attack
    tweenDecay x decay

-- | Convert a pulsing 'VaryingVar' into a continously increasing var
accumVar :: VaryingVar -> STM VaryingVar
accumVar baseSource = do
    sourceV <- newTVar baseSource
    currentV <- newTVar 0
    let update 0 = do
            v <- readTVar currentV
            pure (v, Varying.VarT update)
        update t = do
            source <- readTVar sourceV
            (newValue, newSource) <- Varying.runVarT source t
            writeTVar sourceV newSource
            current <- readTVar currentV
            let newCurrent = current + newValue
            writeTVar currentV newCurrent
            pure (newCurrent, Varying.VarT update)
    pure (Varying.VarT update)

-- | The parameter for 'secondOrder'
data SecondOrder = SecondOrder
    { f :: Double
    -- ^ The speed at which the system respond to inputs
    , z :: Double
    -- ^ The damping coef
    , r :: Double
    -- ^ The intial response of the system.
    }

-- | A typical 'SecondOrder' for mechanical connection
mechanicalConnection :: SecondOrder
mechanicalConnection = SecondOrder 1 0.5 2

-- | A typical 'SecondOrder' for audio tracking
audioTrack :: SecondOrder
audioTrack = SecondOrder 2 1 (2 / 3)

-- | Semi-implicit Euler Method from https://www.youtube.com/watch?v=KPoeNZZ6H4s
secondOrder :: SecondOrder -> TVar Double -> STM VaryingVar
secondOrder so xV = do
    -- initialize state
    x0 <- readTVar xV
    xpV <- newTVar x0
    yV <- newTVar x0
    ydV <- newTVar 0

    let
        doUpdate 0 = do
            y <- readTVar yV
            pure (double2Float y, update)
        doUpdate t = do
            -- estimate velocity
            x <- readTVar xV
            xd <- do
                xp <- readTVar xpV
                writeTVar xpV x
                pure $ (x - xp) / t

            -- clamp k2 to guarantee stability
            let k2_stable = max k2 (t * t / 2 + t * k1 / 2)
                k2_stable' = max k2_stable (t * k1)

            -- integrate position by velocity
            y <- readTVar yV
            yd <- readTVar ydV
            let newY = y + t * yd
            writeTVar yV newY

            -- integrate velocity by acceleration
            let newYD = yd + t * (x + k3 * xd - newY - k1 * yd) / k2_stable'
            writeTVar ydV newYD

            pure (double2Float newY, update)
        update = Varying.VarT \(MSec t) -> doUpdate (t / 1000)
    pure $ update
  where
    -- compute constants
    f = so.f
    k1 = so.z / (pi * f)
    k2 = 1 / ((2 * pi * f) * (2 * pi * f))
    k3 = so.r * so.z / (2 * pi * f)
