module AnimationFractal.Modulation.Auto where

import AnimationFractal.Modulation
import AnimationFractal.Time
import AnimationFractal.Variable
import Frelude

import Control.Varying.Core qualified as Varying
import Control.Varying.Tween qualified as Varying

timeMod :: TVar MSec -> STM Modulation
timeMod sceneTimeV = do
    let getVar = Varying.VarT $ \_ -> do
            MSec sceneTime <- readTVar sceneTimeV
            pure (from (sceneTime / 1000), getVar)
    source <- newTVar getVar
    hvar <- newTVar newHistoryVar
    enabled <- newTVar True
    targets <- newTVar []
    startTreeOpen <- newTVar False
    pure $ Modulation{..}
  where
    exportParams = pure Nothing
    controllersChanged = pure ()
    controllers = []
    name = "iTime"
    modulationName = "iTime"
    reset = pure ()
    trigger = Nothing

lfoExpo :: MSec -> Tween
lfoExpo cycle = forever do
    Varying.tween_ Varying.easeOutExpo 0 1 cycle
    Varying.tween_ Varying.easeOutExpo 1 0 cycle

lfoExpoVar :: MSec -> VaryingVar
lfoExpoVar cycle = Varying.tweenStream (lfoExpo cycle) 0

lfoMod :: Maybe Value -> STM Modulation
lfoMod mParams = do
    -- decode params
    let initialSpeed :: Float
        initialSpeed = case fromJSON <$> mParams of
            Nothing -> 600
            Just (Success [v]) -> v
            Just (Success xs) -> error $ "Invalid value: " <> show xs
            Just (Error v) -> error $ "Invalid speed value: " <> show (v, mParams)

    speedV <- newTVar initialSpeed

    -- encode params
    let exportParams = do
            v <- readTVar speedV
            pure $ Just $ toJSON [v]

    let makeSource speed = lfoExpoVar speed
    modulation <- newModulation "lfo" (makeSource $ MSec $ from initialSpeed) []

    let setSpeed v = atomically do
            writeTVar speedV v

            writeTVar modulation.source $ makeSource (MSec $ from v)
        getSpeed = readTVarIO speedV
        speedState = makeStateVar getSpeed setSpeed
        speedController = Variable "speed (ms)" (ControllerSliderFloat speedState 10 5000) []
    -- writeTVar modulation.startTreeOpen (Just ())
    pure
        $ modulation
            { controllers = [speedController]
            , exportParams
            }
