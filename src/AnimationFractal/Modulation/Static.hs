module AnimationFractal.Modulation.Static where

import AnimationFractal.Modulation
import AnimationFractal.Time
import Frelude

staticLinear :: Maybe Value -> STM Modulation
staticLinear mParams = do
    let (duration, start, end) = case fromJSONE <$> mParams of
            Just (Right [x, y, z]) -> (x, y, z)
            _ -> error $ "Unknown linear static config: " <> from (show mParams)
        v = linear (MSec (from duration)) start end
    newModulation "linearS" v []
