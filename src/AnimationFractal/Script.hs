module AnimationFractal.Script where

import Data.Map.Strict qualified as Map
import Data.Vector ((!?))
import Data.Vector qualified as Vector
import Frelude

import AnimationFractal.Clip
import AnimationFractal.Modulation
import AnimationFractal.Time

mkTransition :: Clip -> Clip -> Clip
mkTransition src dst = Clip{..}
  where
    name = src.name <> "->" <> dst.name
    vars = src.vars
    inps = []
    mods = concatMap mkMod (Map.toList srcVars)
    mkMod (vname, s) = case Map.lookup vname dstVars of
        Just d -> linearTransition duration s d
        Nothing -> []
    next = Just (duration, dst)
    duration = 5000

    srcVars = clipVarMap src
    dstVars = clipVarMap dst

linearTransition :: MSec -> VariableDef -> VariableDef -> [ModulationDef]
linearTransition (MSec duration) srcDef dstDef =
    zipWith mkModulation (outvarDef srcDef) (outvarDef dstDef)
  where
    mkModulation :: (Text, Float) -> (Text, Float) -> ModulationDef
    mkModulation (name, src) (_, dst) = ModulationDef name "linearS" (Just params) [mt]
      where
        mt = ModulationTargetDef name 1.0 False ModSet
        params = toJSON [double2Float duration, src, dst]

data StaticScript = StaticScript
    { filepath :: FilePath
    , lastUpdated :: TVar UTCTime
    , keyFrames :: TVar [Frame]
    , frames :: TVar (Vector (Map Text Float))
    }

data StaticScriptExport = StaticScriptExport
    { keyFrames :: [Frame]
    , frames :: Vector (Map Text Float)
    }
    deriving (Generic, FromJSON, ToJSON)

newStaticScript :: (MonadIO m) => FilePath -> m StaticScript
newStaticScript filepath = do
    lastUpdated <- newTVarIO =<< getModificationTime filepath
    sse <- either error id <$> liftIO (eitherDecodeFileStrict' @StaticScriptExport filepath)
    frames <- newTVarIO (normalizeFrames sse.frames)
    -- TODO: add key frame to the json object
    keyFrames <- newTVarIO sse.keyFrames
    pure $ StaticScript{..}

test_script_encoding :: Assertion
test_script_encoding = do
    let script =
            Vector.fromList
                [Map.fromList [("a", 1), ("b", 2)], Map.fromList [("a", 1), ("b", 3)]]
        expected = "[{\"a\":1.0,\"b\":2.0},{\"b\":3.0}]"
        encoded = encode (removeDuplicateValues script)
    expected @=? encoded

normalizeFrames :: Vector (Map Text Float) -> Vector (Map Text Float)
normalizeFrames base = Vector.unfoldr go (0, mempty)
  where
    go :: (Int, Map Text Float) -> Maybe (Map Text Float, (Int, Map Text Float))
    go (pos, accum) = case base !? pos of
        Nothing -> Nothing
        Just frameValues ->
            let newAccum = Map.union frameValues accum
             in Just (newAccum, (pos + 1, newAccum))

removeDuplicateValues :: Vector (Map Text Float) -> Vector (Map Text Float)
removeDuplicateValues base = Vector.unfoldr go (0, mempty)
  where
    go :: (Int, Map Text Float) -> Maybe (Map Text Float, (Int, Map Text Float))
    go (pos, accum) = case base !? pos of
        Nothing -> Nothing
        Just frameValues ->
            let newAccum = Map.union frameValues accum
                newValues = Map.filterWithKey keepChangedValue frameValues
                keepChangedValue :: Text -> Float -> Bool
                keepChangedValue name v = case Map.lookup name accum of
                    Just prevV -> v /= prevV
                    Nothing -> True
             in Just (newValues, (pos + 1, newAccum))

isScriptUpdated :: (HasLogFunc env) => StaticScript -> RIO env Bool
isScriptUpdated staticScript = do
    lastUpdated <- readTVarIO staticScript.lastUpdated
    currentUpdated <- getModificationTime staticScript.filepath
    if (currentUpdated > lastUpdated)
        then
            liftIO (eitherDecodeFileStrict' @StaticScriptExport staticScript.filepath) >>= \case
                Left e -> do
                    logError $ "Couldn't decode " <> display (into @Text $ staticScript.filepath <> ": " <> e)
                    pure False
                Right sse -> atomically do
                    writeTVar staticScript.frames $ normalizeFrames sse.frames
                    writeTVar staticScript.lastUpdated currentUpdated
                    pure True
        else pure False

readScriptVarsValues :: StaticScript -> Frame -> RIO env [(Text, Float)]
readScriptVarsValues staticScript (Frame frame) = do
    (frames :: Vector (Map Text Float)) <- readTVarIO staticScript.frames
    pure $ maybe [] Map.toList $ frames !? fromIntegral frame
