{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-partial-fields #-}

module AnimationFractal.Config (loadDemo, testDefinitions) where

import AnimationFractal.Scene
import AnimationFractal.Variable qualified as AF
import Data.List (find)
import Data.Map.Strict qualified as Map
import Data.Ord qualified
import Data.Text qualified as Text
import Data.Text.Read qualified as Text
import Dhall qualified
import Dhall.TH
import Engine.Worker qualified as Worker
import Frelude hiding (max, min, mod)
import Prelude qualified

---
Dhall.TH.makeHaskellTypes
    [ MultipleConstructors "DHController" "(./package.dhall).Controller"
    , SingleConstructor "DHVariable" "MkVariable" "(./package.dhall).Variable.Type"
    , MultipleConstructors "DHModulationKind" "(./package.dhall).ModulationKind"
    , SingleConstructor "DHModulation" "MkModulation" "(./package.dhall).Modulation.Type"
    , MultipleConstructors "DHFilterKind" "(./package.dhall).FilterKind"
    , SingleConstructor "DHFilter" "MkFilter" "(./package.dhall).Filter.Type"
    , SingleConstructor "DHAudioMod" "MkAudioMod" "(./package.dhall).AudioMod.Type"
    , MultipleConstructors "DHInput" "(./package.dhall).Input"
    , SingleConstructor "DHScene" "MkScene" "(./package.dhall).Scene.Type"
    ]

floatLens :: Text -> Lens' Scene Float
floatLens = \case
    "var1" -> #var1
    "var2" -> #var2
    "var3" -> #var3
    "var4" -> #var4
    "var5" -> #var5
    "var6" -> #var6
    "evar1" -> #extraVar1
    "n1_1" -> #n1_1
    "n1_2" -> #n1_2
    "n1_3" -> #n1_3
    "n1_4" -> #n1_4
    "n1_5" -> #n1_5
    "n1_6" -> #n1_6
    "n1_7" -> #n1_7
    "n1_8" -> #n1_8
    "n1_9" -> #n1_9
    "n1_10" -> #n1_10
    "n1_11" -> #n1_11
    "n1_12" -> #n1_12
    "n1_13" -> #n1_13
    "n1_14" -> #n1_14
    "n1_15" -> #n1_15
    "n1_16" -> #n1_16
    "n2_1" -> #n2_1
    "n2_2" -> #n2_2
    "n2_3" -> #n2_3
    "n2_4" -> #n2_4
    "n2_5" -> #n2_5
    "n2_6" -> #n2_6
    "n2_7" -> #n2_7
    "n2_8" -> #n2_8
    "n2_9" -> #n2_9
    "n2_10" -> #n2_10
    "n2_11" -> #n2_11
    "n2_12" -> #n2_12
    "n2_13" -> #n2_13
    "n2_14" -> #n2_14
    "n2_15" -> #n2_15
    "n2_16" -> #n2_16
    "n3_1" -> #n3_1
    "n3_2" -> #n3_2
    "n3_3" -> #n3_3
    "n3_4" -> #n3_4
    "n3_5" -> #n3_5
    "n3_6" -> #n3_6
    "n3_7" -> #n3_7
    "n3_8" -> #n3_8
    "n3_9" -> #n3_9
    "n3_10" -> #n3_10
    "n3_11" -> #n3_11
    "n3_12" -> #n3_12
    "n3_13" -> #n3_13
    "n3_14" -> #n3_14
    "n3_15" -> #n3_15
    "n3_16" -> #n3_16
    "n4_1" -> #n4_1
    "n4_2" -> #n4_2
    "n4_3" -> #n4_3
    "n4_4" -> #n4_4
    "n4_5" -> #n4_5
    "n4_6" -> #n4_6
    "n4_7" -> #n4_7
    "n4_8" -> #n4_8
    "n4_9" -> #n4_9
    "n4_10" -> #n4_10
    "n4_11" -> #n4_11
    "n4_12" -> #n4_12
    "n4_13" -> #n4_13
    "n4_14" -> #n4_14
    "n4_15" -> #n4_15
    "n4_16" -> #n4_16
    other -> error $ "Unknown var: " <> from other

mkVariable :: DHVariable -> Worker.Var Scene -> STM AF.Variable
mkVariable var = case var.controller of
    Float{min, max} -> newFloatVar var.name (Just (double2Float min, double2Float max)) (floatLens var.uniformName)
    _ -> error "TODO!"

mkClip :: DHScene -> Clip
mkClip scene = Clip "* default" [] (addTime $ concatMap mkMods scene.inputs) (map mkInput scene.inputs) Nothing
  where
    addTime = case find (\v -> v.name == "iTime") scene.vars of
        Nothing -> id
        Just _ -> (ModulationDef "iTime" "iTime" Nothing [ModulationTargetDef "iTime" 1 False ModAdd] :)
    mkMods :: DHInput -> [ModulationDef]
    mkMods = \case
        Midi{mods} -> flip map mods \m ->
            mkModDef m.var m.kind
        AudioFile{mod} ->
            -- TODO: add filters
            [ mkModDef mod.dry.var mod.dry.kind
            ]
        _ -> error "todo!"

    mkModDef :: Text -> DHModulationKind -> ModulationDef
    mkModDef var = \case
        Incr{speed} ->
            ModulationDef
                var
                "incr"
                (Just $ toJSON [speed])
                [ModulationTargetDef var 1 False ModAdd]
        ADSR{attack, decay, sustain, release} ->
            ModulationDef
                var
                "adsr"
                (Just $ toJSON $ object ["attack" .= attack, "decay" .= decay, "sustain" .= sustain, "release" .= release])
                [ModulationTargetDef var 1 False ModAdd]

    mkInput :: DHInput -> InputDef
    mkInput = \case
        Midi{cap, name, mods, pulse} -> InputDef name (Just $ object ["cap" .= cap, "mods" .= map (\m -> m.var) mods, "pulse" .= pulse])
        AudioIn{} -> InputDef "pulse-in" Nothing
        AudioFile{name, mod} ->
            InputDef
                name
                ( Just
                    $ object
                        [ "modName" .= mod.dry.var
                        , "gain" .= (1 :: Float)
                        , "gate" .= mod.gate
                        , "filters" .= ([] :: [String])
                        ]
                )

-- | Pop the front char of a string
charRead :: Char -> Text -> Either String Text
charRead char txt = case Text.uncons txt of
    Just (c, rest)
        | c == char -> Right rest
        | otherwise -> Left $ "Expected " <> show char <> ", got " <> show c
    Nothing -> Left "empty string"

-- | Decode a variable name of the form 'nT_P' where T and P are decimal numbers
midiVar :: Text -> Either String (Word, Word)
midiVar name = do
    rest <- charRead 'n' name
    (track, rest') <- Text.decimal rest
    rest'' <- charRead '_' rest'
    (pitch, end) <- Text.decimal rest''
    case end of
        "" -> pure (track, pitch)
        _ -> Left "left overs"

-- | Regular variable can be accessed directly
defineRegVar :: DHVariable -> Text
defineRegVar var = Text.unwords ["#define", var.name, "scene." <> var.uniformName]

-- | Midi vars needs some code gen because they are stored in vec4 arrays.
defineMidiVar :: (Text, (Word, Word, Word)) -> [Text]
defineMidiVar (name, (track, pitchMin, pitchMax)) = [info, accessor]
  where
    name' = Text.replace " " "_" name
    d xs = Text.unwords ("#define" : xs)
    info = d [name' <> "_MAX", showT (pitchMax - pitchMin)]
    accessor
        | pitchMin + 1 == pitchMax = d ["i" <> name', midiUniform track (pitchMin - 1)]
        | otherwise =
            Text.unlines
                $ map definePitches [pitchMin .. pitchMax - 1]
                <> [ "float get" <> name' <> "(int pos) {"
                   , "  switch (pos % 4) {"
                   , "    case 0: return " <> vec4pitch <> ".r;"
                   , "    case 1: return " <> vec4pitch <> ".g;"
                   , "    case 2: return " <> vec4pitch <> ".b;"
                   , "    case 3: return " <> vec4pitch <> ".a;"
                   , "  }"
                   , "}"
                   ]
    vec4pitch = "scene.pitches" <> showT track <> "[((" <> showT (pitchMin - 1) <> " + pos) % " <> name' <> "_MAX) / 4]"
    definePitches p = d ["i" <> name' <> "_" <> showT (1 + p - pitchMin), midiUniform track (p - 1)]

midiUniform :: Word -> Word -> Text
midiUniform track pos = "scene.pitches" <> showT track <> "[" <> showT vec4pos <> "]." <> vec4swiz
  where
    vec4pos = pos `div` 4
    vec4swiz = case pos `omod` 4 of
        0 -> "r"
        1 -> "g"
        2 -> "b"
        _ -> "a"

showT :: (Show a) => a -> Text
showT = Text.pack . show

-- | New names because the original ones are used as field name
omin, omax, omod :: _
omin = Data.Ord.min
omax = Data.Ord.max
omod = Prelude.mod

varsDefinitions :: [DHVariable] -> [Text]
varsDefinitions vars = map defineRegVar regVars <> concatMap defineMidiVar (Map.toList midiVars)
  where
    (regVars, midiVars) = foldl' groupVar (mempty, mempty) vars

-- | Split variables into regular and a map of midi track name key and (track number, min pitch, max pitch) value
groupVar :: ([DHVariable], Map Text (Word, Word, Word)) -> DHVariable -> ([DHVariable], Map Text (Word, Word, Word))
groupVar (vars, midiVars) var = case midiVar var.uniformName of
    Left _ -> (var : vars, midiVars)
    Right (track, pitch) ->
        let trackName = Text.dropEnd 1 $ fst $ Text.breakOnEnd ":" var.name
            v = (track, pitch, pitch + 1)
            updatePitch (_, pitchMin, pitchMax) (_, newPitch, _) =
                (track, omin pitchMin newPitch, omax pitchMax (newPitch + 1))
         in (vars, Map.insertWith updatePitch trackName v midiVars)

testDefinitions :: IO ()
testDefinitions = traverse_ testDef testCases
  where
    testDef (vars, expectedOutput) =
        let output = varsDefinitions vars
         in when (output /= expectedOutput)
                $ putStrLn
                $ unlines
                $ [ "Expected:"
                  , Text.unpack (Text.unlines expectedOutput)
                  , "Got:"
                  , Text.unpack (Text.unlines output)
                  ]
    v name uniformName = MkVariable{name, controller = SciFloat, uniformName}
    testCases =
        [
            ( [v "KICK:1" "n2_1", v "SNARE:1" "n2_6"]
            , ["#define KICK_MAX 1", "#define iKICK scene.pitches2[0].r", "#define SNARE_MAX 1", "#define iSNARE scene.pitches2[1].g"]
            )
        ,
            ( [v "BASS:1" "n1_1", v "BASS:2" "n1_2"]
            , ["#define BASS_MAX 2"]
            )
        ]

mkScene :: DHScene -> STM SceneInfo
mkScene scene = do
    si <- newSceneInfo scene.name code [mkClip scene] (map mkVariable scene.vars)
    pure
        $ si
            { mFragPath = Just $ mkPath scene.shaderPath
            , medias = map from scene.medias
            , midiTimeScale = Just scene.midiTimeScale
            , keyFrames = map from scene.keyframes
            }
  where
    mkPath = into
    code = Code $ Text.unlines codeLines
    (regVars, midiVars) = foldl' groupVar (mempty, mempty) scene.vars
    codeLines =
        [ "#version 450"
        , "layout (location = 0) in vec2 inUV;"
        , "layout (location = 0) out vec4 oColor;"
        , "#define iResolution scene.screenResolution"
        , "#define fragColor oColor"
        , "#define ANIMATION_FRACTAL 1"
        , set0binding1.unCode
        ]
            <> map defineRegVar regVars
            <> concatMap defineMidiVar (Map.toList midiVars)

loadDemo :: FilePath -> IO (STM SceneInfo)
loadDemo fp = mkScene <$> Dhall.input Dhall.auto (from fp)
