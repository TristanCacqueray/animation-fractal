module AnimationFractal.Test (testImGUI) where

import Frelude

import Engine.App (engineMain)

import DearImGui qualified

import Graphics.UI.GLFW qualified as GLFW
import Render.ImGui qualified as ImGui

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Keid
import Render.Basic qualified as Basic
import Render.ForwardMsaa qualified as ForwardMsaa
import Resource.Region qualified as Region

import Engine.Events qualified as Events
import Engine.StageSwitch (trySwitchStage)
import Engine.Window.Key (Key (..))
import Engine.Window.Key qualified as Key

import Engine.Vulkan.Swapchain (SwapchainResources)

import AnimationFractal.Input.Key (KeyHandlers, handleKey)

type TestResource st = ResourceT (RIO (App Keid.GlobalHandles (Maybe SwapchainResources))) st
type TestDraw st a = RIO (App Keid.GlobalHandles st, Keid.Frame Basic.RenderPasses Basic.Pipelines Stage.NoFrameResources) a

mainStage :: StackStage -> IO ()
mainStage s = withArgs ["--size", "800x600"] $ engineMain s

-- Runs a dear-imgui code along with a resource.
testImGUI :: forall st. KeyHandlers -> TestResource st -> (st -> TestDraw st ()) -> IO ()
testImGUI kh mkState doDraw = mainStage . StackStage $ Stage.assemble "Main" rendering resources (Just scene)
  where
    rendering = ImGui.renderWith Basic.rpForwardMsaa 0 Basic.rendering_
    resources =
        Stage.Resources
            { Stage.rInitialRS = Region.run mkState
            , Stage.rInitialRR = \_ _ _ -> pure Stage.NoFrameResources
            }
    scene =
        mempty
            { Stage.scBeforeLoop = do
                env <- asks appEnv
                liftIO do
                    GLFW.setWindowAttrib env.ghWindow GLFW.WindowAttrib'Decorated False
                    GLFW.setWindowAttrib env.ghWindow GLFW.WindowAttrib'Floating True
                    GLFW.setWindowAttrib env.ghWindow GLFW.WindowAttrib'Resizable False
                    GLFW.setWindowTitle env.ghWindow "Animation Fractal"
                    GLFW.setWindowOpacity env.ghWindow 1.0

                ImGui.allocateLoop True
                void $! Region.local $ Events.spawn handleEvent [Key.callback . keyHandler]
            , Stage.scRecordCommands = recordCommands
            }
    recordCommands cb _ imageIndex = do
        dear <- do
            stV <- gets id
            snd <$> ImGui.mkDrawData do
                DearImGui.withFullscreen do
                    let ctrlFlags =
                            DearImGui.ImGuiWindowFlags_NoCollapse
                                .|. DearImGui.ImGuiWindowFlags_NoMove
                    _ <- dearImGui'beginWithFlags "Variables and modulations" ctrlFlags
                    doDraw stV
                    DearImGui.end
        Keid.Frame{fRenderpass} <- asks snd
        ForwardMsaa.usePass fRenderpass.rpForwardMsaa imageIndex cb do
            ImGui.draw dear cb

    keyHandler (Events.Sink signal) _ (_, state, key) = case key of
        Key'Escape -> signal ()
        _ -> handleKey kh state key

    handleEvent () = void $ trySwitchStage Keid.Finish
