module AnimationFractal.Modulation (
    lookupOutVar,
    Modulation (..),
    newModulation,
    newVarModulation,
    readModulation,
    renderModulations,
    ModulationTarget (..),
    renderModulationTarget,
    renderAddModulationTargetButton,
    ModulationOP (..),

    -- * helpers
    VaryingVar,
    Tween,
    linear,
    sinSpeed,

    -- * updates
    MSec,
    showMSec,
    getMonotonicTimeMS,
    applyModulations,

    -- * group
    ModulationGroup (..),
    newModulationGroup,
    modulationGroupList,
    modulationGroupMap,
    renderAddModulationButton,
    lookupModulation,

    -- * database
    AvailableModulations,
) where

import Data.Map.Strict qualified as Map
import Data.Text qualified as Text
import Frelude

import Control.Varying.Core qualified as Varying
import Control.Varying.Tween qualified as Varying

import DearImGui qualified
import DearImGui.Extra qualified

import AnimationFractal.Time
import AnimationFractal.Variable

data Modulation = Modulation
    { name :: Text
    , modulationName :: Text
    , source :: TVar VaryingVar
    , hvar :: TVar HistoryVar
    , enabled :: TVar Bool
    , controllers :: [Variable]
    , controllersChanged :: STM ()
    , targets :: TVar [ModulationTarget]
    , trigger :: Maybe (Float -> IO ())
    , startTreeOpen :: TVar Bool
    , exportParams :: STM (Maybe Value)
    , reset :: STM ()
    }
    deriving (Generic)

type AvailableModulations = [(Text, Maybe Value -> STM Modulation)]

data ModulationOP = ModAdd | ModSet
    deriving (Generic, FromJSON, ToJSON)

data ModulationTarget = ModulationTarget
    { ovar :: OutVar
    , gain :: TVar Float
    , inverse :: TVar Bool
    , op :: TVar ModulationOP
    }

newModulationTarget :: OutVar -> STM ModulationTarget
newModulationTarget ovar = ModulationTarget ovar <$> newTVar 1 <*> newTVar False <*> newTVar ModAdd

initializeOutVar :: (MonadIO m) => OutVar -> m ()
initializeOutVar ovar = liftIO do
    current <- getCurrent
    atomically $ writeTVar ovar.buffer current
  where
    StateVar getCurrent _ = ovar.current

performOutput :: (MonadIO m) => OutVar -> m ()
performOutput ovar = liftIO do
    buffer <- readTVarIO ovar.buffer
    output <- getOutput
    when (buffer /= output) do
        setOutput buffer
  where
    StateVar getOutput setOutput = ovar.scene

updateHistory :: (MonadIO m) => OutVar -> m ()
updateHistory ovar = liftIO do
    final <- getOutput
    atomically $ modifyTVar' ovar.hvar (pushHistoryVar final)
  where
    StateVar getOutput _ = ovar.scene

newModulation :: Text -> VaryingVar -> [ModulationTarget] -> STM Modulation
newModulation name sourceVar targets' = do
    source <- newTVar sourceVar
    let reset = writeTVar source sourceVar
    hvar <- newTVar newHistoryVar
    enabled <- newTVar True
    let controllers = []
        controllersChanged = pure ()
        trigger = Nothing
    targets <- newTVar targets'
    startTreeOpen <- newTVar True
    let exportParams = pure Nothing
    pure $ Modulation{..}
  where
    modulationName = name

newVarModulation :: Text -> [OutVar] -> STM Modulation
newVarModulation name outVars = case lookupOutVar name outVars of
    Nothing -> error $ "Unknown var: " <> from name
    Just outVar -> do
        mt <- newModulationTarget outVar
        newModulation name 0 [mt]

readModulation :: MSec -> Modulation -> STM Float
readModulation elapsed modulation = do
    -- advance the VaryingVar
    source <- readTVar modulation.source
    (newValue, newSource) <- Varying.runVarT source elapsed
    writeTVar modulation.source newSource

    -- update the history
    modifyTVar' modulation.hvar (pushHistoryVar newValue)

    -- return the new value
    pure newValue

pollModulation :: MSec -> Modulation -> STM ()
pollModulation now modulation = whenM (readTVar modulation.enabled) do
    -- acquire the current value
    value <- readModulation now modulation

    -- apply to each target
    targets <- readTVar modulation.targets
    forM_ targets $ \target -> do
        prev <- readTVar target.ovar.buffer
        op <- readTVar target.op
        gain <- readTVar target.gain
        inv <- bool 1 -1 <$> readTVar target.inverse
        writeTVar target.ovar.buffer $ case op of
            ModAdd -> prev + value * gain * inv
            ModSet -> value * gain * inv

applyModulations :: (MonadUnliftIO m) => MSec -> [OutVar] -> [Modulation] -> m ()
applyModulations now vars modulations = do
    -- Initialize variable buffers
    traverse_ initializeOutVar vars

    atomically (traverse_ (pollModulation now) modulations)

    -- Update scene var version to trigger render
    traverse_ performOutput vars

    -- Update history
    traverse_ updateHistory vars

-- | Specialize the varying package to MSec time and float value.
type VaryingVar = Varying.VarT STM MSec Float

type Tween = Varying.TweenT MSec Float STM Float

linear :: MSec -> Float -> Float -> VaryingVar
linear duration start end = Varying.tweenStream tween end
  where
    tween = Varying.tween ease start end duration
    ease = Varying.linear

sinSpeed :: Float -> Float -> TVar VaryingVar -> VaryingVar
sinSpeed offset gain counterV = Varying.VarT $ \elapsed -> do
    counter <- readTVar counterV
    (v, newCounter) <- Varying.runVarT counter elapsed
    writeTVar counterV newCounter
    pure (offset + gain * sin v, sinSpeed offset gain counterV)

renderModulationTarget :: (MonadUnliftIO m) => TVar [ModulationTarget] -> ModulationTarget -> m ()
renderModulationTarget xs mt = DearImGui.withID mt.ovar.name do
    DearImGui.spacing
    whenM (DearImGui.button "x") do
        atomically $ modifyTVar' xs $ filter (\x -> x.ovar.name /= mt.ovar.name)
    DearImGui.sameLine
    void $! DearImGui.text $ "target: " <> mt.ovar.name
    DearImGui.sameLine
    -- renderToggle "<set>" mt.setValue
    -- DearImGui.sameLine
    -- renderController $ Controller "<osc>" (ControllerToggle (noOutput $ stateTVarF not mt.setValue))

    void $! renderVariable $ Variable "gain" (ControllerSliderFloat (tvarState mt.gain) 0 12) []
    DearImGui.sameLine
    void $! renderToggle "inv" mt.inverse

renderModulation :: (MonadUnliftIO m) => [OutVar] -> (Int, Modulation) -> m ()
renderModulation vars (idx, m) = DearImGui.withID m.name $ DearImGui.withID idx do
    liftIO $ DearImGui.Extra.startTreeOpen m.startTreeOpen

    isOpen <- DearImGui.treeNode $ "modulation: " <> m.name
    if isOpen
        then do
            void $! renderVariable $ Variable "enabled" (ControllerToggle (tvarState m.enabled)) []
            DearImGui.sameLine
            forM_ m.trigger \doTrigger -> do
                clicked <- DearImGui.button "trigger"
                DearImGui.sameLine
                when clicked (liftIO $ doTrigger 1)
                clicked' <- DearImGui.button "off"
                DearImGui.sameLine
                when clicked' (liftIO $ doTrigger 0)
            renderHistoryVar m.name =<< readTVarIO m.hvar

            (changed :: [Bool]) <- traverse renderVariable m.controllers
            when (or changed) do
                atomically m.controllersChanged
            renderAddModulationTargetButton vars m.targets
            DearImGui.newLine
            DearImGui.treePop
        else renderHistoryVar m.name =<< readTVarIO m.hvar

renderAddModulationTargetButton :: (MonadUnliftIO m) => [OutVar] -> TVar [ModulationTarget] -> m ()
renderAddModulationTargetButton vars targetsV = do
    targets <- reverse <$> readTVarIO targetsV
    traverse_ (renderModulationTarget targetsV) targets
    let avail = filter (\var -> all (\target -> target.ovar.name /= var.name) targets) vars

    unless (null avail) do
        whenM (DearImGui.beginCombo "target" "Pick a target") do
            forM_ avail $ \v -> do
                whenM (DearImGui.selectable v.name) do
                    atomically do
                        mt <- newModulationTarget v
                        modifyTVar' targetsV (mt :)
            DearImGui.endCombo

renderModulations :: (MonadUnliftIO m) => [OutVar] -> [Modulation] -> m ()
renderModulations vars mods = do
    DearImGui.spacing
    void $! DearImGui.textColored dearYellow $ "Modulations:"
    traverse_ (renderModulation vars) (zip [0 ..] mods)

newtype ModulationGroup = ModulationGroup (TVar [Modulation])

newModulationGroup :: [Modulation] -> STM ModulationGroup
newModulationGroup mods = ModulationGroup <$> newTVar mods

addModulation :: ModulationGroup -> Modulation -> STM ()
addModulation (ModulationGroup mg) m = modifyTVar' mg (m :)

modulationGroupList :: ModulationGroup -> STM [Modulation]
modulationGroupList (ModulationGroup mg) = readTVar mg

modulationGroupMap :: ModulationGroup -> STM (Map Text Modulation)
modulationGroupMap mg = do
    mods <- modulationGroupList mg
    pure $ Map.fromList (map (\m -> (m.name, m)) mods)

lookupModulation :: Text -> [Modulation] -> Maybe Modulation
lookupModulation _ [] = Nothing
lookupModulation name (m : xs)
    | name == m.name = Just m
    | otherwise = lookupModulation name xs

renderAddModulationButton :: (MonadUnliftIO m) => ModulationGroup -> AvailableModulations -> m ()
renderAddModulationButton mg avail = do
    DearImGui.spacing
    whenM (DearImGui.beginCombo "##add-mod" "Add a modulation") do
        forM_ (filter (\(name, _) -> not $ "S" `Text.isSuffixOf` name) avail) $ \(name, mkMod) -> do
            whenM (DearImGui.selectable name) do
                atomically do
                    newName <- do
                        total <- length <$> modulationGroupList mg
                        pure $ name <> "-" <> from (show total)
                    m <- mkMod Nothing
                    open <- newTVar True
                    addModulation mg (m & (#startTreeOpen .~ open) . (#name .~ newName))
        DearImGui.endCombo
