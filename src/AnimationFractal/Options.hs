module AnimationFractal.Options (AFOptions (..), parseOptions) where

import Frelude
import Options.Generic

data AFOptions w = AFOptions
    { prog :: w ::: Text <?> "The demo name" <!> "blank"
    , clip :: w ::: Maybe Text <?> "Load a custom clip"
    , media :: w ::: [FilePath] <?> "Play local media"
    , script :: w ::: Maybe FilePath <?> "A static script"
    , debug :: w ::: Bool <?> "Print debug info"
    , renderDemo :: w ::: Bool <?> "Record demo video"
    , render :: w ::: Maybe FilePath <?> "Record video"
    , end :: w ::: Maybe Int <?> "Ending frame"
    , config :: w ::: Maybe FilePath <?> "Scene config file"
    }
    deriving (Generic)

instance ParseRecord (AFOptions Wrapped)
deriving instance Show (AFOptions Unwrapped)

parseOptions :: [String] -> IO (AFOptions Unwrapped)
parseOptions args = case unwrapRecordPure (from <$> args) of
    Just x -> pure x
    Nothing -> unwrapRecord "Animation Fractal"
