module AnimationFractal.Context (
    AFContext (..),
    allModulations,
    allInputs,
) where

import Frelude

import AnimationFractal.Input
import AnimationFractal.Modulation
import AnimationFractal.Script
import AnimationFractal.Time

import AnimationFractal.Input.Key qualified as Key
import AnimationFractal.Input.PortMidi qualified as PortMidi
import AnimationFractal.Input.PulseAudio qualified as PulseAudio

import AnimationFractal.Modulation.Auto
import AnimationFractal.Modulation.Event
import AnimationFractal.Modulation.Static

data AFContext = AFContext
    { inputs :: AvailableInputs
    , modulations :: AvailableModulations
    , staticScript :: Maybe StaticScript
    , render :: Maybe (FilePath, Frame)
    }

allModulations :: TVar MSec -> AvailableModulations
allModulations sceneTime =
    [ ("follow", const followMod)
    , ("adsr", adsrMod)
    , ("sec-ord", const (soMod True))
    , ("sec-pulse", const (soMod False))
    , ("incr", incrMod)
    , ("fincr", fastIncrMod)
    , ("sin", sinMod)
    , ("linear", linearMod)
    , ("lfo", lfoMod)
    , ("pulseAD", pulseMod)
    , ("iTime", const (timeMod sceneTime))
    , -- static modulation for script, not to be used dynamically
      ("linearS", staticLinear)
    ]

allInputs :: Key.KeyHandlers -> AvailableInputs
allInputs kh =
    [ ("key", Key.mkInput kh)
    , ("pulse-in", PulseAudio.mkInput)
    , ("midi-in", PortMidi.mkInput)
    ]
