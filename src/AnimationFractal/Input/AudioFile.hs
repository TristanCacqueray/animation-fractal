module AnimationFractal.Input.AudioFile where

import Data.Vector.Storable qualified as SV
import DearImGui.Extra qualified
import Frelude

import AnimationFractal.Input as AF
import AnimationFractal.Input.AudioFilter as AFF
import AnimationFractal.Modulation as AF
import AnimationFractal.Time (Frame (..))
import AnimationFractal.Variable as AF
import Engine.AudioDecoder (Samples, soundRMS)

data InputConfig = InputConfig
    { gate :: Maybe Float
    , gain :: Maybe Float
    , modName :: Maybe Text
    , filters :: Maybe [FilterConfig]
    }
    deriving (Generic, ToJSON, FromJSON)

mkInput :: Bool -> (Text, Samples) -> Maybe Value -> ModulationGroup -> IO Input
mkInput withFilters (name, samples) configM mg = do
    hvar <- newTVarIO newHistoryVar

    filtersV <- atomically AFF.mkFilters

    (targetV :: TVar (Maybe Modulation)) <- newTVarIO Nothing

    gainV <- newTVarIO 1.0
    gateV <- newTVarIO 1
    let controllers =
            [ Variable "gate" (ControllerSliderFloat (tvarState gateV) 0 100) []
            , Variable "gain" (ControllerSliderFloat (tvarState gainV) 0 10) []
            ]

    let applyConfig value = case fromJSON @InputConfig value of
            Success conf -> atomically $ do
                writeTVar gateV (fromMaybe 10 conf.gate)
                writeTVar gainV (fromMaybe 1.0 conf.gain)
                forM_ conf.modName \modName -> do
                    mods <- modulationGroupList mg
                    writeTVar targetV (lookupModulation modName mods)
                traverse_ (addFilters filtersV) =<< case conf.filters of
                    Just xs@(_ : _) -> traverse (mkFilterConfig mg) xs
                    _
                        | withFilters -> defaultFilters
                        | otherwise -> pure []
            Error err -> do
                putStrLn $ "Oops, couldn't load pulse config: " <> show err
        exportConfig = do
            gate <-
                readTVar gateV >>= \case
                    0 -> pure Nothing
                    n -> pure $ Just n
            gain <-
                readTVar gainV >>= \case
                    1 -> pure Nothing
                    n -> pure $ Just n
            modName <- fmap (\m -> m.name) <$> readTVar targetV
            -- TODO: export filters config
            filters <- exportFiltersConfig filtersV
            pure . Just $ toJSON InputConfig{..}
    traverse_ applyConfig configM

    let stop = pure ()
        draw = do
            void $! DearImGui.Extra.renderTree ("f: dry " <> name) do
                traverse_ renderVariable controllers
                renderHistoryVar "peak" =<< readTVarIO hvar
                drawSingleModSelector mg targetV
            AFF.drawFilters mg filtersV

        advance (Frame position) = do
            let frameSize = 44100 `div` 60
            let frameSamples
                    | fromIntegral (position + 1) * frameSize >= SV.length samples = mempty
                    | otherwise = SV.slice (fromIntegral position * frameSize) frameSize samples
            gain <- readTVarIO gainV
            let peak = soundRMS frameSamples
            gate <- (/ 100) <$> readTVarIO gateV
            let newv
                    | peak > gate = gain * (peak - gate)
                    | otherwise = 0
            targets <- atomically do
                modifyTVar' hvar (pushHistoryVar newv)
                readTVar targetV

            when (SV.length frameSamples > 0) do
                updateFilters filtersV frameSamples

            -- putStrLn $ "Peak: " <> show peak
            forM_ targets \target -> do
                forM_ target.trigger (\t -> liftIO $ t newv)

    pure $ Input{..}
