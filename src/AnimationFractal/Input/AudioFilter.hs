module AnimationFractal.Input.AudioFilter where

import DearImGui qualified
import DearImGui.Extra qualified
import Engine.AudioDecoder (Samples)
import Frelude
import SimpleDSP.IIR qualified as IIR

import AnimationFractal.Input
import AnimationFractal.Modulation
import AnimationFractal.Variable

data FilterConfig = FilterConfig
    { name :: Text
    , ftype :: FilterType
    , freq :: Float
    , gain :: Float
    , modName :: Maybe Text
    }
    deriving (Generic, ToJSON, FromJSON)

data Filter = Filter
    { controllers :: [Variable]
    , name :: Text
    , ftype :: FilterType
    , infos :: TVar IIR.RMSInfo
    , hvar :: TVar HistoryVar
    , freq :: TVar Float
    , appliedFreq :: TVar Float
    , res :: TVar Float
    , gain :: TVar Float
    , target :: TVar (Maybe Modulation)
    , startTreeOpen :: TVar Bool
    }

data FilterType = LowPass | HighPass | BandPass | BandSkirt
    deriving (Eq, Enum, Bounded)
    deriving (Generic, ToJSON, FromJSON)

mkFilterConfig :: ModulationGroup -> FilterConfig -> STM Filter
mkFilterConfig mg fc = do
    f <- mkFilter fc.name fc.ftype fc.freq
    forM_ fc.modName \modName -> do
        mods <- modulationGroupList mg
        writeTVar f.target (lookupModulation modName mods)
    writeTVar f.gain fc.gain
    pure f

exportFilterConfig :: Filter -> STM FilterConfig
exportFilterConfig f =
    FilterConfig
        f.name
        f.ftype
        <$> readTVar f.freq
        <*> readTVar f.gain
        <*> fmap (fmap (\m -> m.name)) (readTVar f.target)

mkFilter :: Text -> FilterType -> Float -> STM Filter
mkFilter name ft freq = do
    freqV <- newTVar freq
    gainV <- newTVar 1.0
    Filter
        [ Variable "freq" (ControllerSliderFloat (tvarState freqV) 0 20_000) []
        , Variable "gain" (ControllerSliderFloat (tvarState gainV) 0 42) []
        ]
        ("f: " <> name <> "-" <> filterName ft)
        ft
        <$> newTVar (IIR.mkRMSInfo params)
        <*> newTVar newHistoryVar
        <*> pure freqV
        <*> newTVar freq
        <*> newTVar res
        <*> pure gainV
        <*> newTVar Nothing
        <*> newTVar False
  where
    res = 42
    params = mkParams freq ft

mkParams :: Float -> FilterType -> IIR.IIRParams
mkParams freq = \case
    LowPass -> IIR.lowPassFilter freq 4
    HighPass -> IIR.highPassFilter freq 4
    BandPass -> IIR.bandPassFilter freq 4
    BandSkirt -> IIR.bandPassSkirtFilter freq 4

filterName :: FilterType -> Text
filterName = \case
    LowPass -> "low-pass"
    HighPass -> "high-pass"
    BandPass -> "band-8pass"
    BandSkirt -> "band-skit"

-- | Return the name of the filter when the user click delete
drawFilter :: ModulationGroup -> (Int, Filter) -> IO (Maybe Text)
drawFilter mg (idx, f) = DearImGui.withID idx do
    DearImGui.Extra.startTreeOpen f.startTreeOpen

    isDeleted <- DearImGui.Extra.renderTree f.name do
        clicked <- DearImGui.button "delete"
        traverse_ renderVariable f.controllers
        drawSingleModSelector mg f.target
        pure clicked
    renderHistoryVar "rms" =<< readTVarIO f.hvar
    pure $ if isDeleted == Just True then Just f.name else Nothing

newtype Filters = Filters (TVar [Filter])

defaultFilters :: STM [Filter]
defaultFilters =
    sequence
        [ mkFilter "1" LowPass 150
        , mkFilter "2" BandPass 5800
        , mkFilter "3" HighPass 12000
        ]

mkFilters :: STM Filters
mkFilters = Filters <$> newTVar []

exportFiltersConfig :: Filters -> STM (Maybe [FilterConfig])
exportFiltersConfig (Filters filtersV) = do
    confs <- traverse exportFilterConfig =<< readTVar filtersV
    pure $ case confs of
        [] -> Nothing
        xs -> Just xs

addFilters :: Filters -> Filter -> STM ()
addFilters (Filters filtersV) newFilter =
    modifyTVar' filtersV (\xs -> xs <> [newFilter])

updateFilters :: Filters -> Samples -> IO ()
updateFilters (Filters fsV) samples = do
    traverse_ doUpdateFilters =<< readTVarIO fsV
  where
    doUpdateFilters f = do
        newv <- atomically do
            infos <- readTVar f.infos
            gain <- readTVar f.gain

            -- check if filter needs new params
            appliedFreq <- readTVar f.appliedFreq
            freq <- readTVar f.freq
            infos' <-
                if appliedFreq == freq
                    then pure infos
                    else do
                        writeTVar f.appliedFreq freq
                        let params = mkParams freq f.ftype
                        pure $ IIR.setRMSParams params infos

            let newInfos = IIR.updateInfo infos' samples
                newv = gain * IIR.maxRMSVolume newInfos
            writeTVar f.infos newInfos
            modifyTVar' f.hvar (pushHistoryVar newv)
            pure newv
        mTarget <- readTVarIO f.target
        forM_ mTarget \target -> do
            forM_ target.trigger (\t -> liftIO $ t newv)

drawFilters :: ModulationGroup -> Filters -> IO ()
drawFilters mg (Filters fsV) = do
    toDelete <- catMaybes <$> (traverse (drawFilter mg) . zip [0 ..] =<< readTVarIO fsV)
    atomically $ do
        traverse_ (\name -> modifyTVar' fsV (filter (\f -> f.name /= name))) toDelete
    drawAddFilter
  where
    drawAddFilter = whenM (DearImGui.beginCombo "##add-f" "Add filter") do
        forM_ (zip [(0 :: Int) ..] [minBound .. maxBound]) $ \(idx, ft) -> DearImGui.withID idx do
            whenM (DearImGui.selectable (filterName ft)) do
                putStrLn "TODO: add new filter!"
        DearImGui.endCombo
