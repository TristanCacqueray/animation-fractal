module AnimationFractal.Input.Key (
    mkInput,

    -- * export for engine integration
    KeyHandlers,
    newKeyHandlers,
    handleKey,
) where

import Data.Map.Strict qualified as Map
import Frelude

import AnimationFractal.Input
import AnimationFractal.Modulation
import DearImGui qualified
import Engine.Window.Key qualified as Engine (Key (..), KeyState (..))

data KeyHandler = KeyHandler
    { onPress :: IO ()
    , onRelease :: IO ()
    }

newtype KeyHandlers = KeyHandlers (TVar (Map Engine.Key KeyHandler))

newKeyHandlers :: STM KeyHandlers
newKeyHandlers = KeyHandlers <$> newTVar mempty

handleKey :: (MonadIO m) => KeyHandlers -> Engine.KeyState -> Engine.Key -> m ()
handleKey (KeyHandlers kh) state key = do
    handlers <- readTVarIO kh
    case Map.lookup key handlers of
        Just handler
            | state == Engine.KeyState'Pressed -> liftIO handler.onPress
            | state == Engine.KeyState'Released -> liftIO handler.onRelease
            | otherwise -> pure ()
        Nothing -> pure ()

data InputState = WaitingForKey | Ready Engine.Key | Running Engine.Key

data KeyInputConfig = KeyInputConfig
    { key :: Engine.Key
    , modName :: Text
    }
    deriving (Generic, ToJSON, FromJSON)

mkInput :: KeyHandlers -> Maybe Value -> ModulationGroup -> IO Input
mkInput (KeyHandlers kh) keyInputConfigM mg = do
    stateV <- newTVarIO WaitingForKey
    (dstV :: TVar (Maybe Modulation)) <- newTVarIO Nothing

    let applyConfig value = case fromJSON @KeyInputConfig value of
            Success conf -> atomically $ do
                writeTVar stateV (Ready conf.key)
                mods <- modulationGroupList mg
                case filter (\m -> m.name == conf.modName) mods of
                    modulation : _ -> writeTVar dstV (Just modulation)
                    _ -> pure ()
            Error err -> do
                putStrLn $ "Oops, couldn't load key config: " <> show err
        exportConfig = do
            readTVar stateV >>= \case
                Running key -> do
                    modName <- maybe "" (.name) <$> readTVar dstV
                    pure $ Just $ toJSON KeyInputConfig{..}
                _ -> pure Nothing
    traverse_ applyConfig keyInputConfigM

    let writeV v = do
            dstM <- readTVarIO dstV
            case dstM of
                Just dst -> case dst.trigger of
                    Just trigger -> trigger v
                    Nothing -> pure ()
                Nothing -> pure ()
        handler =
            KeyHandler
                { onPress = writeV 1
                , onRelease = writeV 0
                }
        drawRunning key = do
            DearImGui.text (from $ show key)
            DearImGui.sameLine
            drawSingleModSelector mg dstV
        draw = do
            state <- readTVarIO stateV
            case state of
                WaitingForKey -> do
                    drawKeyConfig (atomically . writeTVar stateV . Ready)
                Ready key -> do
                    atomically do
                        modifyTVar' kh (Map.insert key handler)
                        writeTVar stateV (Running key)
                    drawRunning key
                Running key -> drawRunning key
        stop = do
            state <- readTVarIO stateV
            case state of
                Running key -> atomically $ modifyTVar' kh (Map.delete key)
                _ -> pure ()
        advance = const (pure ())
    pure $ Input{..}
  where
    name = "key"
    drawKeyConfig cb = do
        forM_ [Engine.Key'I, Engine.Key'J, Engine.Key'K, Engine.Key'L] $ \key -> do
            keys <- readTVarIO kh
            when (Map.notMember key keys) do
                whenM (DearImGui.button (from $ show key)) do
                    cb key
                DearImGui.sameLine
