module AnimationFractal.Input.PortMidi (mkInput) where

import Data.Map.Strict qualified as Map
import DearImGui qualified
import Frelude
import UnliftIO.Concurrent

import AnimationFractal.Input as AF
import AnimationFractal.Modulation as AF

import Sound.PortMidi.Simple qualified as Midi

data MidiSource
    = ControlChange Int
    | Pitch
    | Velocity
    deriving (Ord, Eq, Generic, ToJSON, FromJSON)

instance DearImGui.ToID MidiSource where
    pushID ms = DearImGui.pushID $ case ms of
        ControlChange x -> x
        Pitch -> -1
        Velocity -> -2

showMidiSource :: MidiSource -> Text
showMidiSource = \case
    ControlChange x -> "cc:" <> from (show x)
    Pitch -> "note"
    Velocity -> "vel"

type MidiMap = Map MidiSource (TVar (Maybe Modulation))

newtype InputConfig = InputConfig [(MidiSource, Text)]
    deriving newtype (ToJSON, FromJSON)

mkInput :: (MonadUnliftIO m) => Maybe Value -> ModulationGroup -> m Input
mkInput configM mg = do
    (midiMapV :: TVar MidiMap) <- newTVarIO mempty

    let applyConfig value = case fromJSON @InputConfig value of
            Success (InputConfig conf) -> atomically do
                mods <- modulationGroupList mg
                newMap <- forM conf $ \(chan, modName) -> do
                    modV <- newTVar (lookupModulation modName mods)
                    pure (chan, modV)
                writeTVar midiMapV (Map.fromList newMap)
            Error err -> do
                putStrLn $ "Oops, couldn't load midi config: " <> show err
        exportConfig = do
            curMap <- readTVar midiMapV
            newMap <- forM (Map.toList curMap) $ \(src, modV) -> do
                modM <- readTVar modV
                pure $ case modM of
                    Just modulation -> Just (src, modulation.name)
                    Nothing -> Nothing
            pure $ Just $ toJSON $ InputConfig $ catMaybes newMap
    traverse_ applyConfig configM

    let makeValue :: Int -> MidiSource -> Float
        makeValue (fromIntegral -> x) _ = x / 127

    let triggerOrAdd :: MidiMap -> (MidiSource, Int) -> IO MidiMap
        triggerOrAdd midiMap (midiSource, value) =
            case Map.lookup midiSource midiMap of
                Nothing -> do
                    modM <- newTVarIO Nothing
                    pure $ Map.insert midiSource modM midiMap
                Just modV -> do
                    modulationM <- readTVarIO modV
                    forM_ modulationM $ \modulation ->
                        case modulation.trigger of
                            Nothing -> putStrLn "no trigger :("
                            Just trig -> trig (makeValue value midiSource)
                    pure midiMap

    let handleEvent :: MidiMap -> (_, Midi.Message) -> IO MidiMap
        handleEvent midiMap (_, ev) = do
            -- putStrLn $ "ev: " <> show ev
            xs <- case ev of
                Midi.Channel 0 (Midi.ControlChange ccNumber ccValue) ->
                    pure [(ControlChange ccNumber, ccValue)]
                Midi.Channel 0 (Midi.NoteOn pitch velocity) ->
                    pure [(Pitch, pitch), (Velocity, velocity)]
                _ -> do
                    putStrLn $ "Unknown event: " <> show ev
                    pure []

            foldM triggerOrAdd midiMap xs

    -- todo: re-use a single client
    thread <- forkIO $ liftIO $ Midi.withMidi do
        Just device <- Midi.findInputNamed "Midi Through Port-0"
        Midi.withInput device \stream -> do
            Midi.withReadMessages stream 256 \readMessages -> do
                forever do
                    -- putStrLn $ "Waiting"
                    Midi.waitInput 10_000 stream
                    msgs <- readMessages
                    midiMap <- readTVarIO midiMapV
                    newMap <- foldM handleEvent midiMap msgs
                    atomically $ writeTVar midiMapV newMap
    let stop = Frelude.throwTo thread (toException StopInput)
        draw = do
            midiMap <- readTVarIO midiMapV
            when (Map.null midiMap) do
                DearImGui.text "Waiting for input..."
            forM_ (Map.toList midiMap) $ \(chan, modM) -> DearImGui.withID chan do
                DearImGui.text (showMidiSource chan)
                DearImGui.sameLine
                drawSingleModSelector mg modM
        advance = const (pure ())
    pure $ Input{..}
  where
    name = "midi-in"
