module AnimationFractal.Input.PulseAudio (mkInput) where

import Control.Concurrent
import Data.ByteString qualified as BS
import Data.Vector.Storable qualified as SV
import Frelude

import AnimationFractal.Input as AF
import AnimationFractal.Input.AudioFilter as AFF
import AnimationFractal.Modulation as AF
import AnimationFractal.Variable as AF

import Pipes (await, runEffect, (>->))
import Pipes.PulseSimple (readPulse)
import Pipes.Safe (runSafeT)
import Sound.Pulse.Simple qualified as PS

data InputConfig = InputConfig
    { gate :: Int32
    }
    deriving (Generic, ToJSON, FromJSON)

mkInput :: (MonadUnliftIO m) => Maybe Value -> ModulationGroup -> m Input
mkInput configM mg = do
    hvar <- newTVarIO newHistoryVar
    source <- newTVarIO 0

    afs <- atomically AFF.mkFilters

    (targetV :: TVar (Maybe Modulation)) <- newTVarIO Nothing

    gateV <- newTVarIO 180
    let controllers =
            [ Variable "gate" (ControllerSliderScallar (tvarState gateV) 0 255) []
            ]

    let applyConfig value = case fromJSON @InputConfig value of
            Success conf -> atomically $ do
                writeTVar gateV conf.gate
            Error err -> do
                putStrLn $ "Oops, couldn't load pulse config: " <> show err
        exportConfig = do
            gate <- readTVar gateV
            pure . Just $ toJSON InputConfig{..}
    traverse_ applyConfig configM

    let consume = forever do
            -- TODO: check if we are reading fast enough...
            samples <- await
            let fsamples :: SV.Vector Float
                fsamples = SV.generate (BS.length samples) \idx ->
                    (128 - fromIntegral (BS.index samples idx)) / 256

            gate <- fromIntegral <$> readTVarIO gateV
            let cur = SV.maximum fsamples
                fgate = gate / 512
            let newv
                    | cur > fgate = cur - fgate
                    | otherwise = 0
            targets <- liftIO $ atomically do
                -- store the current value
                writeTVar source newv
                modifyTVar' hvar (pushHistoryVar newv)
                readTVar targetV

            -- udate targets
            forM_ targets $ \trg -> case trg.trigger of
                Just t -> liftIO do
                    t newv
                Nothing -> pure ()

            -- update filters
            liftIO $ updateFilters afs fsamples

    -- todo: re-use a single client
    thread <- liftIO $ forkIO do
        runSafeT $ runEffect do
            putStrLn "Creating a pulse reader"
            readPulse "animation-fractal-in" (Just sampleFormat) fps >-> consume

    let stop = Frelude.throwTo thread (toException StopInput)
        draw = do
            traverse_ renderVariable controllers
            renderHistoryVar "peak" =<< readTVarIO hvar
            drawSingleModSelector mg targetV
            AFF.drawFilters mg afs
        advance = const (pure ())

    pure $ Input{..}
  where
    name = "pulse-in"

    fps = 25
    sampleFormat = PS.SampleSpec (PS.U8 PS.Raw) 44100 1
