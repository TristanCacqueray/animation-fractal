module AnimationFractal.Input.MidiFile where

import Frelude

import AnimationFractal.Input as AF
import AnimationFractal.Modulation as AF
import AnimationFractal.Time (Frame (..))
import AnimationFractal.Variable as AF
import Data.IntMap.Strict qualified as IM
import Data.Map.Strict qualified as Map
import Data.Vector qualified as V
import Engine.MidiDecoder

data InputConfig = InputConfig
    { mods :: [Text]
    , cap :: Float
    , pulse :: Bool
    }
    deriving (Generic, ToJSON, FromJSON)

mkInputs :: MidiEvents -> [(Text, Maybe Value -> ModulationGroup -> IO Input)]
mkInputs (MidiEvents tracks _) = map (\x@(TrackName track, _) -> (track, mkInput x)) (Map.toList tracks)

mkInput :: (TrackName, IM.IntMap [MidiEvent]) -> Maybe Value -> ModulationGroup -> IO Input
mkInput (TrackName name, events) mConfig mg = do
    (targets, cap, doNoteOff) <- case fromJSON <$> mConfig of
        Nothing -> pure (mempty, 1, False)
        Just (Success (InputConfig mods cap pulse)) -> do
            amods <- atomically $ modulationGroupList mg
            pure (V.fromList $ mapMaybe (flip lookupModulation amods) mods, cap, pulse)
        Just (Error err) -> do
            putStrLn $ "Oops, couldn't load midi config: " <> show err
            pure (mempty, 1, False)

    hvar <- newTVarIO newHistoryVar
    (targetV :: TVar (Maybe Modulation)) <- newTVarIO Nothing

    let exportConfig = pure Nothing

    let stop = pure ()
        draw = do
            renderHistoryVar "peak" =<< readTVarIO hvar
            drawSingleModSelector mg targetV
        advance (Frame position) = do
            let evs = fromMaybe [] $ IM.lookup (unsafeFrom position) events
            let newv = case evs of
                    [] -> 0
                    _ -> 1

            unless (V.null targets) do
                forM_ evs \ev -> case ev of
                    MidiNoteOn pitch vel -> case targets V.!? (pitch `mod` V.length targets) of
                        Nothing -> putStrLn $ "Couldn't find target for " <> show pitch
                        Just target -> forM_ target.trigger (\t -> liftIO $ t (min cap vel))
                    MidiNoteOff pitch
                        | doNoteOff -> case targets V.!? (pitch `mod` V.length targets) of
                            Nothing -> putStrLn $ "Couldn't find target for " <> show pitch
                            Just target -> forM_ target.trigger (\t -> liftIO $ t 0)
                        | otherwise -> pure ()
            mTarget <- atomically do
                modifyTVar' hvar (pushHistoryVar newv)
                readTVar targetV

            -- putStrLn $ "Peak: " <> show peak
            forM_ mTarget \target -> do
                forM_ target.trigger (\t -> liftIO $ t newv)

    pure $ Input{..}
