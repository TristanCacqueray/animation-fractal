-- | A clip defines a single scene element.
--  This module defines how to encode/decode a scene into a clip.
module AnimationFractal.Clip (
    Clip (..),
    newClip,
    clipVarMap,
    showClip,
    VariableDef (..),
    importVariable,
    exportVariable,
    outvarDef,
    ModulationDef (..),
    importModulation,
    exportModulation,
    ModulationTargetDef (..),
    InputDef (..),
    importInput,
    exportInput,
) where

import Data.Aeson qualified as Aeson
import Data.Map.Strict qualified as Map
import Frelude

import AnimationFractal.Input
import AnimationFractal.Modulation
import AnimationFractal.Variable

data Clip = Clip
    { name :: Text
    , vars :: [VariableDef]
    , mods :: [ModulationDef]
    , inps :: [InputDef]
    , next :: Maybe (MSec, Clip)
    }
    deriving (Generic, FromJSON, ToJSON)

newClip :: Text -> Clip
newClip name = Clip name [] [] [] Nothing

clipVarMap :: Clip -> Map Text VariableDef
clipVarMap clip = Map.fromList $ map (\v -> (v.name, v)) clip.vars

showClip :: Clip -> Text
showClip clip = decodeUtf8 (from (encode clip))

data VariableDef = VariableDef
    { name :: Text
    , value :: Value
    }
    deriving (Generic, FromJSON, ToJSON)

exportVariable :: (MonadIO m) => Variable -> m VariableDef
exportVariable var = do
    value <- liftIO $ case var.controller of
        ControllerToggle v -> toJSON <$> get v
        ControllerFloat v _ -> toJSON <$> get v
        ControllerSliderFloat v _ _ -> toJSON <$> get v
        ControllerSliderScallar v _ _ -> toJSON <$> get v
        ControllerVec2 v _ _ -> toJSON <$> get v
        ControllerVec3 v _ _ _ _ _ -> toJSON <$> get v
    pure $ VariableDef var.name value

importVariable :: (MonadIO m) => [Variable] -> VariableDef -> m ()
importVariable vars obj = case lookupVariable obj.name vars of
    Nothing -> putStrLn $ "Unknown var: " <> from obj.name
    Just var -> case var.controller of
        ControllerToggle v -> writeJSON v
        ControllerFloat v o -> writeJSON o >> writeJSON (tvarState v)
        ControllerSliderFloat v _ _ -> writeJSON v
        ControllerSliderScallar v _ _ -> writeJSON v
        ControllerVec2 v _ _ -> writeJSON v
        ControllerVec3 v _ _ _ _ _ -> writeJSON v
  where
    writeJSON :: (MonadIO m) => (FromJSON a) => StateVar a -> m ()
    writeJSON v = liftIO $ case fromJSON obj.value of
        Success a -> v $=! a
        _ -> error $ "Invalid value: " <> show obj.value

data ValueDef = ValueDefBool Bool | ValueDefFloat Float | ValueDefFloats [Float]

valueDef :: Value -> Maybe ValueDef
valueDef = \case
    Aeson.Bool b -> Just $ ValueDefBool b
    Aeson.Number n -> Just $ ValueDefFloat (toRealFloat n)
    v@(Aeson.Array _) -> Just $ ValueDefFloats $ fromJSON' v
    _ -> Nothing

outvarDef :: VariableDef -> [(Text, Float)]
outvarDef varDef = case valueDef varDef.value of
    Just (ValueDefBool b) -> [(name, bool 0 1 b)]
    Just (ValueDefFloat v) -> [(name, v)]
    Just (ValueDefFloats [x, y]) -> [(name <> ".x", x), (name <> ".y", y)]
    Just (ValueDefFloats [x, y, z, _w]) -> [(name <> ".x", x), (name <> ".y", y), (name <> ".z", z)]
    _ -> error $ "Unknown value def: " <> from (decodeUtf8 (from $ encode varDef))
  where
    name = varDef.name

data ModulationDef = ModulationDef
    { name :: Text
    , prog :: Text
    , params :: Maybe Value
    , targets :: [ModulationTargetDef]
    }
    deriving (Generic, FromJSON, ToJSON)

exportModulation :: Modulation -> STM ModulationDef
exportModulation modulation = do
    let name = modulation.name
        prog = modulation.modulationName
    params <- modulation.exportParams
    targets <- traverse exportModulationTarget =<< readTVar modulation.targets
    pure $ ModulationDef{..}

importModulation :: AvailableModulations -> [OutVar] -> ModulationDef -> STM (Maybe Modulation)
importModulation availableModulations ovars obj = case lookup obj.prog availableModulations of
    Nothing -> pure Nothing
    Just mkMod -> do
        modulation <- mkMod obj.params
        targets <- newTVar . catMaybes =<< traverse (importModulationTarget ovars) obj.targets
        pure $ Just $ modulation & (#targets .~ targets) . (#name .~ obj.name)

data ModulationTargetDef = ModulationTargetDef
    { name :: Text
    , gain :: Float
    , inverse :: Bool
    , op :: ModulationOP
    }
    deriving (Generic, FromJSON, ToJSON)

exportModulationTarget :: ModulationTarget -> STM ModulationTargetDef
exportModulationTarget mt = do
    let name = mt.ovar.name
    gain <- readTVar mt.gain
    inverse <- readTVar mt.inverse
    op <- readTVar mt.op
    pure $ ModulationTargetDef{..}

importModulationTarget :: [OutVar] -> ModulationTargetDef -> STM (Maybe ModulationTarget)
importModulationTarget ovars mt = case lookupOutVar mt.name ovars of
    Nothing -> pure Nothing
    Just ovar -> do
        gain <- newTVar mt.gain
        inverse <- newTVar mt.inverse
        op <- newTVar mt.op
        pure $ Just $ ModulationTarget{..}

data InputDef = InputDef
    { name :: Text
    , config :: Maybe Value
    }
    deriving (Generic, FromJSON, ToJSON)

exportInput :: Input -> STM InputDef
exportInput input = InputDef input.name <$> input.exportConfig

importInput :: (MonadIO m) => AvailableInputs -> ModulationGroup -> InputDef -> m (Maybe Input)
importInput availableInputs mg obj = case lookup obj.name availableInputs of
    Nothing -> pure Nothing
    Just mkInput -> Just <$> (liftIO $ mkInput obj.config mg)
