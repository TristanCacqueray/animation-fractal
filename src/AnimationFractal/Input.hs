module AnimationFractal.Input (
    Input (..),
    renderInput,
    renderInputs,
    renderAddInputButton,
    drawModSelector,
    drawSingleModSelector,
    AvailableInputs,
    ModulationWorker (..),
    StopInput (..),
) where

import Frelude

import DearImGui qualified

import AnimationFractal.Modulation
import AnimationFractal.Time (Frame)

data Input = Input
    { name :: Text
    , draw :: IO ()
    , stop :: IO ()
    , advance :: Frame -> IO ()
    , exportConfig :: STM (Maybe Value)
    }

drawModSelector :: (MonadUnliftIO m) => ModulationGroup -> Text -> (Maybe Modulation -> m ()) -> m ()
drawModSelector mg n cb = whenM (DearImGui.beginCombo "##mod-target" n) do
    mods <- atomically (modulationGroupList mg)
    forM_ (zip [(0 :: Int) ..] mods) $ \(idx, modulation) -> DearImGui.withID idx $ case modulation.trigger of
        Just _ -> whenM (DearImGui.selectable modulation.name) do
            cb (Just modulation)
        Nothing -> pure ()
    whenM (DearImGui.selectable "Remove") do
        cb Nothing
    DearImGui.endCombo

drawSingleModSelector :: (MonadUnliftIO m) => ModulationGroup -> TVar (Maybe Modulation) -> m ()
drawSingleModSelector mg dstMV = do
    destM <- readTVarIO dstMV
    case destM of
        Just n -> drawModSelector mg n.name cb
        Nothing -> drawModSelector mg "Pick a target" cb
  where
    cb = atomically . writeTVar dstMV

renderInput :: (MonadUnliftIO m) => TVar [Input] -> (Int, Input) -> m ()
renderInput inputsV (idx, input) = DearImGui.withID input.name $ DearImGui.withID idx do
    whenM (DearImGui.button "x") do
        liftIO input.stop
        atomically $ modifyTVar' inputsV $ \xs -> take idx xs <> drop (idx + 1) xs
    DearImGui.sameLine
    liftIO input.draw

renderInputs :: (MonadUnliftIO m) => TVar [Input] -> m ()
renderInputs inputsV = do
    DearImGui.spacing
    void $! DearImGui.textColored dearYellow "Inputs:"
    inputs <- readTVarIO inputsV
    traverse_ (renderInput inputsV) (reverse $ zip [0 ..] inputs)

renderAddInputButton :: (MonadUnliftIO m) => AvailableInputs -> ModulationGroup -> TVar [Input] -> m ()
renderAddInputButton avail mg inputs = do
    DearImGui.spacing
    whenM (DearImGui.beginCombo "##add-inp" "Add input") do
        forM_ avail $ \(name, mkInput) -> do
            whenM (DearImGui.selectable name) do
                input <- liftIO (mkInput Nothing mg)
                atomically $ modifyTVar' inputs (input :)
        DearImGui.endCombo

type AvailableInputs = [(Text, Maybe Value -> ModulationGroup -> IO Input)]

data ModulationWorker = ModulationWorker
    { worker :: ThreadId
    , modulation :: Modulation
    }

data StopInput = StopInput
    deriving (Show)

instance Exception StopInput
