module AnimationFractal.Time where

import Frelude
import Numeric (showFFloat)

newtype MSec = MSec Double
    deriving newtype (Num, Show, Real, Ord, Eq, Fractional, Enum, FromJSON, ToJSON)

showMSec :: MSec -> Text
showMSec (MSec msec) = from (showFFloat (Just 1) sec "")
  where
    sec = msec / 1000

getMonotonicTimeMS :: (MonadIO m) => m MSec
getMonotonicTimeMS = do
    t <- getMonotonicTime
    pure $ MSec (t * 1_000)

newtype Frame = Frame Word
    deriving newtype (Num, Show, Real, Ord, Eq, Enum, FromJSON, ToJSON)

instance From Frame Int where
    from (Frame x) = fromIntegral x

instance From Frame Float where
    from (Frame x) = fromIntegral x

instance From Int Frame where
    from = Frame . unsafeFrom

instance From Natural Frame where
    from = Frame . unsafeFrom

time2Frame :: MSec -> Frame
time2Frame (MSec ms) = Frame (round $ ms / fpms)

frame2time :: Frame -> MSec
frame2time (Frame f) = MSec (fromIntegral f * fpms)

frameSub :: Frame -> Frame -> Frame
frameSub x y
    | x > y = 0
    | otherwise = y - x

frameAdd :: Frame -> Frame -> Frame
frameAdd = (+)

frameLength :: MSec
frameLength = MSec fpms

-- TODO: make this configurable
fpms :: Double
fpms = 1000 / 60

newtype BPM = BPM Word
