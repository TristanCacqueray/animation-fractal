module DearImGui.Extra where

import Frelude

import DearImGui qualified

startTreeOpen :: TVar Bool -> IO ()
startTreeOpen tv =
    readTVarIO tv >>= \case
        False -> pure ()
        True -> do
            DearImGui.setNextItemOpen True
            atomically $ writeTVar tv False

renderTree :: Text -> IO a -> IO (Maybe a)
renderTree name action =
    DearImGui.treeNode name >>= \case
        False -> pure Nothing
        True -> do
            res <- action
            DearImGui.treePop
            pure (Just res)
