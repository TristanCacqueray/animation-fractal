module Demo.Kaleidoscope2 where

import AnimationFractal.Scene as AF
import Demo.MicroOrg
import Frelude

sceneInfo :: (Text, IO SceneInfo)
sceneInfo = keys $ AF.withFragPath "Kaleidoscope2.glsl" $ AF.mkSceneInfo "kalei2" fragCode [mkClip itracks] vars
  where
    keys = AF.withKeyframes [1200, 2300, 4000, 6100, 9000]
    vars =
        [ AF.newFloatVar "move" Nothing #var1
        , AF.newFloatVar "rot1" Nothing #var2
        , AF.newFloatVar "rot2" Nothing #var3
        , AF.newFloatVar "shape1" Nothing #var4
        , AF.newFloatVar "mod1" Nothing #var5
        , AF.newFloatVar "mod2" Nothing #var6
        , AF.newFloatVar "color" Nothing #extraVar1
        ]

    itracks =
        [ TrackInfo "opCBC3-rhode" "move" 0 [FilterConfig "m" BandSkirt 5800 40 (Just "color")]
        , TrackInfo "opCBC3-hats" "shape1" 0 []
        , TrackInfo "opCBC3-snare" "rot1" 0 []
        , TrackInfo "opCBC3-kick" "rot2" 0 []
        , TrackInfo "opCBC3-bass" "mod2" 0 []
        ]
