module Demo.Kaleidoscope1 where

import AnimationFractal.Scene as AF
import Demo.MicroOrg
import Frelude

sceneInfo :: (Text, IO SceneInfo)
sceneInfo = AF.withFragPath "Kaleidoscope1.glsl" $ AF.mkSceneInfo "kalei1" fragCode [mkClip itracks] vars
  where
    vars =
        [ AF.newFloatVar "move" Nothing #var1
        , AF.newFloatVar "rot1" Nothing #var2
        , AF.newFloatVar "shape1" Nothing #var3
        , AF.newFloatVar "shape2" Nothing #var4
        , AF.newFloatVar "mod1" Nothing #var5
        , AF.newFloatVar "mod2" Nothing #var6
        , AF.newFloatVar "color" Nothing #extraVar1
        ]

    itracks = [TrackInfo "pulse-in" "rot1" 0 []]

    _tracks =
        [ TrackInfo "opRecharge-kick" "rot1" 42 [FilterConfig "1" HighPass 16030 6 (Just "shape1")]
        , TrackInfo "opRecharge-hats" "mod1" 5 []
        , TrackInfo "opRecharge-bass" "shape2" 40 [FilterConfig "3" BandSkirt 900 6 (Just "mod2")]
        , TrackInfo "opRecharge-melo" "move" 120 [FilterConfig "4" LowPass 80 6 (Just "color")]
        ]
