module Demo.Blank where

import AnimationFractal.Scene as AF
import Frelude

sceneInfo :: (Text, IO SceneInfo)
sceneInfo = AF.mkSceneInfo "blank" fragCode [defSeq] vars
  where
    vars = [AF.newFloatVar "time" Nothing #var1]
    defSeq =
        newClip "default"
            & #mods
            .~ [ ModulationDef "iTime" "iTime" Nothing [ModulationTargetDef "time" 1.0 False ModAdd]
               , ModulationDef "pulse" "pulseAD" Nothing [ModulationTargetDef "time" 1.0 False ModAdd]
               , ModulationDef "lfo" "lfo" Nothing []
               ]

fragCode :: Code
fragCode =
    fromString
        [glsl|
    #version 450

    ${set0binding1}

    #define iTime scene.var1

    layout (location = 0) in vec2 inUV;
    layout (location = 0) out vec4 oColor;

    void main() {
      // Pixel coordinate in the [0..1] range
      vec2 uv = inUV;

      // Time varying pixel color
      vec3 col = 0.5 + 0.5*cos(iTime+uv.xyx+vec3(0,2,4));

      oColor = vec4(col, 1.0);
    }
  |]
