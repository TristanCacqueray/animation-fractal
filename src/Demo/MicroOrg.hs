-- Render with: nixVulkanIntel  cabal run -O2 animation-fractal -- --prog upOrg --media ~/reaper/microX/microX.mp3 --media ~/reaper/microX/microX-voice.mp3 --media ~/reaper/microX/microX-bip.mp3 --media ~/reaper/microX/microX-melo.mp3 --media ~/reaper/microX/microX-kick.mp3 --media ~/reaper/microX/microX-hats.mp3 --media ~/reaper/microX/microX-bird.mp3 --end 15700
module Demo.MicroOrg where

import AnimationFractal.Scene as AF
import Frelude
import GLSL

data TrackInfo = TrackInfo
    { trackName :: Text
    , modName :: Text
    , time :: Int
    , filters :: [FilterConfig]
    }

sceneInfo :: (Text, IO SceneInfo)
sceneInfo = AF.withFragPath "MicroOrg.glsl" $ AF.mkSceneInfo "upOrg" fragCode [clip] vars
  where
    vars =
        [ AF.newFloatVar "voice" Nothing #var1
        , AF.newFloatVar "bip" Nothing #var2
        , AF.newFloatVar "moveFWD" Nothing #var3
        , AF.newFloatVar "rot1" Nothing #var4
        , AF.newFloatVar "hats" Nothing #var5
        , AF.newFloatVar "bird" Nothing #var6
        ]

    baseClip = mkClip tracks
    clip = baseClip{inps = audioOut : baseClip.inps}

    audioOut =
        InputDef "audio-out"
            $ Just
            $ toJSON
            $ object
                [ "gain" .= (0.003 :: Float)
                , "modName" .= ("bird" :: Text)
                , "filters"
                    .= [ FilterConfig "l" LowPass 300 6 (Just "rot1") -- The filter defines how to update the variables
                       , FilterConfig "m" BandPass 5800 40 (Just "moveFWD")
                       , FilterConfig "s" BandSkirt 5357 4 (Just "bip")
                       , FilterConfig "s" BandSkirt 916 3 (Just "voice")
                       , FilterConfig "h" HighPass 12000 8 (Just "hats")
                       ]
                ]

    tracks =
        [ TrackInfo "microX-voice" "voice" 200 []
        , TrackInfo "microX-bip" "bip" 150 []
        , TrackInfo "microX-melo" "moveFWD" 600 []
        , TrackInfo "microX-kick" "rot1" 140 []
        , TrackInfo "microX-hats" "hats" 50 []
        , TrackInfo "microX-bird" "bird" 100 []
        ]

mkClip :: [TrackInfo] -> Clip
mkClip tis = Clip "default" (map mkVariableDef tis) (concatMap mkModulationDef tis) (map mkInputDef tis) Nothing
  where
    mkInputDef :: TrackInfo -> InputDef
    mkInputDef ti = InputDef ti.trackName $ Just $ object ["gain" .= (1 :: Int), "gate" .= (0 :: Int), "modName" .= ti.modName, "filters" .= ti.filters]
    mkVariableDef :: TrackInfo -> VariableDef
    mkVariableDef ti = VariableDef ti.modName (toJSON (0 :: Int))
    mkModulationDef :: TrackInfo -> [ModulationDef]
    mkModulationDef ti = ModulationDef ti.modName mProg (Just params) targets : mapMaybe filterMod ti.filters
      where
        mProg = "sec-ord"
        filterMod f = do
            fModName <- f.modName
            pure $ ModulationDef fModName "incr" (Just params) [ModulationTargetDef fModName 1 False ModAdd]
        params = toJSON [ti.time]
        targets = [ModulationTargetDef ti.modName 1 False ModAdd]

fragCode :: Code
fragCode =
    fromString
        [glsl|
    #version 450

    ${set0binding1}
    ${palettes}

    layout (location = 0) in vec2 inUV;
    layout (location = 0) out vec4 oColor;
  |]
