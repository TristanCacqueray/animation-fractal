// https://www.shadertoy.com/view/ttKGDt
// Phantom Star  Created by kasari39 in 2020-01-28
// This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

#define imove   (1 + -.1 * scene.var1)
#define irot1   (.3 * scene.var2)
#define irot2   (-.6 * scene.var3)
#define ishape1 (2.5 * scene.var4)
#define imod1   (.1 * scene.var5)
#define imod2   (-.1 * scene.var6)
#define icolor  (.2 * scene.evar1)

#define iTime   (imove * .3)
#define PI          3.141592654

precision highp float;

mat2 rot(float a) {
    a -= sin(imove * .4) * .6;
    float c = cos(a), s = sin(a);
    return mat2(c,s,-s,c);
}

const float pi = acos(-1.0);
const float pi2 = pi*2.0;

vec2 pmod(vec2 p, float r) {
    // r *= (2.6 + 2.4 * sin(imod2*.5));
    float a = atan(p.x, p.y) + pi/r;
    float n = pi2 / r;
    a = floor(a/n)*n;
    return p*rot(-a - imod2*.2);
}

float box( vec3 p, vec3 b ) {
    vec3 d = abs(p) - b;
    return min(max(d.x,max(d.y,d.z)),0.0) + length(max(d,0.0));
}

float fSphere(vec3 p, float r) {
    return length(p) - r;
}

float ifsBox(vec3 p) {
    for (int i=0; i<5; i++) {
        p = abs(p) - 1.0;
        p.xy *= rot(ishape1 * 0.1);
        p.xz *= rot(ishape1 * 0.08 + imod2 * 0.01);
    }
    // p.xz *= rot(iTime + irot2*0.5);
    float sz = .1 * sin(imod2*.05);
    float fs = box(p, (sz + .9) * vec3(0.2,0.1,.9));
    float fb = box(p, (sz + 1.2) * vec3(0.1,0.5,0.2)); // * (0.9 + 0.4 * sin(-.0 * irot1)));
    return mix(fb, fs, 0.45 + 0.4*sin(irot1*4)) ;
    // return
}

float map(vec3 p, vec3 cPos) {
    vec3 p1 = p;
    p1.x = mod(p1.x-5., 10.) - 5.;
    p1.y = mod(p1.y-5., 10.) - 5.;
    p1.z = mod(p1.z-8,  16.) - 8.;
    p1.xy = pmod(p1.xy, 12 + 11 * (0.5 + 0.5 * sin(irot1)));

    return ifsBox(p1) * (1.5 + 0.5 * cos(imod2*.1));
}

void main() {
    vec2 p = -1 + 2 * inUV; // (fragCoord.xy * 2.0 - iResolution.xy) / min(iResolution.x, iResolution.y);

    vec3 cPos = vec3(0, 0, -2.1*iTime);
    // vec3 cPos = vec3(0, 0, -6.0 * iTime + irot1 * -.2);
    // vec3 cPos = vec3(0.3*sin(iTime*0.8), 0.4*cos(iTime*0.3), -6.0 * iTime);
    vec3 cDir = vec3(0.0, 0.0, 1);
    vec3 cUp  = vec3(sin(imod2*.1), 1.0, 0.0);
    vec3 cSide = cross(cDir, cUp);

    vec3 ray = normalize(cSide * p.x + cUp * p.y + cDir);
    ray.xy *= rot(irot2*.1);
    // Phantom Mode https://www.shadertoy.com/view/MtScWW by aiekick
    float acc = 0.0;
    float acc2 = 0.0;
    float t = 0.0;
    vec3 pos = vec3(0.);
    for (int i = 0; i < 80; i++) {
        pos = cPos + ray * t;
        float dist = map(pos, cPos);
        dist = max(abs(dist), 0.02);
        float a = exp(-dist*4.0);
        if (t > 3) {
        if (mod(length(pos)+(12.0)*imove, 37.0) < 3.0) {
            a *= 2 + float(i) / 10;
            acc2 += a;
        }
        acc += a;
        }
        t += dist * 0.5;
    }

    // vec3 col = vec3(acc * 0.02 + 0.1 * sin(icolor), acc * 0.001, acc * 0.001 + acc2 * acc2 * 0.01);
    vec3 col = vec3(acc * 0.02 + 0.05 * sin(icolor), acc * 0.001, acc * 0.001);
    vec3 gcol = vec3(0, acc2*0.04, acc2*acc2*0.02);

    col = clamp(col, 0.0, 1.0);
    gcol = clamp(gcol, 0.0, 1.0);
    col += gcol;
    // col = pow(col, vec3(1.0/2.2));
    // col = col*0.6+0.4*col*col*(3.0-2.0*col);
    // col = mix(col, vec3(dot(col, vec3(0.33))), -0.4);

    // col *=0.5+0.5*pow(.01*pos.x*pos.y*(1.0-pos.x)*(1.0-pos.y),0.1);
    // col = pow(col, vec3(1.5, 1.2, 1.));
    col = pow(col, vec3(6));
    oColor = vec4(col, 1.0);
}
