-- | Render with:
-- nixVulkanIntel cabal run -O2 animation-fractal -- --prog juicer  --media  ~/reaper/Documents/REAPER\ Media/opJuicer.mp3  --media  ~/reaper/Documents/REAPER\ Media/opJuicer-bass.mp3  --media  ~/reaper/Documents/REAPER\ Media/opJuicer-drums.mp3 --media  ~/reaper/Documents/REAPER\ Media/opJuicer-rev.mp3  --render opJuicer-video.mp4 --end 9600
module Demo.Juicer where

import AnimationFractal.Scene as AF
import Frelude

sceneInfo :: (Text, IO SceneInfo)
sceneInfo = AF.mkSceneInfo "juicer" fragCode [defSeq] vars
  where
    vars =
        [ AF.newFloatVar "bass" Nothing #var1
        , AF.newFloatVar "drum" Nothing #var2
        , AF.newFloatVar "rev" Nothing #var3
        ]
    defSeq =
        fromJSON'
            [aesonQQ|
  {
    "inps": [
      { "config": { "gain": 1, "gate": 0, "modName": "bass" },
        "name": "opJuicer-bass"
      },
      { "config": { "gain": 1, "gate": 0, "modName": "drum" },
        "name": "opJuicer-drums"
      },
      { "config": { "gain": 1, "gate": 0, "modName": "rev" },
        "name": "opJuicer-rev"
      }
    ],
    "mods": [
      { "name": "rev", "params": null, "prog": "incr", "targets": [
          { "gain": 1.0, "inverse": false, "name": "rev", "op": "ModAdd" }
        ]
      },
      { "name": "drum", "params": [124], "prog": "incr", "targets": [
          { "gain": 1.762, "inverse": false, "name": "drum", "op": "ModAdd" }
        ]
      },
      { "name": "bass", "params": [94], "prog": "incr", "targets": [
          { "gain": 2.5, "inverse": false, "name": "bass", "op": "ModAdd" }
        ]
      }
    ],
    "name": "default",
    "next": null,
    "vars": [
      { "name": "bass", "value": 0 },
      { "name": "drum", "value": 0 },
      { "name": "rev", "value": 0 }
    ]
  }
|]

fragCode :: Code
fragCode =
    fromString
        [glsl|
    #version 450

    ${set0binding1}

    #define bass scene.var1
    #define drum scene.var2
    #define rev scene.var3

    layout (location = 0) in vec2 inUV;
    layout (location = 0) out vec4 oColor;

// https://iquilezles.org/articles/palettes/
vec3 palette( float t ) {
    vec3 a = vec3(0.5, 0.5, 0.5);
    vec3 b = vec3(0.5, 0.5, 0.5);
    vec3 c = vec3(1.0, 1.0, 1.0);
    vec3 d = vec3(0.3, 0.4, 0.5);

    return a + b*cos( 6.28318*(c*t+d) );
}

// https://www.shadertoy.com/view/mtyGWy
void main() {
      vec2 p = (inUV - 0.5) * 1.9;
      vec2 uv0 = p;
      float bassIncr = 1.47 + bass + rev * 20.;
      float t = bassIncr * .1;

      vec3 c = vec3(0.);
      for(int i=0;i<5;i++) {
        p = fract(p * (1.5 * (.7 + 1.8 * rev))) - .5;
        float d = length(p) * exp(-length(uv0));
        vec3 col = palette(length(uv0) + i*.5 + drum*.4);
        d = sin(d*8. + bassIncr * .1)/8.;
        d = abs(d);
        d = pow(0.001 / d, 1.7);

        c += d * col;
      }
      oColor = vec4(c,1.);
}
  |]
