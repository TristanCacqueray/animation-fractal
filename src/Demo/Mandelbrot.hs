module Demo.Mandelbrot where

import AnimationFractal.Scene
import Frelude
import GLSL (complex, palettes, turboColormap)

sceneInfo :: (Text, IO SceneInfo)
sceneInfo = mkSceneInfo "mandelbrot" fragCode clips vars
  where
    vars =
        [ newVec2Var "origin" #origin
        , newFloatVar "zoom" Nothing #zoom
        , newSciVar "pow" #var1
        , newIntVar "max-iter" #varInt1
        , newBoolVar "burning ship" #varBool1
        , newFloatVar "intensity" Nothing #var2
        , -- line trap
          newVec2Var "line-trap" #pos1v2
        , newFloatVar "line-limit" (Just (0, 1)) #var3
        , -- shape trap
          newVec2Var "shape-trap" #pos2v2
        , newFloatVar "shape-mix" (Just (0, 1)) #var4
        ]
    m1 = vec2 -1.48 0
    clips =
        [ mkClip "default" 0.5 m1 42 False
        , mkClip "ship" 18.70 m1 59 True
        , mkClip "spiral" 96.5 (vec2 -0.81812817 0.19885011) 205 False
        ]

    mkClip name zoom origin maxIter burningShip =
        newClip name
            & ( #vars
                    .~ [ VariableDef "zoom" (toJSON @Float zoom)
                       , VariableDef "origin" (toJSON @Vec2 origin)
                       , VariableDef "max-iter" (toJSON @Int maxIter)
                       , VariableDef "burning ship" (toJSON @Bool burningShip)
                       , VariableDef "pow" (toJSON @Int 2)
                       , VariableDef "intensity" (toJSON @Int 1)
                       , VariableDef "line-limit" (toJSON @Float 0.99)
                       , VariableDef "shape-mix" (toJSON @Float 0.0)
                       ]
              )

fragCode :: Code
fragCode =
    fromString
        [glsl|
    #version 450

    ${set0binding1}
    ${complex}
    ${turboColormap}
    ${palettes}

    #define MAX_ITER scene.varInt1
    #define MPOW scene.var1
    #define BSHIP scene.varBool1

    #define intensity scene.var2

    #define lineTrap scene.pos1v2
    #define lineLimit scene.var3
    #define shapeTrap scene.pos2v2
    #define shapeMix scene.var4

    layout (location = 0) in vec2 inUV;
    layout (location = 0) out vec4 oColor;

float dot2( in vec2 v ) { return dot(v,v); }
float sdHeart( in vec2 p )
{
    p.x = abs(p.x);

    if( p.y+p.x>1.0 )
        return sqrt(dot2(p-vec2(0.25,0.75))) - sqrt(2.0)/4.0;
    return sqrt(min(dot2(p-vec2(0.00,1.00)),
                    dot2(p-0.5*max(p.x+p.y,0.0)))) * sign(p.x-p.y);
}

vec2 opRep( in vec2 p, in float s )
{
    return mod(p+s*0.5,s)-s*0.5;
}

    vec3 mandelbrot_color(vec2 coord) {
      vec2 z = vec2(0.0);
      vec2 c = coord;

      vec3 traps = vec3(1e20);

      int idx;
      for (idx = 0; idx < MAX_ITER; idx += 1) {
        z = c_pow(z, MPOW) + c;

        // when abs_imag controller is on:
        if (BSHIP) {
          z.y = abs(z.y);
        }

        // update traps
        float lineRep = 2;
        vec2 line = vec2(sin(lineTrap.x / lineRep) * lineRep, sin(lineTrap.y / lineRep) * lineRep);
        traps = min(traps, vec3(
          min(length(line.x + z.x), length(line.y + z.y)),
          sdHeart(opRep((shapeTrap.xy + z) * 2.0, 6.0)),
          length(fract(z) - 0.5)
        ));
        if (dot(z, z) > 500.0) {
          break;
        }
      }

      vec3 base_color = vec3(0);
      if (idx < MAX_ITER) {
        float ci = idx - log2(log2(dot(z,z))) + 4.0;
        ci /= MAX_ITER;
        base_color = getColor(ci * intensity);
      }

      vec3 color = base_color;
      // return color;

      vec3 trap_color;
      float trap_mix;

      // line trap
      trap_color = getColor(traps.x); // vec3(1.0, 0.0, 0.0);
      trap_mix = clamp(1 - traps.x, 0, 1);
      trap_mix = smoothstep(lineLimit, 1.0, trap_mix);
      color = mix(color, trap_color, trap_mix);

      // shape trap
      float shape_ci = clamp(0.1, 0.5, traps.y);
      vec3 shape_color = getColor(shape_ci * 0.1); // vec3(0.8, 0.2, 0.1);
      color = mix(color, shape_color, shapeMix);

      // trap3
      color = mix(color, vec3(0.95, 0.3, 0.1), traps.z);

      return color;
    }

    #define AA 2

    void main() {
      vec3 col = vec3(0.0);
      vec2 screenResolution = vec2(scene.screenResolution);
      for (int m = 0; m < AA; m++) {
      for (int n = 0; n < AA; n++) {
        // super sample
        vec2 offset = vec2(m, n) / screenResolution / AA;

        // Adjust position to [-1.0..1.0]
        vec2 uv = (inUV + offset - 0.5) * 2;

        // Take into account the screen ratio
        uv.y = uv.y * scene.screenRatio;

        // Adjust to the scene center and zoom
        uv = scene.origin + uv * (1.0 / scene.zoom);
        col += mandelbrot_color(uv);
      }}
      col = col / float(AA * AA);

      // gamma + vignetting
      vec2 glUV = (inUV - 0.5) * 2.0;
      glUV.y = glUV.y * scene.screenRatio;
      col *= 1.4 - 0.2 * length(glUV);

      oColor = vec4(col, 1.0);
    }
  |]
