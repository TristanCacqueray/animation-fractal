-- | Run `ghcid --test Demo.SummerDawn.main`
module Demo.SummerDawn (main) where

import Control.Varying.Core qualified as Varying
import Control.Varying.Tween qualified as Varying
import Data.Map.Strict qualified as Map
import Data.Vector.Generic (generateM)
import Frelude

import Demo.Mandelbulb qualified
import Engine.MidiDecoder qualified as Midi

import AnimationFractal.Modulation
import AnimationFractal.Scene
import AnimationFractal.Script
import AnimationFractal.Time
import AnimationFractal.Variable

easeOut :: MSec -> Float -> Float -> VaryingVar
easeOut duration current value = Varying.tweenStream tween value
  where
    tween = Varying.tween Varying.easeOutCubic current value duration

pulseMod :: Modulation -> Float -> Float -> IO ()
pulseMod modulation value base = atomically do
    writeTVar modulation.source (easeOut 5000 (base + value) base)

pulseMod' :: Modulation -> Float -> Float -> IO ()
pulseMod' modulation value base = atomically do
    writeTVar modulation.source (easeOut 1800 (base + value) base)

incrMod :: Modulation -> MSec -> Float -> IO ()
incrMod modulation duration value = atomically do
    current <- (.value) <$> readTVar modulation.hvar
    writeTVar modulation.source (easeOut duration current (current + value))

incrSpeed :: TVar VaryingVar -> MSec -> Float -> IO ()
incrSpeed counterV duration value = atomically do
    counter <- readTVar counterV
    (current, _) <- Varying.runVarT counter 0
    writeTVar counterV (easeOut duration current (current + value))

linMod :: Modulation -> MSec -> Float -> IO ()
linMod modulation duration value = atomically do
    current <- (.value) <$> readTVar modulation.hvar
    writeTVar modulation.source (linear duration current value)

linSpeed :: TVar VaryingVar -> MSec -> Float -> IO ()
linSpeed counterV duration value = atomically do
    counter <- readTVar counterV
    (current, _) <- Varying.runVarT counter 0
    writeTVar counterV (linear duration current (current + value))

hasNote :: (Int -> Bool) -> [Midi.MidiEvent] -> Bool
hasNote p xs = any checkNote xs
  where
    checkNote = \case
        Midi.MidiNoteOn x _ -> p x
        _ -> False

main :: IO ()
main = do
    midiEvents <- Midi.decodeMidiFile Nothing midiFile
    let
        -- Add 30 seconds after the last event.
        maxFrame = time2Frame (30_000) + Frame midiEvents.endFrame
    putStrLn $ "total frame: " <> show maxFrame

    scene <- snd Demo.Mandelbulb.sceneInfo
    traverse_ (importVariable scene.vars) startingVars

    -- setup script modulation
    let outVars = sceneMods scene
        newSceneModulation outVar = do
            startingValue <- get outVar.current
            atomically do
                modulation <- newVarModulation outVar.name outVars
                writeTVar modulation.source (Varying.done startingValue)
                modifyTVar' modulation.hvar (pushHistoryVar startingValue)
                pure modulation

    mods <- traverse newSceneModulation $ sceneMods scene
    let getModulation name = fromMaybe (error "unknown var") $ lookupModulation name mods
        seedX = getModulation "seed.x"
        seedY = getModulation "seed.y"
        seedZ = getModulation "seed.z"
        zoom = getModulation "zoom"
        intensity = getModulation "intensity"
        minDist = getModulation "min-dist"
        pow = getModulation "pow"
        yaw = getModulation "yaw"
        pitch = getModulation "pitch"
        glow = getModulation "glow"

    seedXSpeed <- newTVarIO (-0.1)
    atomically $ writeTVar seedX.source (sinSpeed 0 6 seedXSpeed)
    seedYSpeed <- newTVarIO (0.05)
    atomically $ writeTVar seedY.source (sinSpeed 0 6 seedYSpeed)
    seedZSpeed <- newTVarIO (-0.1)
    atomically $ writeTVar seedZ.source (sinSpeed 0 6 seedZSpeed)

    yawSpeed <- newTVarIO 0
    atomically $ writeTVar yaw.source (sinSpeed 0 (pi / 4) yawSpeed)
    pitchSpeed <- newTVarIO 0
    atomically $ writeTVar pitch.source (sinSpeed 0 (pi / 4) pitchSpeed)

    -- setupScene
    startingValues <- forM (sceneMods scene) $ \outVar -> do
        value <- get outVar.current
        pure $ (outVar.name, value)

    let handleEvents :: Frame -> (Midi.TrackName, _) -> IO ()
        handleEvents frame (track, xs)
            | -- fade out
              frame >= 12141 = do
                when lulaby do
                    incrSpeed seedXSpeed 500 -0.018
                    incrSpeed seedZSpeed 500 -0.027
                    incrMod pow 2000 0.5
                    incrMod intensity 250 0.1
                    incrMod seedY 250 0.1
                when nappe do
                    incrMod minDist 2000 0.015
                    incrMod zoom 2000 -0.05
            | -- outro
              frame >= 8750 = do
                when kick do
                    pulseMod seedY -1 1.194
                when lulaby do
                    incrSpeed seedXSpeed 500 -0.02
                when bass do
                    incrSpeed seedZSpeed 300 0.013
                when hats do
                    incrMod intensity 550 -0.1
                when snare do
                    incrMod minDist 250 -0.00001
                when nappe $ incrMod pow 2000 -0.2
            | -- pre-outro =
              frame >= 8152 = do
                when lulaby do
                    incrSpeed seedXSpeed 500 0.01
                    incrMod minDist 300 -0.1e-3
                    incrMod seedY 300 0.03
                when kick do
                    incrSpeed seedZSpeed 300 -0.011
                when snare do
                    pulseMod' glow 0.1 0
            | -- p3
              frame >= 6494 = do
                when lulaby do
                    incrSpeed seedXSpeed 500 0.018
                when kick do
                    incrSpeed pitchSpeed 1500 0.34
                    pulseMod' seedY -3 1.194
                when snare do
                    pulseMod' glow 0.1 0
                    pulseMod pow 1 8.879
                    incrSpeed seedZSpeed 700 -0.04
                    incrMod intensity 500 0.1
            | -- brk2
              frame >= 5929 = do
                when lulaby $ incrSpeed seedXSpeed 500 0.2
                when kick $ incrMod pow 2000 -0.5
                when bass do
                    incrSpeed seedZSpeed 500 0.2
                -- incrMod minDist 1000 0.005

                when nappe $ incrMod intensity 500 -1
            | -- p2
              frame >= 4230 = do
                when bass do
                    incrSpeed seedXSpeed 280 0.04
                when kick do
                    pulseMod' seedY -3.5 1.194
                -- incrSpeed seedYSpeed 180 0.2
                when snare do
                    pulseMod' glow 0.2 0
                    incrSpeed seedZSpeed 500 -0.02
                    incrMod intensity 500 0.2
                    incrSpeed pitchSpeed 2500 0.34
                    incrSpeed yawSpeed 2500 -0.01
            | -- p1
              frame >= 2540 = do
                when (frame >= 3700 && bass) $ incrMod pow 800 0.5
                case track of
                    "lulaby" -> do
                        when (hasNote (>= 87) xs) do
                            incrSpeed seedYSpeed 360 0.05
                        -- incrMod yaw 2000 0.3
                        pure ()
                    "bass" -> do
                        incrSpeed seedXSpeed 250 -0.04
                        incrSpeed seedZSpeed 250 -0.03
                    "hats" -> incrMod intensity 300 0.02
                    "waldo A61" -> incrMod pow 3000 0.2
                    _ -> pure ()
            | frame >= 1410 = case track of
                "lulaby" -> incrSpeed seedYSpeed 360 -0.03
                "waldo A61" -> incrSpeed seedZSpeed 5060 -0.04
                "bass" -> incrSpeed seedXSpeed 3000 -0.1
                "hats" -> incrMod intensity 300 0.04
                _ -> pure ()
            | -- intro
              otherwise = case track of
                "lulaby" -> do
                    incrSpeed seedZSpeed 360 0.004
                    when (hasNote (>= 80) xs) do
                        incrSpeed seedXSpeed 500 0.01
                "lulaoverlay" -> incrMod intensity 200 0.1
                _ -> pure ()
          where
            lulaby = track `elem` ["lulaby", "lulaoverlay", "Copy of andy 084"]
            nappe = track == "waldo A61"
            bass = track == "bass"
            hats = track == "hats"
            kick = track == "kick" && hasNote (== 36) xs
            snare = track == "snare" || (track == "kick" && hasNote (== 42) xs)

    let mkFrame' :: Frame -> MSec -> IO (Map Text Float)
        mkFrame' frame elapsed = do
            let evs = Midi.getFrameEvents (from frame) midiEvents

            when (frame == 0) do
                linMod zoom 42352 0.685
                -- expMod minDist 42352 3.83e-2
                linMod intensity 42352 5.438

            when (frame == 2540) do
                let rotLength = frame2time (4230 - 2540)
                linSpeed yawSpeed rotLength (-2.1 * pi)
                linSpeed pitchSpeed rotLength (1.7 * pi)

            when (frame == 5929) do
                linMod zoom (frame2time $ 12141 - 5929) 1.153

            when (frame == 8152) do
                let brkDuration = frame2time $ 8750 - 8152
                linMod zoom (brkDuration * 3) 1.27
                linMod yaw brkDuration 0
                linMod pitch brkDuration 0

            when (frame == 12671) do
                let fadeOut = 10_000
                linMod seedX fadeOut 2.173
                linMod seedY fadeOut 1.322
                linMod seedZ fadeOut 0.549
                linMod minDist fadeOut 0.09
                linMod pow fadeOut 16

            traverse_ (handleEvents frame) evs

            -- update modulation and read outvar value
            frameValues <- forM mods $ \modulation -> do
                v <- atomically $ readModulation elapsed modulation
                pure (modulation.name, v)

            let currentValues
                    | frame == 0 = Map.fromList $ startingValues <> frameValues
                    | otherwise = Map.fromList frameValues

            pure $ currentValues

        mkFrame :: Int -> IO (Map Text Float)
        mkFrame x = mkFrame' (from x) elapsed
          where
            elapsed
                | x == 0 = 0
                | otherwise = frameLength
    (frames :: Vector (Map Text Float)) <- generateM (from maxFrame) mkFrame
    let keyFrames = [0, 1410, 2541, 3670, 4230, 5929, 6494, 8152, 8750, 12141]
        sse = StaticScriptExport keyFrames (removeDuplicateValues frames)
    encodeFile (scriptFile <> ".gen") sse
    renameFile (scriptFile <> ".gen") scriptFile
  where
    -- sndFile = "data/SummerDawn.wav"
    midiFile = "data/SummerDawn.mid"
    scriptFile = "data/SummerDawn.json"

    startingVars :: [VariableDef]
    startingVars =
        fromJSON'
            [aesonQQ|[
      { "name": "zoom", "value": 3.2 },
      { "name": "pitch", "value": 0 },
      { "name": "yaw", "value": 0 },
      { "name": "pow", "value": 4.8 },
      { "name": "min-dist", "value": 5e-3 },
      { "name": "seed", "value": [ 0.28, 0.9, -0.461, 0 ] },
      { "name": "intensity", "value": 5.8 }
     ]|]
