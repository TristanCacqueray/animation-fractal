-- nixVulkanIntel cabal run -O2 animation-fractal -- --prog juicer  --media  ~/reaper/Documents/REAPER\ Media/opJuicer.mp3  --media  ~/reaper/Documents/REAPER\ Media/opJuicer-bass.mp3  --media  ~/reaper/Documents/REAPER\ Media/opJuicer-drums.mp3 --media  ~/reaper/Documents/REAPER\ Media/opJuicer-rev.mp3
module Demo.Truchet where

import AnimationFractal.Scene as AF
import Frelude

sceneInfo :: (Text, IO SceneInfo)
sceneInfo = AF.withFragPath "Truchet.glsl" $ AF.mkSceneInfo "truchet" fragCode [defSeq] vars
  where
    vars =
        [ AF.newFloatVar "drum" Nothing #var1
        , AF.newFloatVar "hats" Nothing #var2
        , AF.newFloatVar "bass" Nothing #var3
        , AF.newFloatVar "melo" Nothing #var4
        ]
    defSeq =
        fromJSON'
            [aesonQQ|
  {
    "inps": [
      { "config": { "gain": 1, "gate": 0, "modName": "drum" },
        "name": "opCBC-drum"
      },
      { "config": { "gain": 1, "gate": 0, "modName": "hats" },
        "name": "opCBC-hats"
      },
      { "config": { "gain": 1, "gate": 0, "modName": "bass" },
        "name": "opCBC-bass"
      },
      { "config": { "gain": 1, "gate": 0, "modName": "melo" },
        "name": "opCBC-melo"
      }
    ],
    "mods": [
      { "name": "drum", "params": [100], "prog": "incr", "targets": [
          { "gain": 1.0, "inverse": false, "name": "drum", "op": "ModAdd" }
        ]
      },
      { "name": "hats", "params": [104], "prog": "incr", "targets": [
          { "gain": 1.0, "inverse": false, "name": "hats", "op": "ModAdd" }
        ]
      },
      { "name": "bass", "params": [200], "prog": "incr", "targets": [
          { "gain": 1.0, "inverse": false, "name": "bass", "op": "ModAdd" }
        ]
      },
      { "name": "melo", "params": [200], "prog": "incr", "targets": [
          { "gain": 1.0, "inverse": false, "name": "melo", "op": "ModAdd" }
        ]
      }
    ],
    "name": "default",
    "next": null,
    "vars": [
      { "name": "drum", "value": 0 },
      { "name": "hats", "value": 0 },
      { "name": "bass", "value": 0 },
      { "name": "melo", "value": 0 }
    ]
  }
|]

fragCode :: Code
fragCode =
    fromString
        [glsl|
    #version 450

    ${set0binding1}

    #define drum scene.var1
    #define hats scene.var2
    #define bass scene.var3
    #define melo scene.var4

    layout (location = 0) in vec2 inUV;
    layout (location = 0) out vec4 oColor;
  |]
