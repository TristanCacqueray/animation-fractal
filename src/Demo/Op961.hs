-- | Run `ghcid --test Demo.Op961.main`
-- render with:
-- cabal run -O2 exe:animation-fractal -- --prog ufc --media ~/reaper/Documents/REAPER\ Media/op961.mp3 --script ./data/Op961.json --render data/Op961.mp4  --end 13102
-- ffmpeg -i ~/reaper/Documents/REAPER\ Media/op961.mp3 -i ./data/Op961.mp4 -c:a copy -c:v copy data/Op961-snd.mp4
module Demo.Op961 (main) where

import Control.Varying.Core qualified as Varying
import Control.Varying.Tween qualified as Varying
import Data.Map.Strict qualified as Map
import Data.Vector.Generic (generateM)
import Data.Vector.Storable qualified as SV
import Frelude

import Demo.UnderwaterFractalCreature qualified

import AnimationFractal.Modulation
import AnimationFractal.Scene
import AnimationFractal.Script
import AnimationFractal.Time
import AnimationFractal.Variable
import Engine.AudioDecoder qualified as AudioDecoder

easeOut :: MSec -> Float -> Float -> VaryingVar
easeOut duration current value = Varying.tweenStream tween value
  where
    tween = Varying.tween Varying.easeOutCubic current value duration

incrMod :: Modulation -> MSec -> Float -> IO ()
incrMod modulation duration value = atomically do
    current <- (.value) <$> readTVar modulation.hvar
    writeTVar modulation.source (easeOut duration current (current + value))

linMod :: Modulation -> MSec -> Float -> IO ()
linMod modulation duration value = atomically do
    current <- (.value) <$> readTVar modulation.hvar
    writeTVar modulation.source (linear duration current value)

main :: IO ()
main = do
    mainSamples <- AudioDecoder.analyzeFile "/home/fedora/reaper/Documents/REAPER Media/op961.mp3"
    meloSamples <- AudioDecoder.analyzeFile "/home/fedora/reaper/Documents/REAPER Media/op961-melo.mp3"
    drumSamples <- AudioDecoder.analyzeFile "/home/fedora/reaper/Documents/REAPER Media/op961-drums.mp3"
    bassSamples <- AudioDecoder.analyzeFile "/home/fedora/reaper/Documents/REAPER Media/op961-bass.mp3"
    let
        maxFrame = time2Frame (500) + Frame (unsafeFrom $ SV.length mainSamples)
    putStrLn $ "total frame: " <> show maxFrame

    scene <- snd Demo.UnderwaterFractalCreature.sceneInfo
    traverse_ (importVariable scene.vars) startingVars

    -- setup script modulation
    let outVars = sceneMods scene
        newSceneModulation outVar = do
            startingValue <- get outVar.current
            atomically do
                modulation <- newVarModulation outVar.name outVars
                writeTVar modulation.source (Varying.done startingValue)
                modifyTVar' modulation.hvar (pushHistoryVar startingValue)
                pure modulation

    mods <- traverse newSceneModulation $ sceneMods scene
    let getModulation name = fromMaybe (error $ "unknown var: " <> from name) $ lookupModulation name mods
        seedX = getModulation "Julia.x"
        seedY = getModulation "Julia.y"
        seedZ = getModulation "Julia.z"
        view_y = getModulation "view_y"
        zoom = getModulation "zoom"
        koff = getModulation "koff"
        amp = getModulation "Amplitude"
        yaw = getModulation "yaw"
        pitch = getModulation "pitch"

    -- setupScene
    startingValues <- forM (sceneMods scene) $ \outVar -> do
        value <- get outVar.current
        pure $ (outVar.name, value)

    let mkFrame' :: Frame -> MSec -> IO (Map Text Float)
        mkFrame' frame elapsed = do
            let melo = 10 * fromMaybe 0 (meloSamples SV.!? from frame)
            let drum = 10 * fromMaybe 0 (drumSamples SV.!? from frame)
            let bass = 10 * fromMaybe 0 (bassSamples SV.!? from frame)

            if
                | frame < 694 -> do
                    when (melo > 0.1) do
                        incrMod seedX 100 (-1 * melo / 8)
                        incrMod seedY 100 (-1 * melo / 5)
                | frame < 2776 -> do
                    when (frame == 694) do linMod zoom (2000_000 / 60) 3.2
                    when (melo > 0.1) do
                        -- incrMod seedY 100 (melo / 100)
                        incrMod amp 100 (melo / 18)
                    incrMod koff 200 (-1 * bass / 60)
                    when (drum > 1) do
                        incrMod seedX 300 (drum / 25.5)
                        incrMod seedZ 500 (drum / 35)
                | frame < 3470 -> do
                    let sceneLength = (3816 - 3470) * 1000 / 60
                    when (frame == 2776) do
                        linMod zoom sceneLength 8.5
                    -- linMod pitch sceneLength 1.38
                    incrMod koff 200 (melo / 18)
                    incrMod seedZ 50 (melo / -50)
                    incrMod amp 200 (bass / -400)
                | frame < 3816 -> do
                    incrMod koff 100 (bass / 20)
                    incrMod seedY 100 (bass / 10)
                --  pulseMod koff -6 (-1 * drum / 10)
                | frame < 4858 -> do
                    -- incrMod seedY 50 (drum / 30)
                    incrMod amp 200 (bass / -55)
                    incrMod koff 500 (bass / 5)
                    incrMod seedY 200 (drum / 12)
                | frame < 5552 -> do
                    let sceneLength = (5552 - 4858) * 1000 / 60
                    when (frame == 4858) do
                        linMod zoom sceneLength 5
                        linMod yaw sceneLength (-0.065)
                    incrMod koff 200 (bass / -20)
                    incrMod amp 50 (bass / 200)
                | frame < 6246 -> do
                    -- incrMod koff 100 (bass / 40)
                    incrMod amp 100 (melo / 10)
                    incrMod koff 200 (drum / 50)
                    incrMod seedZ 200 (melo / 120)
                | frame < 7460 -> do
                    -- incrMod koff 100 (bass / 40)
                    incrMod amp 100 (melo / 10)
                    incrMod koff 1000 (drum / -20)
                    incrMod seedY 100 (melo / -30)
                -- incrMod koff 100 (melo / 10)
                | frame < 8848 -> do
                    incrMod seedY 100 (drum / 200)
                    incrMod koff 100 (bass / 33)
                -- incrMod seedX 50 (melo / -10)
                | frame < 10930 -> do
                    when (frame == 8848) do
                        let cameraPitchLength = 10_000
                        -- linMod pitch cameraPitchLength 1.237
                        -- linMod view_y cameraPitchLength (-1.8)

                        linMod pitch cameraPitchLength 1.037
                        linMod view_y cameraPitchLength (-2.467)

                    incrMod amp 200 (drum / -100)
                    when (drum > 1) do
                        incrMod koff 2000 (drum / -100)
                    incrMod seedZ 200 (melo / -8)
                    incrMod seedX 100 (melo / -7)
                | frame < 11624 -> do
                    when (frame == 10930) do
                        let cameraPitchLength = 11_000
                        linMod zoom (cameraPitchLength * 3) 20
                        linMod pitch cameraPitchLength 1.5707964
                        linMod view_y cameraPitchLength 0

                    incrMod seedX 50 (melo / -8)
                    incrMod amp 100 (melo / 30)
                    incrMod seedY 100 (melo / 30)
                | otherwise -> do
                    let sceneLength = (13102 - 11624) * 1000 / 60
                    when (frame == 11624) do
                        -- linMod zoom sceneLength 5
                        linMod yaw sceneLength (pi / 2 - 0.065)
                    incrMod seedX 50 (melo / 8)
                    incrMod seedY 200 (melo / -8)
                    incrMod koff 200 (drum / 50)
                    incrMod amp 200 (drum / -22.5)

            -- update modulation and read outvar value
            frameValues <- forM mods $ \modulation -> do
                v <- atomically $ readModulation elapsed modulation
                pure (modulation.name, v)
            let currentValues
                    | frame == 0 = Map.fromList $ startingValues <> frameValues
                    | otherwise = Map.fromList frameValues

            pure $ currentValues

        mkFrame :: Int -> IO (Map Text Float)
        mkFrame x = mkFrame' (from x) elapsed
          where
            elapsed
                | x == 0 = 0
                | otherwise = frameLength
    (frames :: Vector (Map Text Float)) <- generateM (from maxFrame) mkFrame
    let keyFrames = [694, 2776, 3470, 3816, 4800, 4858, 5552, 7460, 8848, 10930, 11624, 11971]
        sse = StaticScriptExport keyFrames (removeDuplicateValues frames)
    encodeFile (scriptFile <> ".gen") sse
    renameFile (scriptFile <> ".gen") scriptFile
  where
    scriptFile = "data/Op961.json"

    startingVars :: [VariableDef]
    startingVars =
        fromJSON'
            [aesonQQ|[
      { "name": "zoom", "value": 5 },
      { "name": "swim_angle", "value": 0 },
      { "name": "pitch", "value": 1.5707964 },
      { "name": "yaw", "value": 1.506 },
      { "name": "clamp_fix", "value": 0.0 },
      { "name": "view_y", "value": 0 },
      { "name": "koff", "value": -6.143 },
      { "name": "Amplitude", "value": -12 },
      { "name": "Julia", "value": [ 0.152, 0, -0.639, 0 ] }
     ]|]
