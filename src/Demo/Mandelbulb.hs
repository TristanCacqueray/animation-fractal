module Demo.Mandelbulb where

import AnimationFractal.Scene
import Frelude
import GLSL (camera, palettes)

sceneInfo :: (Text, IO SceneInfo)
sceneInfo = mkSceneInfo "mandelbulb" fragCode clips vars
  where
    vars =
        [ newFloatVar "zoom" (Just (0.001, 20)) #zoom
        , newFloatVar "pitch" piRange #var1
        , newFloatVar "yaw" piRange #var2
        , newSciVar "pow" #var3
        , newSciVar "min-dist" #var5
        , newVec3Var "seed" Nothing #coord1
        , newFloatVar "intensity" Nothing #var4
        , newFloatVar "glow" Nothing #var6
        ]
    piRange = Just (-pi, pi)
    defVars =
        [ VariableDef "zoom" (toJSON @Float 0.9)
        , VariableDef "pow" (toJSON @Float 8.0)
        , VariableDef "min-dist" (toJSON @Float 0.01)
        , VariableDef "intensity" (toJSON @Float 1.9)
        ]
    clips =
        [ newClip "default" & (#vars .~ defVars)
        ]
            <> savedClips
    savedClips =
        fromJSON'
            [aesonQQ|
[
  {
    "inps": [],
    "mods": [],
    "name": "show",
    "vars": [
      { "name": "zoom", "value": 1.3806889 },
      { "name": "pitch", "value": 3.142 },
      { "name": "yaw", "value": 3.142 },
      { "name": "pow", "value": 5.31 },
      { "name": "seed", "value": [0.694, 0.806, 0.494, 0] },
      { "name": "intensity", "value": 3.438 }
    ]
  },
  {
    "inps": [],
    "mods": [],
    "name": "vase",
    "vars": [
      { "name": "zoom", "value": 0.7663119 },
      { "name": "pitch", "value": 0.237 },
      { "name": "yaw", "value": -0.76 },
      { "name": "pow", "value": 4.31 },
      { "name": "min-dist", "value": 1.0e-2 },
      { "name": "seed", "value": [-2.166, -2.222, 0.202, 0] },
      { "name": "intensity", "value": 4.057 }
    ]
  }
]
|]

fragCode :: Code
fragCode =
    fromString
        [glsl|
#version 450
${set0binding1}
${palettes}
${camera}

#define zoom scene.zoom
#define pitch scene.var1
#define yaw scene.var2
#define power scene.var3
#define C scene.coord1.xyz

#define intensity scene.var4

#define max_iter 8

#define max_march 150

#define minDist scene.var5

#define glow scene.var6

// adapted from https://www.shadertoy.com/view/XsXXWS:
// Mandelbulb Explained
// open source under the http://opensource.org/licenses/BSD-2-Clause license
// by Morgan McGuire, http://graphics-codex.com

mat3 rotation;

float map(vec3 P, out vec4 trap) {
    vec3 z = rotation * P;
    vec3 c = C;

    // Put the whole shape in a bounding sphere to
    // speed up distant ray marching. This is necessary
    // to ensure that we don't expend all ray march iterations
    // before even approaching the surface
    {
        float r = length(z) - 3;
        if (r > 1) { return r; }
    }

    // Used to smooth discrete iterations into continuous distance field
    // (similar to the trick used for coloring the Mandelbrot set)
    float derivative = 1.1;

    trap = vec4(abs(z), 42.0);

    for (int i = 0; i < max_iter; ++i) {
        float r = length(z);

        if (r > 2.0) {
            return 0.25 * log(r) * r / derivative;
        } else {
            // Convert to polar coordinates and then rotate by the power
            float theta = acos(z.z / r) * power;
            float phi   = atan(z.y, z.x) * power;

            // Update the derivative
            derivative = pow(r, power - 1.0) * power * derivative + 2.0;

            // Convert back to Cartesian coordinates and
            // offset by the original point (which we're orbiting)
            float sinTheta = sin(theta);

            z = vec3(sinTheta * cos(phi),
                     sinTheta * sin(phi),
                     cos(theta));
            z *= (pow(r, power) + c);

        }

        // orbit trapping
        trap = min( trap, vec4(abs(z),r) );
    }

    return minDist;
}

// https://iquilezles.org/articles/normalsSDF
vec3 calcNormal( in vec3 pos, in float t, in float px )
{
    vec2 e = vec2(1.0,-1.0)*0.5773*0.25*px;
    vec4 ignore;
    return normalize( e.xyy*map( pos + e.xyy, ignore) +
                      e.yyx*map( pos + e.yyx, ignore) +
                      e.yxy*map( pos + e.yxy, ignore) +
                      e.xxx*map( pos + e.xxx, ignore) );
}

vec4 render(vec2 coord) {
    vec3 rayOrigin = vec3(vec2(1 / zoom) * (2.0 * coord - 1.0), -5.0);
    // rayOrigin.y = rayOrigin.y * scene.screenRatio;
    vec3 rayDirection = vec3(0, 0, 1.0);

    // Distance from ray origin to hit point
    float t = 0.0;

    // Point on (technically, near) the surface of the Mandelbulb
    vec3 X;

    bool hit = false;
    float d;
    float closest = 120;
    int i;

    vec4 trap;

    // March along the ray, detecting when we are very close to the surface
    for (i = 0; i < max_march; ++i) {
        X = rayOrigin + rayDirection * t;

        d = map(X, trap);
        hit = (d < minDist);
        closest = min(closest, d);
        if (hit) { break; }
        if (d > 3) { break ; }
        t += d;
    }

    vec3 color = vec3(0);
    if (hit) {
        color = getColor(1.0 + 0.01 * abs(intensity) * length(trap.xyw));
        // color += 0.3 * trap.w * getColor(intensity * float(i) / max_march);

        // color += 0.4 * getColor(trap.w);

#if 1
        vec3 n = calcNormal(X, d, scene.screenRatio);
        vec3 light1 = vec3(-0.6, -0.5, -2);

        vec3 eps = vec3(minDist * 5.0, 0.0, 0.0);
        X -= rayDirection * eps.x;
        n = normalize(n + normalize(X) * 16);

        float dif1 = clamp( dot( light1, n ), 0.2, 1.0 );
        vec3 lin = 2*vec3(0.50,0.55,0.53)*dif1*(1+glow);
        color *= lin;
#endif
    } else {
        // sky
        vec3 sky = vec3(0.1, 0.1 + 0.2 * (1 - coord.x), 0.5);
        float glow_mix = smoothstep((1+glow/3)*2 * float(i) / max_march, 0, 0.5);
        vec3 glow_color = getColor(0.01) * 2;
        color = mix(sky, glow_color, glow_mix);
        // color = sky;
    }
    return vec4(color, 1.0);
}


    layout (location = 0) in vec2 inUV;
    layout (location = 0) out vec4 oColor;


#define PI 3.1415926538
void main(void) {
    vec2 fragCoord = inUV;
    rotation = rotateX(pitch) * rotateY(yaw) * rotateZ( - PI / 2);

    oColor = render(fragCoord.xy);
}
  |]
