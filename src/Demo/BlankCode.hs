-- | This module bakes the fragment into the executable.
--  Because of a template-haskell restriction, this need to be defined in a dedicated module,
--  otherwise fragSpirv should be part of the fragCode module.
module Demo.BlankCode (fragSpirv) where

import Demo.Blank (fragCode)
import Frelude

fragSpirv :: ByteString
fragSpirv = $(compileFrag fragCode)
