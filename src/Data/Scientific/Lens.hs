module Data.Scientific.Lens (exp10L, coefL) where

import Frelude

exp10L :: Lens' Scientific Int
exp10L = lens getv setv
  where
    getv :: Scientific -> Int
    getv = base10Exponent
    setv :: Scientific -> Int -> Scientific
    setv s c = scientific (coefficient s) c

coefL :: Lens' Scientific Int
coefL = lens getv setv
  where
    -- This should be Integer, but dear-imgui works with Int.
    getv :: Scientific -> Int
    getv = fromInteger . coefficient
    setv :: Scientific -> Int -> Scientific
    setv s c = scientific (toInteger c) (base10Exponent s)
