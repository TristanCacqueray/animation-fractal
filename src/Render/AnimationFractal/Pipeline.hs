module Render.AnimationFractal.Pipeline (
    Pipeline,
    allocate,
    Config,
    config,
) where

import Data.Tagged (Tagged (..))
import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (DsLayoutBindings, HasRenderPass (..), HasVulkan)

import Render.AnimationFractal.Model (Scene)

-- The mandelbrot pipeline bindings are tagged with the scene type here
type Pipeline = Graphics.Pipeline '[Scene] () ()
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate ::
    (HasVulkan env, HasRenderPass renderpass) =>
    Vk.SampleCountFlagBits ->
    Tagged Scene DsLayoutBindings ->
    renderpass ->
    ResourceT (RIO env) Pipeline
allocate multisample ds rp =
    snd <$> Graphics.allocate Nothing multisample (config ds) rp

config :: Tagged Scene DsLayoutBindings -> Config
config (Tagged ds) =
    Graphics.baseConfig
        { Graphics.cDescLayouts = Tagged @'[Scene] [ds]
        }
