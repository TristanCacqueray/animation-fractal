module Render.AnimationFractal.Model (
    Scene (..),
    newScene,
    observe,
    mkBindings,
    FrameResource (..),
    set0binding1,
    withBoundSet0,
    allocateFrameResource,
    -- debug
    encodeScene,
) where

import Frelude

import Data.Kind (Type)
import Data.Vector qualified as Vector
import Data.Vector.Storable qualified as VectorS
import Foreign.Storable.Generic (GStorable)
import Vulkan.CStruct.Extends (SomeStruct (..))
import Vulkan.Core10 qualified as Vk

import Resource.Region qualified as Region
import UnliftIO.Resource (MonadResource)

import Engine.Vulkan.DescSets (Bound, withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline (Pipeline)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Types (DsLayoutBindings, HasVulkan (..), MonadVulkan)
import Engine.Worker qualified as Worker
import Resource.Buffer qualified as Buffer
import Resource.Vulkan.DescriptorPool qualified as DescriptorPool

import Render.Code (trimming)

-- for debug

import Data.ByteString.Unsafe
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Foreign.Storable

-- | Members must be aligned to 8 bytes, or by group of 2 float.
--  Check packing with
--    putStrLn =<< Hexdump.prettyHex <$> encodeScene newScene
--
--  Reference: https://registry.khronos.org/OpenGL/specs/gl/glspec45.core.pdf
--  page 138: 7.6.2.2 Standard Uniform Block Layout
data Scene = Scene
    { screenRatio :: Float
    , extraVar1 :: Float
    , screenResolution :: UVec2
    , -- 1
      origin :: Vec2
    , zoom :: Float
    , extraVar2 :: Float
    , -- 2
      varBool1 :: Bool
    , varBool2 :: Bool
    , varBool3 :: Bool
    , varBool4 :: Bool
    , -- 3
      varInt1 :: Int32
    , varInt2 :: Int32
    , var1 :: Float
    , var2 :: Float
    , -- 4
      var3 :: Float
    , var4 :: Float
    , var5 :: Float
    , var6 :: Float
    , -- 5
      pos1v2 :: Vec2
    , pos2v2 :: Vec2
    , pos3v2 :: Vec2
    , pos4v2 :: Vec2
    , -- 7
      coord1 :: Vec4
    , -- 8
      n1_1 :: Float
    , n1_2 :: Float
    , n1_3 :: Float
    , n1_4 :: Float
    , n1_5 :: Float
    , n1_6 :: Float
    , n1_7 :: Float
    , n1_8 :: Float
    , n1_9 :: Float
    , n1_10 :: Float
    , n1_11 :: Float
    , n1_12 :: Float
    , n1_13 :: Float
    , n1_14 :: Float
    , n1_15 :: Float
    , n1_16 :: Float
    , n2_1 :: Float
    , n2_2 :: Float
    , n2_3 :: Float
    , n2_4 :: Float
    , n2_5 :: Float
    , n2_6 :: Float
    , n2_7 :: Float
    , n2_8 :: Float
    , n2_9 :: Float
    , n2_10 :: Float
    , n2_11 :: Float
    , n2_12 :: Float
    , n2_13 :: Float
    , n2_14 :: Float
    , n2_15 :: Float
    , n2_16 :: Float
    , n3_1 :: Float
    , n3_2 :: Float
    , n3_3 :: Float
    , n3_4 :: Float
    , n3_5 :: Float
    , n3_6 :: Float
    , n3_7 :: Float
    , n3_8 :: Float
    , n3_9 :: Float
    , n3_10 :: Float
    , n3_11 :: Float
    , n3_12 :: Float
    , n3_13 :: Float
    , n3_14 :: Float
    , n3_15 :: Float
    , n3_16 :: Float
    , n4_1 :: Float
    , n4_2 :: Float
    , n4_3 :: Float
    , n4_4 :: Float
    , n4_5 :: Float
    , n4_6 :: Float
    , n4_7 :: Float
    , n4_8 :: Float
    , n4_9 :: Float
    , n4_10 :: Float
    , n4_11 :: Float
    , n4_12 :: Float
    , n4_13 :: Float
    , n4_14 :: Float
    , n4_15 :: Float
    , n4_16 :: Float
    }
    deriving (Show, Generic, FromJSON, ToJSON)

newScene :: Scene
newScene = Scene 0 0 0 0 0 0 False False False False 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

encodeScene :: Scene -> IO ByteString
encodeScene s = do
    ptr <- malloc :: IO (Ptr Scene)
    poke ptr s
    unsafePackMallocCStringLen (castPtr ptr, sizeOf s)

set0binding1 :: Code
set0binding1 =
    Code
        [trimming|
    #extension GL_EXT_scalar_block_layout : require
    layout(set=0, binding=1, std430) uniform Globals {
      float screenRatio;
      float evar1;
      ivec2  screenResolution;  // 1
      vec2  origin;
      float zoom;
      float evar2;              // 2
      bool  varBool1;
      bool  varBool2;
      bool  varBool3;
      bool  varBool4;           // 3
      int   varInt1;
      int   varInt2;
      float var1;
      float var2;               // 4
      float var3;
      float var4;
      float var5;
      float var6;               // 5
      vec2  pos1v2;
      vec2  pos2v2;
      vec2  pos3v2;
      vec2  pos4v2;             // 7
      vec4  coord1;             // 8
      vec4 pitches1[4];
      vec4 pitches2[4];
      vec4 pitches3[4];
      vec4 pitches4[4];
    } scene;
  |]

instance GStorable Scene

-- * Common descriptor set

mkBindings :: Tagged Scene DsLayoutBindings
mkBindings =
    Tagged
        [ (set0bind1, zero)
        ]

set0bind1 :: Vk.DescriptorSetLayoutBinding
set0bind1 =
    Vk.DescriptorSetLayoutBinding
        { binding = 1
        , descriptorType = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
        , descriptorCount = 1
        , stageFlags = Vk.SHADER_STAGE_ALL
        , immutableSamplers = mempty
        }

-- * Setup

allocateFrameResource ::
    (MonadVulkan env m, MonadResource m) =>
    Tagged '[Scene] Vk.DescriptorSetLayout ->
    ResourceT m (FrameResource '[Scene])
allocateFrameResource (Tagged set0layout) = do
    descPool <- Region.local $ DescriptorPool.allocate (Just "AF") 1 dpSizes

    let set0dsCI =
            zero
                { Vk.descriptorPool = descPool
                , Vk.setLayouts = Vector.singleton set0layout
                }
    context <- asks id
    descSets <-
        (Tagged @'[Scene])
            <$> Vk.allocateDescriptorSets (getDevice context) set0dsCI

    (_, sceneData) <-
        Buffer.allocateCoherent
            (Just "sceneData")
            Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
            1
            (VectorS.singleton newScene)

    updateSet0Ds context descSets sceneData

    scene <- Worker.newObserverIO newScene

    pure $ FrameResource descSets sceneData scene
  where
    dpSizes = [(Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1)]
    updateSet0Ds ::
        ( HasVulkan context
        , MonadIO m
        ) =>
        context ->
        Tagged '[Scene] (Vector Vk.DescriptorSet) ->
        Buffer.Allocated 'Buffer.Coherent Scene ->
        m ()
    updateSet0Ds context (Tagged ds) sceneData =
        Vk.updateDescriptorSets (getDevice context) writeSets mempty
      where
        destSet0 = case Vector.headM ds of
            Nothing -> error "assert: descriptor sets promised to contain [Scene]"
            Just one -> one

        writeSet0b0 =
            SomeStruct
                zero
                    { Vk.dstSet = destSet0
                    , Vk.dstBinding = 1
                    , Vk.dstArrayElement = 0
                    , Vk.descriptorCount = 1
                    , Vk.descriptorType = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
                    , Vk.bufferInfo = Vector.singleton set0bind0I
                    }
          where
            set0bind0I =
                Vk.DescriptorBufferInfo
                    { Vk.buffer = Buffer.aBuffer sceneData
                    , Vk.offset = 0
                    , Vk.range = Vk.WHOLE_SIZE
                    }

        writeSets =
            Vector.fromList $ pure writeSet0b0

-- * Frame data

data FrameResource (ds :: [Type]) = FrameResource
    { frDescSets :: Tagged ds (Vector Vk.DescriptorSet)
    , frBuffer :: Buffer.Allocated 'Buffer.Coherent Scene
    , frObserver :: Worker.ObserverIO Scene
    }

observe :: (HasLogFunc env, MonadReader env m, MonadUnliftIO m) => Worker.Merge Scene -> FrameResource ds -> m ()
observe process FrameResource{frBuffer, frObserver} =
    Worker.observeIO_ process frObserver \_old new -> do
        when False do
            -- todo: minify the output
            logInfo $ "Updating fractal scene buffer with " <> displayShow new
        _same <- Buffer.updateCoherent (VectorS.singleton new) frBuffer
        pure new

-- * Rendering

withBoundSet0 :: (MonadIO m) => FrameResource ds -> Pipeline ds vertices instances -> Vk.CommandBuffer -> Bound ds Void Void m b -> m b
withBoundSet0 FrameResource{frDescSets} refPipeline cb =
    withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS refPipeline.pLayout frDescSets
