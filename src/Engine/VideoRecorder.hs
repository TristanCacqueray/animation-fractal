module Engine.VideoRecorder where

import Data.Vector.Storable qualified as V
import Frelude
import UnliftIO.Concurrent

import Codec.FFmpeg qualified as FFmpeg
import Codec.FFmpeg.Encode qualified as FFmpeg
import Codec.FFmpeg.Internal.Linear (V2 (..))

import Data.Massiv.Array.Manifest qualified as Massiv (toStorableVector)

import Engine.AudioDecoder (combineAudio)
import Engine.Screenshot qualified as Screenshot

data Pipe a = Pipe
    { chan :: TChan a
    , size :: TVar Natural
    }

newPipe :: STM (Pipe a)
newPipe = Pipe <$> newTChan <*> newTVar 0

writePipe :: Pipe a -> a -> STM ()
writePipe p v = do
    writeTChan p.chan v
    modifyTVar' p.size (+ 1)

readPipe :: Pipe a -> STM a
readPipe p = do
    v <- readTChan p.chan
    modifyTVar' p.size (\s -> s - 1)
    pure v

data VideoRecorderEvent
    = RecordFrame Screenshot.MassivImage
    | Stop

data VideoRecorderStatus
    = Running
    | Stopping
    | Done
    deriving (Show, Eq)

data VideoRecorder = VideoRecorder
    { pipe :: Pipe VideoRecorderEvent
    , status :: TVar VideoRecorderStatus
    , thread :: ThreadId
    }

recorderStatus :: (MonadIO m) => VideoRecorder -> m (VideoRecorderStatus, Natural)
recorderStatus vr = atomically do
    (,) <$> readTVar vr.status <*> readTVar vr.pipe.size

stopRecorder :: (MonadIO m) => VideoRecorder -> m ()
stopRecorder vr = atomically do
    writePipe vr.pipe Stop
    writeTVar vr.status Stopping

recordFrame :: (MonadIO m) => VideoRecorder -> Screenshot.MassivImage -> m ()
recordFrame vr img = atomically $ writePipe vr.pipe (RecordFrame img)

createRecorder :: (HasLogFunc env) => Maybe FilePath -> FilePath -> RIO env VideoRecorder
createRecorder mAudioFP fp = do
    logInfo "Starting ffmpeg"
    liftIO do
        FFmpeg.initFFmpeg
        FFmpeg.setLogLevel (FFmpeg.LogLevel 5)
    writer <- liftIO (FFmpeg.videoWriter encParams fp)
    pipe <- atomically newPipe
    status <- atomically $ newTVar Running
    thread <- forkIO $ fix \loop -> do
        logInfo "Waiting for frame"
        atomically (readPipe pipe) >>= \case
            RecordFrame frame -> do
                -- logInfo "Writing frame"
                let img = (fmt, V2 w h, V.unsafeCast $ Massiv.toStorableVector frame)
                liftIO $ writer $ Just img
                -- logInfo "Done writing"
                loop
            Stop -> do
                liftIO $ writer Nothing
                forM_ mAudioFP (combineAudio fp)
                atomically $ writeTVar status Done
                logInfo "Recorder done"

    pure $ VideoRecorder{..}
  where
    (w, h) = (fromIntegral oWidth, fromIntegral oHeight)
    fmt = FFmpeg.avPixFmtRgba
    encParams =
        (FFmpeg.defaultParams w h)
            { FFmpeg.epPreset = "fast"
            , FFmpeg.epFps = 60
            }
