module Engine.AudioDecoder (
    Samples,
    decodeFile,
    combineAudio,
    sampleFrameCount,
    soundRMS,
    analyzeFile,
) where

import Frelude hiding (Array)

import System.Process.Typed

import Codec.Serialise (readFileDeserialise, writeFileSerialise)
import Data.Vector.Storable qualified as SV

import Data.ByteString.Internal (toForeignPtr0)
import Foreign (Storable (sizeOf))
import Foreign.ForeignPtr (ForeignPtr, castForeignPtr)

type Samples = SV.Vector Float

type SamplesRMS = SV.Vector Float

-- Increase this number when change the algorithm
analyzeVersion :: Int
analyzeVersion = 0

analyzeFile :: (MonadIO m) => FilePath -> m SamplesRMS
analyzeFile fileName = liftIO do
    current <- getModificationTime fileName
    try @IO @SomeException (getModificationTime cacheName) >>= \case
        Right lastUpdated | lastUpdated > current -> readFileDeserialise cacheName
        _ -> cacheAnalyze
  where
    cacheAnalyze = do
        res <- doAnalyzeFile
        writeFileSerialise cacheName res
        putStrLn $ "Wrote: " <> cacheName
        pure res
    cacheName = "data" </> takeBaseName fileName <> ".gen" <> show analyzeVersion
    doAnalyzeFile = do
        samples <- decodeFile fileName
        putStrLn $ "Processing: " <> show (sampleFrameCount samples)
        pure $ SV.generate (unsafeFrom $ sampleFrameCount samples - 1) \position -> do
            let frameSize = 44100 `div` 30
            let frameSamples = SV.slice ((position `div` 2) * frameSize) (frameSize) samples
            soundRMS frameSamples

soundRMS :: Samples -> Float
soundRMS arr = sqrt (tot / fromIntegral (SV.length arr))
  where
    tot = SV.foldr (\v acc -> acc + v * v) 0 arr

sampleFrameCount :: Samples -> Word
sampleFrameCount arr = ceiling $ frameCount
  where
    frameCount = timeLength * fps
    timeLength :: Double
    timeLength = fromIntegral (SV.length arr) / 44100
    fps = 60

-- | Normalize input file into a 44100 mono float array.
decodeFile :: (MonadIO m) => FilePath -> m Samples
decodeFile fname = liftIO do
    let args =
            ["-hide_banner", "-loglevel", "info"] -- quiet
                <> ["-i", fname] -- input
                -- <> ["-to", "30"] -- limit to 30sec
                <> ["-ac", "1"] -- convert to mono
                <> ["-ar", "44100"] -- sample at 44100 (fit for 60 fps)
                <> ["-f", "f32le"] -- use float
                <> ["-"] -- output
    putStrLn $ "Running: ffmpeg " <> unwords args
    (ExitSuccess, pcmBuf) <- readProcessStdout $ proc "ffmpeg" args
    let (wordPtr, wordSZ) = toForeignPtr0 (from pcmBuf)
        pcmPtr = castForeignPtr wordPtr :: ForeignPtr Float
        pcmSZ = wordSZ `div` sizeOf (0 :: Float)
        arr = SV.unsafeFromForeignPtr0 pcmPtr pcmSZ
    -- print (Data.Massiv.Array.size arr)
    pure arr

combineAudio :: (MonadIO m) => FilePath -> FilePath -> m ()
combineAudio videoPath audioPath = do
    runProcess_ $ proc "ffmpeg" args
    renameFile oPath videoPath
  where
    oPath = takeBaseName videoPath <> "-final.mp4"
    args =
        ["-hide_banner", "-loglevel", "info"] -- quiet
            <> ["-i", videoPath, "-i", audioPath] -- input
            <> ["-c:a", "copy", "-c:v", "copy"] -- codecs
            <> [oPath]
