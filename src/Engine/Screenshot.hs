{-# LANGUAGE OverloadedLists #-}

-- | This module should probably part of keid-core or a new keid-screenshot
module Engine.Screenshot (
    -- * CPU image
    ScreenshotImage,
    createScreenshotImage,
    recordScreenshotCommand,

    -- * Massiv Array
    MassivImage,
    readImageMassiv,
    encodeImageMassiv,

    -- * Benchmark
    makeScreenshot,
    writeImageMassiv,
    writeImageJuicy,
) where

import RIO
import RIO.ByteString.Lazy qualified as BSL
import Vulkan.Zero (zero)

import Codec.Picture qualified as JP
import Codec.Picture.Types qualified as JP
import Control.Monad.Trans.Resource qualified as Resource
import Foreign (Storable (peek, sizeOf), plusPtr)
import Vulkan.CStruct.Extends (SomeStruct (..))
import Vulkan.Core10 qualified as Vk
import VulkanMemoryAllocator qualified as VMA

import Engine.Vulkan.Types qualified as Vulkan
import Resource.CommandBuffer qualified as CommandBuffer
import Resource.Image qualified as Image

import Data.Massiv.Array
import Data.Massiv.Array.IO qualified as MIO
import Foreign.Ptr (Ptr)
import Graphics.Pixel (RGB)
import Graphics.Pixel.ColorSpace hiding (zero)

import Data.Text (pack)
import Text.Printf (printf)

outImageCI :: Vk.Format -> (Word32, Word32) -> Vk.ImageCreateInfo '[]
outImageCI fmt (width, height) =
    zero
        { Vk.imageType = Vk.IMAGE_TYPE_2D
        , Vk.flags = zero
        , Vk.format = fmt
        , Vk.extent = Vk.Extent3D width height 1
        , Vk.mipLevels = 1
        , Vk.arrayLayers = 1
        , Vk.tiling = Vk.IMAGE_TILING_LINEAR
        , Vk.initialLayout = Vk.IMAGE_LAYOUT_UNDEFINED
        , Vk.usage = Vk.IMAGE_USAGE_TRANSFER_DST_BIT
        , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
        , Vk.samples = Vk.SAMPLE_COUNT_1_BIT
        }

outImageAllocationCI :: VMA.AllocationCreateInfo
outImageAllocationCI =
    zero
        { VMA.flags = VMA.ALLOCATION_CREATE_MAPPED_BIT
        , VMA.usage = VMA.MEMORY_USAGE_GPU_TO_CPU
        }

data ScreenshotImage = ScreenshotImage
    { info :: VMA.AllocationInfo
    , image :: Vk.Image
    , layout :: Vk.SubresourceLayout
    , dim :: (Word32, Word32)
    }

createScreenshotImage ::
    (HasLogFunc a, MonadReader a m, Vulkan.HasVulkan a, Resource.MonadResource m) =>
    Maybe Vk.Format ->
    (Word32, Word32) ->
    m (Resource.ReleaseKey, ScreenshotImage)
createScreenshotImage fmtM dim = do
    let fmt = fromMaybe Vk.FORMAT_R8G8B8A8_UNORM fmtM
    context <- ask
    (outImage, outAllocation, outInfo) <-
        VMA.createImage
            (Vulkan.getAllocator context)
            (outImageCI fmt dim)
            outImageAllocationCI
    outKey <-
        Resource.register
            $ VMA.destroyImage
                (Vulkan.getAllocator context)
                outImage
                outAllocation
    outLayout <-
        Vk.getImageSubresourceLayout
            (Vulkan.getDevice context)
            outImage
            ( Vk.ImageSubresource
                { aspectMask = Vk.IMAGE_ASPECT_COLOR_BIT
                , mipLevel = 0
                , arrayLayer = 0
                }
            )

    logInfo $ "Output layout: " <> displayShow outLayout
    pure (outKey, ScreenshotImage outInfo outImage outLayout dim)

takeScreenshot :: (MonadIO m) => Vk.CommandBuffer -> ScreenshotImage -> Vk.Image -> m ()
takeScreenshot cb si srcImage = do
    -- see: https://github.com/SaschaWillems/Vulkan/blob/ed406e61a69a9fb5616e087c99291eb27ba2b9a9/examples/screenshot/screenshot.cpp#L239
    -- Transition dst image to transfer destination layout
    Vk.cmdPipelineBarrier
        cb
        Vk.PIPELINE_STAGE_TRANSFER_BIT
        Vk.PIPELINE_STAGE_TRANSFER_BIT
        zero
        mempty
        mempty
        [ SomeStruct
            zero
                { Vk.srcAccessMask = zero
                , Vk.dstAccessMask = Vk.ACCESS_TRANSFER_WRITE_BIT
                , Vk.oldLayout = Vk.IMAGE_LAYOUT_UNDEFINED
                , Vk.newLayout = Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
                , Vk.image = si.image
                , Vk.subresourceRange = Image.subresource Vk.IMAGE_ASPECT_COLOR_BIT 1 1
                }
        ]

    -- Transition source image to transfer source layout
    Vk.cmdPipelineBarrier
        cb
        Vk.PIPELINE_STAGE_TRANSFER_BIT
        Vk.PIPELINE_STAGE_TRANSFER_BIT
        zero
        mempty
        mempty
        [ SomeStruct
            zero
                { Vk.srcAccessMask = Vk.ACCESS_MEMORY_READ_BIT
                , Vk.dstAccessMask = Vk.ACCESS_TRANSFER_READ_BIT
                , Vk.oldLayout = Vk.IMAGE_LAYOUT_UNDEFINED
                , Vk.newLayout = Vk.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
                , Vk.image = srcImage
                , Vk.subresourceRange = Image.subresource Vk.IMAGE_ASPECT_COLOR_BIT 1 1
                }
        ]

    let imageSubr =
            Vk.ImageSubresourceLayers
                { aspectMask = Vk.IMAGE_ASPECT_COLOR_BIT
                , mipLevel = 0
                , baseArrayLayer = 0
                , layerCount = 1
                }

    Vk.cmdCopyImage
        cb
        srcImage
        Vk.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        si.image
        Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        [ Vk.ImageCopy
            { srcSubresource = imageSubr
            , srcOffset = Vk.Offset3D 0 0 0
            , dstSubresource = imageSubr
            , dstOffset = Vk.Offset3D 0 0 0
            , extent = uncurry Vk.Extent3D si.dim 1
            }
        ]

    -- Transition destination image to general layout
    Vk.cmdPipelineBarrier
        cb
        Vk.PIPELINE_STAGE_TRANSFER_BIT
        Vk.PIPELINE_STAGE_TRANSFER_BIT
        zero
        mempty
        mempty
        [ SomeStruct
            zero
                { Vk.srcAccessMask = Vk.ACCESS_TRANSFER_WRITE_BIT
                , Vk.dstAccessMask = Vk.ACCESS_MEMORY_READ_BIT
                , Vk.oldLayout = Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
                , Vk.newLayout = Vk.IMAGE_LAYOUT_GENERAL
                , Vk.image = si.image
                , Vk.subresourceRange = Image.subresource Vk.IMAGE_ASPECT_COLOR_BIT 1 1
                }
        ]

    -- TODO: transition the src image back
    pure ()

recordScreenshotCommand ::
    (MonadReader env m, MonadUnliftIO m, Vulkan.HasVulkan env, Resource.MonadResource m) =>
    ScreenshotImage ->
    Vk.Image ->
    m ()
recordScreenshotCommand si src = do
    context <- ask
    CommandBuffer.withPools $ \pools ->
        CommandBuffer.oneshot_ context pools Vulkan.qTransfer \cb -> do
            takeScreenshot cb si src

makeScreenshot ::
    (MonadReader env m, Resource.MonadResource m, MonadUnliftIO m, HasLogFunc env, Vulkan.HasVulkan env) =>
    FilePath ->
    (Word32, Word32) ->
    Vk.Image ->
    m ()
makeScreenshot fp dim src = do
    (siKey, si) <- measure "create screenshot image" do
        createScreenshotImage Nothing dim

    measure "record commands" do
        recordScreenshotCommand si src

    imgJuicy <- measure "read juicy" do
        readImageJuicy si

    imgMassiv <- measure "read massiv" do
        liftIO $ readImageMassiv si

    bsJuicy <- measure "encode juicy" do
        pure $! encodeImageJuicy imgJuicy
    logInfo $ "Juicy length: " <> displayShow (BSL.length bsJuicy)

    bsMassiv <- measure "encode massiv" do
        liftIO $ encodeImageMassiv imgMassiv
    logInfo $ "Massiv length: " <> displayShow (BSL.length bsMassiv)

    measure "write juicy" do
        BSL.writeFile (fp <> "-juicy.bmp") bsJuicy

    measure "write massiv" do
        BSL.writeFile (fp <> "-massiv.bmp") bsMassiv

    Resource.release siKey
  where
    measure (name :: Text) action = do
        begin <- getMonotonicTime
        res <- action
        end <- getMonotonicTime
        logInfo $ display $ pack (printf "%8.3f" (1000 * (end - begin))) <> " ms: " <> name
        pure res

type MassivPixel = Pixel (Alpha RGB) Word8
type MassivImage = Array S Ix2 MassivPixel

readImageMassiv :: ScreenshotImage -> IO MassivImage
readImageMassiv si =
    generateArrayS
        (Sz $ fromIntegral (snd si.dim) :. fromIntegral (fst si.dim))
        getPixel
  where
    getPixel :: Ix2 -> IO MassivPixel
    getPixel (y :. x) = liftIO $! peek (pixelAddr x y)

    pixelAddr :: Int -> Int -> Ptr MassivPixel
    pixelAddr x y =
        plusPtr
            (VMA.mappedData si.info)
            ( fromIntegral si.layout.offset
                + (y * fromIntegral si.layout.rowPitch)
                + (x * sizeOf (0 :: Word32))
            )

encodeImageMassiv :: MassivImage -> IO LByteString
encodeImageMassiv = MIO.encodeM MIO.BMP MIO.def

writeImageMassiv :: (MonadIO m) => FilePath -> MassivImage -> m ()
writeImageMassiv = MIO.writeArray MIO.BMP MIO.def

readImageJuicy :: (MonadIO m) => ScreenshotImage -> m (JP.Image JP.PixelRGBA8)
readImageJuicy si = do
    let
        -- pixelAddr :: Int -> Int -> Ptr Word32
        pixelAddr x y =
            plusPtr
                (VMA.mappedData si.info)
                ( fromIntegral si.layout.offset
                    + (y * fromIntegral si.layout.rowPitch)
                    + (x * sizeOf (0 :: Word32))
                )

    liftIO do
        JP.withImage
            (fromIntegral $ fst si.dim)
            (fromIntegral $ snd si.dim)
            ( \x y ->
                fmap
                    (JP.unpackPixel @JP.PixelRGBA8)
                    (peek $ pixelAddr x y)
            )

encodeImageJuicy :: JP.Image JP.PixelRGBA8 -> LByteString
encodeImageJuicy = JP.encodeBitmap

writeImageJuicy :: (MonadIO m) => FilePath -> JP.Image JP.PixelRGBA8 -> m ()
writeImageJuicy fp = BSL.writeFile fp . JP.encodePng
