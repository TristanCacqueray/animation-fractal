module Engine.MidiDecoder where

import Frelude hiding (Array, Vector)
import RIO.List.Partial (maximum)

import Data.IntMap.Strict qualified as IM
import Data.Map.Strict qualified as Map

import Data.EventList.Relative.TimeBody qualified as TB
import Numeric.NonNegative.Wrapper qualified as NN
import Sound.MIDI.File qualified
import Sound.MIDI.File.Event qualified as EV
import Sound.MIDI.File.Event.Meta qualified as Meta
import Sound.MIDI.File.Load (fromFile)
import Sound.MIDI.Message.Channel qualified as Chan
import Sound.MIDI.Message.Channel.Voice qualified as Voice

data MidiEvent = MidiNoteOn Int Float | MidiNoteOff Int
    deriving (Show)

data MidiAccum = MidiAccum
    { -- The absolute time elapsed in the track
      elapsed :: Double
    , -- The current frame position
      frame :: Int
    , name :: TrackName
    , -- The accumulated events of the frame
      current :: [MidiEvent]
    , -- The final list of events
      events :: IM.IntMap [MidiEvent]
    }
    deriving (Generic, Show)

newtype TrackName = TrackName Text
    deriving newtype (Semigroup, Monoid, Ord, Eq, Show, IsString)

frameTime :: Double
frameTime = 1 / 60

data MidiEvents = MidiEvents
    { tracks :: Map TrackName (IM.IntMap [MidiEvent])
    , endFrame :: Word
    }

emptyMidiEvents :: MidiEvents
emptyMidiEvents = MidiEvents mempty 65535

getFrameEvents :: Int -> MidiEvents -> [(TrackName, [MidiEvent])]
getFrameEvents pos midiEvents = mapMaybe getEvents (Map.toList midiEvents.tracks)
  where
    getEvents :: (TrackName, IM.IntMap [MidiEvent]) -> Maybe (TrackName, [MidiEvent])
    getEvents (name, im) = case IM.lookup pos im of
        Nothing -> Nothing
        Just xs -> Just (name, xs)

decodeMidiFile :: Maybe Double -> (MonadIO m) => FilePath -> m MidiEvents
decodeMidiFile mMidiTimeScale fp = liftIO do
    Sound.MIDI.File.Cons _ td midiTracks <- Sound.MIDI.File.explicitNoteOff <$> Sound.MIDI.File.Load.fromFile fp

    let
        -- Convert track events from tick time to real time
        normalizedTracks :: [[(Double, _)]]
        normalizedTracks = map (map (first (fromRational . NN.toNumber)) . TB.toPairList . Sound.MIDI.File.secondsFromTicks td) midiTracks

        -- Process the events into a MidiAccum
        allTracks :: [MidiAccum]
        allTracks = map (addLastFrame . foldl' groupEventPerFrame (MidiAccum 0 0 mempty mempty mempty)) normalizedTracks

        -- Inject the events of the last frame
        addLastFrame :: MidiAccum -> MidiAccum
        addLastFrame acc = case acc.current of
            [] -> acc
            xs -> acc{events = IM.insert acc.frame (reverse xs) acc.events, current = []}

        -- Create the final map
        tracks = Map.fromListWith mergeEvents $ map (\ma -> (ma.name, ma.events)) $ allTracks
          where
            mergeEvents :: IM.IntMap [MidiEvent] -> IM.IntMap [MidiEvent] -> IM.IntMap [MidiEvent]
            mergeEvents = IM.unionWith (++)

        -- Compute the total length of the midi file
        endFrame = fromIntegral $ maximum $ map (.frame) allTracks

    putStrLn (from $ fp <> ": end frame " <> show endFrame)
    pure $ MidiEvents{..}
  where
    groupEventPerFrame :: MidiAccum -> (Double, EV.T) -> MidiAccum
    groupEventPerFrame acc (evElapsed, ev) = newAcc
      where
        newAcc = MidiAccum elapsed frame name current events

        elapsed :: Double
        elapsed = acc.elapsed + evElapsed

        -- TODO: figure out why reaper MIDI export sometime needs that...
        reaperOffset = fromMaybe 1 mMidiTimeScale
        -- to adjust:
        -- let t1 = the last midi event frame,
        -- let t2 = the actual frame
        -- t1 = (5*60+38.558)*60 = 20313
        -- t2 = 20412
        -- in t1 / t2

        frame :: Int
        frame = floor (reaperOffset * elapsed / frameTime)

        name = case ev of
            EV.MetaEvent (Meta.TrackName n) -> TrackName $ from n
            _ -> acc.name

        (frameCurrent, events)
            | frame == acc.frame = (acc.current, acc.events)
            | -- we are in a new frame
              otherwise =
                ( -- reset the current events
                  []
                , -- add the current events to the intmap
                  case acc.current of
                    [] -> acc.events
                    _ -> IM.insert acc.frame (reverse acc.current) acc.events
                )

        current = case toMidiEv ev of
            Nothing -> frameCurrent
            Just mev -> mev : frameCurrent

    toMidiEv :: EV.T -> Maybe MidiEvent
    toMidiEv = \case
        EV.MIDIEvent (Chan.Cons _ (Chan.Voice (Voice.NoteOn pitch vel))) ->
            let velf = fromIntegral (Voice.fromVelocity vel) / 127
             in Just (MidiNoteOn (Voice.fromPitch pitch) velf)
        EV.MIDIEvent (Chan.Cons _ (Chan.Voice (Voice.NoteOff pitch _vel))) ->
            Just (MidiNoteOff (Voice.fromPitch pitch))
        _ev -> Nothing
