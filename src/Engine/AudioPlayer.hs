module Engine.AudioPlayer (
    AudioPlayer,
    createPlayer,
    playFrame,
    stopPlayer,
    demo,
) where

import Control.Concurrent hiding (yield)
import Data.ByteString.Internal (fromForeignPtr0)
import Data.Vector.Storable qualified as SV
import Engine.AudioDecoder (Samples, decodeFile)
import Foreign.ForeignPtr (castForeignPtr)
import Frelude
import Sound.Pulse.Simple qualified as PS

data AudioPlayerEvent = PlayFrame Samples | StopPlayer

data AudioPlayer = AudioPlayer
    { chan :: TChan AudioPlayerEvent
    , _thread :: ThreadId
    }

playFrame :: (MonadIO m) => AudioPlayer -> Samples -> m ()
playFrame ap arr = atomically $ writeTChan ap.chan (PlayFrame arr)

stopPlayer :: (MonadIO m) => AudioPlayer -> m ()
stopPlayer ap = atomically $ writeTChan ap.chan StopPlayer

createPlayer :: (MonadIO m) => m AudioPlayer
createPlayer = do
    chan <- newTChanIO

    mClientV <- newTVarIO Nothing

    let stopClient = do
            atomically (readTVar mClientV) >>= \case
                Nothing -> pure ()
                Just client -> do
                    putStrLn "Stopping client..."
                    PS.simpleDrain client
                    freeClient client
                    atomically $ writeTVar mClientV Nothing

        startClient :: IO PS.Simple
        startClient = do
            atomically (readTVar mClientV) >>= \case
                Just client -> pure client
                Nothing -> do
                    putStrLn "Starting client..."
                    client <- newClient name PS.Play spec
                    atomically $ writeTVar mClientV (Just client)
                    pure client

        go :: IO ()
        go = forever do
            atomically (readTChan chan) >>= \case
                StopPlayer -> stopClient
                PlayFrame arr ->
                    -- stopClient
                    playClient arr =<< startClient

    thread <- liftIO $ forkIO do
        go `finally` stopClient
    pure $ AudioPlayer chan thread
  where
    name = "animation-fractal"
    spec = PS.SampleSpec (PS.F32 PS.LittleEndian) 44100 1

playClient :: (MonadIO m) => Samples -> PS.Simple -> m ()
playClient arr client = liftIO $ PS.simpleWriteRaw client bs
  where
    (floatPtr, floatSZ) = SV.unsafeToForeignPtr0 arr
    wordPtr = castForeignPtr floatPtr
    wordSZ = floatSZ * 4
    bs = fromForeignPtr0 wordPtr wordSZ

newClient :: (MonadIO m) => String -> PS.Direction -> PS.SampleSpec -> m PS.Simple
newClient name dir spec = liftIO create
  where
    create = PS.simpleNew Nothing name dir Nothing "pulse-pipe" spec Nothing Nothing

freeClient :: (MonadIO m) => PS.Simple -> m ()
freeClient = liftIO . PS.simpleFree

demo :: FilePath -> IO ()
demo fname = do
    arr <- decodeFile fname
    player <- createPlayer

    let frameDelay = 1_000_000 `div` 60
        totalFrames = 60 * 5 - 1 :: Int
        frameSize = 44100 `div` 60

    forM_ [0 .. totalFrames] $ \frame -> do
        putStrLn $ "Playing frame: " <> show frame
        let buf = SV.slice (frame * frameSize) (frameSize) arr
        playFrame player buf
        Frelude.threadDelay frameDelay

    Frelude.threadDelay 5_000_000
    putStrLn "over"
