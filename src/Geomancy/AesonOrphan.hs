{-# OPTIONS_GHC -Wno-orphans #-}

module Geomancy.AesonOrphan where

import Geomancy.Vec3 qualified as Vec3
import RIO

import Data.Aeson
import Data.Vector qualified as Vector
import Geomancy

import Engine.Window.Key (Key)

instance FromJSON Engine.Window.Key.Key
instance ToJSON Engine.Window.Key.Key

instance FromJSON Vec2 where
    parseJSON = withArray "Vec2" $ \a -> case Vector.toList a of
        [x, y] -> vec2 <$> parseJSON x <*> parseJSON y
        _ -> fail "array missing elements"

instance ToJSON Vec2 where
    toJSON (WithVec2 x y) = Array $ Vector.fromList [toJSON x, toJSON y]

instance FromJSON Vec3 where
    parseJSON = withArray "Vec3" $ \a -> case Vector.toList a of
        [x, y, z] -> vec3 <$> parseJSON x <*> parseJSON y <*> parseJSON z
        _ -> fail "array missing elements"

instance ToJSON Vec3 where
    toJSON (WithVec3 x y z) = Array $ Vector.fromList [toJSON x, toJSON y, toJSON z]

instance FromJSON Vec3.Packed where
    parseJSON v = Vec3.Packed <$> parseJSON v

instance ToJSON Vec3.Packed where
    toJSON (Vec3.Packed v) = toJSON v

instance FromJSON IVec2 where
    parseJSON = withArray "IVec2" $ \a -> case Vector.toList a of
        [x, y] -> ivec2 <$> parseJSON x <*> parseJSON y
        _ -> fail "array missing elements"

instance ToJSON IVec2 where
    toJSON (WithIVec2 x y) = Array $ Vector.fromList [toJSON x, toJSON y]

instance FromJSON UVec2 where
    parseJSON = withArray "UVec2" $ \a -> case Vector.toList a of
        [x, y] -> uvec2 <$> parseJSON x <*> parseJSON y
        _ -> fail "array missing elements"

instance ToJSON UVec2 where
    toJSON (WithUVec2 x y) = Array $ Vector.fromList [toJSON x, toJSON y]

instance FromJSON Vec4 where
    parseJSON = withArray "Vec4" $ \a -> case Vector.toList a of
        [x, y, z, v] -> vec4 <$> parseJSON x <*> parseJSON y <*> parseJSON z <*> parseJSON v
        _ -> fail "array missing elements"

instance ToJSON Vec4 where
    toJSON (WithVec4 x y z v) = Array $ Vector.fromList [toJSON x, toJSON y, toJSON z, toJSON v]
