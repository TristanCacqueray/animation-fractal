module Geomancy.Lens where

import Geomancy
import RIO

v2x :: Lens' Vec2 Float
v2x = lens getv setv
  where
    getv :: Vec2 -> Float
    getv (WithVec2 x _) = x
    setv :: Vec2 -> Float -> Vec2
    setv (WithVec2 _ y) newX = vec2 newX y

v2y :: Lens' Vec2 Float
v2y = lens getv setv
  where
    getv :: Vec2 -> Float
    getv (WithVec2 _ y) = y
    setv :: Vec2 -> Float -> Vec2
    setv (WithVec2 x _) = vec2 x

v4x :: Lens' Vec4 Float
v4x = lens getv setv
  where
    getv :: Vec4 -> Float
    getv (WithVec4 x _ _ _) = x
    setv :: Vec4 -> Float -> Vec4
    setv (WithVec4 _ y z w) newX = vec4 newX y z w

v4y :: Lens' Vec4 Float
v4y = lens getv setv
  where
    getv :: Vec4 -> Float
    getv (WithVec4 _ y _ _) = y
    setv :: Vec4 -> Float -> Vec4
    setv (WithVec4 x _ z w) newY = vec4 x newY z w

v4z :: Lens' Vec4 Float
v4z = lens getv setv
  where
    getv :: Vec4 -> Float
    getv (WithVec4 _ _ z _) = z
    setv :: Vec4 -> Float -> Vec4
    setv (WithVec4 x y _ w) newZ = vec4 x y newZ w
