module Stage.Fractal.Events (
    EventVars (..),
    handler,
    callbacks,
) where

import Frelude hiding (Down)

import Engine.Events qualified as Events
import Engine.Events.Sink (MonadSink, Sink (..))
import Engine.StageSwitch (trySwitchStage)
import Engine.Types qualified as Keid
import Engine.Worker qualified as Worker
import Render.ImGui qualified as ImGui
import UnliftIO.Resource (ReleaseKey)

import Graphics.UI.GLFW qualified as GLFW

import Engine.Events.CursorPos qualified as CursorPos

import Engine.Events.MouseButton qualified as MouseButton
import Engine.Window.MouseButton qualified as MB

import Engine.Window.Scroll qualified as Scroll

import Engine.Window.Key (Key (..), KeyState (..))
import Engine.Window.Key qualified as Key

import Engine.Types qualified as Engine

import Vulkan.Core10 qualified as Vk

import AnimationFractal.Input.Key (KeyHandlers, handleKey)
import AnimationFractal.Scene
import AnimationFractal.Script
import AnimationFractal.Time

data ScrollDir = Up | Down
    deriving (Eq, Show, Generic)

data Event
    = Abort
    | ResetParams
    | Recenter Vec2
    | Scroll ScrollDir
    | Screenshot
    | ToggleFullscreen
    | MoveForward Frame
    | MoveBackward Frame
    | Jump ScrollDir
    deriving (Eq, Show, Generic)

data EventVars = EventVars
    { zoom :: StateVar Float
    , origin :: StateVar Vec2
    , markers :: [Frame]
    , screenshotToggle :: TVar Bool
    , sceneInfo :: SceneInfo
    , staticScript :: Maybe StaticScript
    }

readStateVar :: (MonadIO m) => StateVar a -> m a
readStateVar (StateVar read _) = liftIO read

writeStateVar :: (MonadIO m) => StateVar a -> a -> m ()
writeStateVar (StateVar _ write) = liftIO . write

handler :: EventVars -> Event -> RIO (App Keid.GlobalHandles runState) ()
handler vars = \case
    Abort -> void $ trySwitchStage Keid.Finish
    Recenter (WithVec2 pWidth pHeight) -> do
        let extent2ivec Vk.Extent2D{width, height} = (fromIntegral width, fromIntegral height)
        (rWidth, rHeight) <- extent2ivec <$> (Worker.readVar =<< Engine.askScreenVar)
        -- box the coordinate without the dear controller
        let scenePos = vec2 (pWidth - dearImGuiWidth) pHeight
            sceneDim = ivec2 (rWidth - dearImGuiWidth) (rHeight - dearImGuiHeight)
        logInfo $ "Clicked " <> displayShow scenePos <> ", dim is " <> displayShow sceneDim
        origin <- readStateVar vars.origin
        zoom <- readStateVar vars.zoom
        writeStateVar vars.origin $ pos2Coord sceneDim origin zoom scenePos
    Scroll sdir -> do
        zoom <- readStateVar vars.zoom
        let offset = case sdir of
                Up -> 0.1
                Down -> -0.1
            newZoom = zoom + zoom * offset
        writeStateVar vars.zoom newZoom
    Screenshot -> do
        logInfo "Screenshoting!"
        atomically $ writeTVar vars.screenshotToggle True
    MoveForward offset -> do
        atomically $ modifyTVar' vars.sceneInfo.sceneFrame (frameAdd offset)
    MoveBackward offset -> do
        atomically $ modifyTVar' vars.sceneInfo.sceneFrame (frameSub offset)
    Jump sdir -> case vars.staticScript of
        Nothing -> atomically do
            current <- readTVar vars.sceneInfo.sceneFrame
            let findNextFrame :: Maybe Frame -> [Frame] -> Maybe Frame
                findNextFrame prev = \case
                    [] -> prev
                    (f : rest)
                        | current < f -> Just f
                        | otherwise -> findNextFrame (Just f) rest
                kfs = case sdir of
                    Down -> Just 0
                    Up -> findNextFrame Nothing vars.sceneInfo.keyFrames
            case kfs of
                Just nframe -> do
                    {-
                    let prev = case sdir of
                            Down -> 0
                            Up -> current
                    inps <- readTVar vars.sceneInfo.inputs
                    forM_ [prev .. nframe] \frame ->
                    -}
                    writeTVar vars.sceneInfo.sceneFrame nframe
                Nothing -> pure ()
        Just staticScript -> atomically do
            current <- readTVar vars.sceneInfo.sceneFrame
            keyFrames <- readTVar staticScript.keyFrames
            allFrames <- readTVar staticScript.frames
            let findNextFrame :: [Frame] -> Frame
                findNextFrame = \case
                    (x : rest)
                        | current < x -> x
                        | otherwise -> findNextFrame rest
                    [] -> from $ length allFrames
                findPrevFrame = \case
                    (x : rest)
                        | current > x -> x
                        | otherwise -> findPrevFrame rest
                    [] -> 0
                nextFrame = case sdir of
                    Down -> findPrevFrame . reverse
                    Up -> findNextFrame
            writeTVar vars.sceneInfo.sceneFrame (nextFrame keyFrames)
    ToggleFullscreen -> do
        win <- asks (Engine.ghWindow . appEnv)
        liftIO (GLFW.getWindowMonitor win) >>= \case
            Nothing -> setFullscreen win
            Just _ -> liftIO $ GLFW.setWindowed win 200 200 10 10
    ev -> logInfo $ "todo handle: " <> displayShow ev
  where
    setFullscreen win = do
        liftIO (GLFW.getPrimaryMonitor) >>= \case
            Nothing -> logError "No monitor found!"
            Just monitor ->
                liftIO (GLFW.getVideoMode monitor) >>= \case
                    Nothing -> logError "No video mode found!"
                    Just mode -> liftIO $ GLFW.setFullscreen win monitor mode
callbacks :: (MonadSink runState m) => KeyHandlers -> Worker.Var Vec2 -> [Sink Event runState -> m ReleaseKey]
callbacks kh cursorPos =
    [ CursorPos.callback cursorPos
    , MouseButton.callback cursorPos clickHandler
    , Scroll.callback . scrollHandler
    , Key.callback . keyHandler kh
    ]

keyHandler :: (MonadSink runState m) => KeyHandlers -> Sink Event runState -> Key.Callback m
keyHandler kh (Events.Sink signal) keyCode keyEvent@(mods, state, key) = do
    ImGui.capturingKeyboard do
        logInfo $ "Key event (" <> display keyCode <> "): " <> displayShow keyEvent
        case key of
            Key'Escape -> signal Abort
            Key'Left | shifted && pressed -> signal (Jump Down)
            Key'Left | pressed -> signal (MoveBackward 180)
            Key'Right | shifted && pressed -> signal (Jump Up)
            Key'Right | pressed -> signal (MoveForward 180)
            Key'R | pressed -> signal ResetParams
            Key'P | pressed -> signal Screenshot
            Key'F | pressed -> signal ToggleFullscreen
            _ -> handleKey kh state key
  where
    shifted = mods.modifierKeysShift
    pressed = state == KeyState'Pressed

clickHandler :: (MonadSink runState m) => MouseButton.ClickHandler Event runState m
clickHandler (Sink signal) cursorPos (_, buttonState, _) = do
    ImGui.capturingMouse do
        when (buttonState == MB.MouseButtonState'Released) do
            signal $ Recenter cursorPos

scrollHandler :: (MonadSink runState m) => Sink Event runState -> Scroll.Callback m
scrollHandler (Sink signal) x y = ImGui.capturingMouse action
  where
    action
        | x == 0 && y == 1.0 = signal $ Scroll Up
        | x == 0 && y == -1.0 = signal $ Scroll Down
        | otherwise = logError $ "Unknown scroll event: " <> displayShow x

pos2Coord :: IVec2 -> Vec2 -> Float -> Vec2 -> Vec2
pos2Coord (WithIVec2 iscreenWidth iscreenHeight) (WithVec2 centerX centerY) range (WithVec2 posX posY) =
    let (screenX, screenY) = (fromIntegral iscreenWidth, fromIntegral iscreenHeight)
        -- normalize mouse coordinate in a [0..1] range
        (uvX, uvY) = (posX / screenX, posY / screenY)
        -- move to plane coordinate and adjust for screen ratio
        coordX = 2 * (uvX - 0.5)
        coordY = 2 * (uvY - 0.5) * (-1 * screenY / screenX)
        -- adjust for center and range
        x = centerX + coordX * (1.0 / range)
        y = centerY + coordY * (1.0 / range)
     in vec2 x y
