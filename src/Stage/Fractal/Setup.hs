module Stage.Fractal.Setup (compileCode, stage) where

import Data.Text.IO qualified as Text
import Data.Vector qualified as Vector
import Frelude
import RIO.Process (HasProcessContext)

-- To tweak the window
import Graphics.UI.GLFW qualified as GLFW

import Engine.Events qualified as Events (spawn)
import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Keid
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (RenderPass (..))
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Render.Draw qualified as Draw
import Render.Pass qualified as Pass
import Render.Samplers qualified as Samplers
import Resource.Region qualified as Region

-- To create texture:
import Render.Pass.Offscreen (Offscreen)
import Render.Pass.Offscreen qualified as Offscreen

-- To draw on screen:
import Render.ForwardMsaa (ForwardMsaa)
import Render.ForwardMsaa qualified as ForwardMsaa

import Render.AnimationFractal.Code qualified as AFPipeline
import Render.AnimationFractal.Model qualified as AFPipeline
import Render.AnimationFractal.Pipeline qualified as AFPipeline
import Render.Fullscreen.Pipeline qualified as Fullscreen

import Engine.SpirV.Compile qualified as Compile
import Engine.Vulkan.Pipeline.External qualified as External

import DearImGui qualified
import Engine.Types qualified as Engine
import Engine.Worker qualified as Worker
import Render.ImGui qualified as ImGui
import UnliftIO.Resource qualified as Resource

import Stage.Fractal.Events qualified as Events

import AnimationFractal.Input qualified as AF
import AnimationFractal.Modulation qualified as AF
import AnimationFractal.Player qualified as AF
import AnimationFractal.Scene
import AnimationFractal.Scene qualified as AF
import AnimationFractal.Time
import AnimationFractal.Variable qualified as AF

import AnimationFractal.Input.Key (KeyHandlers)
import AnimationFractal.Script

import Data.ByteString.Lazy qualified as BSL
import Engine.Screenshot qualified as Screenshot
import Engine.VideoRecorder qualified as VideoRecorder
import Resource.Image qualified as Image

data RunState = RunState
    { -- The scene managed by dear imgui and event handlers
      rsScene :: Worker.Var Scene
    , -- The final scene, updated when resolution change
      rsSceneP :: Worker.Merge Scene
    , -- Trigger to re-render the texture
      rsUpdate :: Worker.Var ()
    , -- Windows info
      rsCursorPos :: Worker.Var Vec2
    , -- Timing info
      rsFPS :: TVar Int
    , rsFrameCount :: TVar Word64
    , rsLastFrameCreatedAt :: TVar (Maybe AF.MSec)
    , rsLastFrame :: TVar Frame
    , -- recorder
      rsRecorder :: TVar (Maybe VideoRecorder.VideoRecorder)
    , rsScreenshot :: TVar Bool
    }

data RenderPasses = RenderPasses
    { -- The forwardMsaa pass renders the final image
      rpForwardMsaa :: ForwardMsaa
    , -- The offscreen pass renders the texture
      rpOffscreen :: Offscreen
    }

data Pipelines = Pipelines
    { -- The fullscreen pipeline draws the texture on the final image
      pFullscreen :: Fullscreen.Pipeline
    , -- The animationFractal pipeline draws the fractal on the texture.
      -- It's wrapped by an External process to be reloaded when the shader changes.
      -- See 'frFratalExternalObs' frame resource.
      pFractalExternalProc :: External.Process AFPipeline.Config
    }

data FrameResources = FrameResources
    { -- The texture used by the fullscreen pipeline
      frTexture :: Fullscreen.FrameResource '[Fullscreen.FullscreenTexture]
    , -- The parameters used by the mandelbrot pipeline
      frScene :: AFPipeline.FrameResource '[AF.Scene]
    , -- The observer to reload the fractal shader when the code change.
      frFractalExternalObs :: External.Observer AFPipeline.Pipeline
    , -- The cpu image for screenshot
      frScreenshotImage :: Screenshot.ScreenshotImage
    }

-- Engine types
type StageFrameRIO a = Keid.StageFrameRIO RenderPasses Pipelines FrameResources RunState a

stage :: AF.AFContext -> SceneInfo -> KeyHandlers -> Keid.Stage RenderPasses Pipelines FrameResources RunState
stage ctx sceneInfo kh = Stage.assemble sceneInfo.name rendering resources scenes
  where
    -- The settings for the fractal texture

    ratio = case ctx.render of
        Nothing -> 3
        Just _ -> 1

    sExtent = Vk.Extent2D (oWidth `div` ratio) (oHeight `div` ratio)
    sFormat = Vk.FORMAT_R8G8B8A8_SRGB
    sSamples = Vk.SAMPLE_COUNT_1_BIT

    settingsStatic swapchain =
        Offscreen.Settings
            { sLabel = "Static"
            , sLayers = 1
            , sMultiView = False
            , sSamples
            , sExtent
            , sFormat
            , sDepthFormat = Swapchain.getDepthFormat swapchain
            , sMipMap = False
            , sColorLayout = Nothing
            , sDepthLayout = Nothing
            }

    rendering =
        Stage.Rendering
            { Stage.rAllocateRP = \swapchain -> do
                rpForwardMsaa <- ForwardMsaa.allocateMsaa swapchain
                rpOffscreen <- Offscreen.allocate (settingsStatic swapchain)
                pure RenderPasses{..}
            , Stage.rAllocateP = \swapchain rps -> do
                void $! ImGui.allocate swapchain rps.rpForwardMsaa 0
                samplers <- Samplers.allocate (Swapchain.getAnisotropy swapchain)
                pFullscreen <- Fullscreen.allocate (Swapchain.getMultisample swapchain) (Fullscreen.mkBindings samplers) rps.rpForwardMsaa

                -- Hot reload the fragment when it changes
                let basicSceneBinds = AFPipeline.mkBindings
                    fragPath = shaderDir </> from sceneInfo.name <.> "frag" <.> "spv"
                    base = AFPipeline.config basicSceneBinds
                    stageFiles =
                        Graphics.basicStages
                            (shaderDir </> from sceneInfo.name <.> "vert" <.> "spv")
                            fragPath

                -- Ensure the current fragment is built
                fragExist <- doesFileExist fragPath
                unless fragExist do
                    logInfo $ "Rebuilding " <> displayShow fragPath
                    lift $ compileCode sceneInfo

                pFractalExternalProc <-
                    External.spawnReflect (from sceneInfo.name) stageFiles \(stageCode, reflect) ->
                        base
                            { Graphics.cStages = stageCode
                            , Graphics.cReflect = Just reflect
                            }

                pure Pipelines{..}
            }

    resources =
        Stage.Resources
            { Stage.rInitialRR = \_pool rp p -> do
                frFractalExternalObs <- External.newObserverGraphics rp.rpOffscreen sSamples p.pFractalExternalProc
                (_, pAnimationFractal) <- Worker.readObservedIO frFractalExternalObs

                -- The final texture is the offscreen output.
                let tex = Offscreen.colorTexture rp.rpOffscreen
                frTexture <- Fullscreen.allocateFR (Vector.head <$> p.pFullscreen.pDescLayouts) tex
                -- The scene is updated by the scene updateBuffers.
                frScene <- AFPipeline.allocateFrameResource (Vector.head <$> pAnimationFractal.pDescLayouts)
                -- The screenshot image
                (_, frScreenshotImage) <- Screenshot.createScreenshotImage (Just sFormat) oDim
                pure $ FrameResources{..}
            , Stage.rInitialRS = initialRunState ctx sceneInfo
            }

    zoomVar = case AF.lookupVariable "zoom" sceneInfo.vars of
        Just (AF.Variable _ (AF.ControllerSliderFloat v _ _) _) -> v
        _ -> AF.makeSceneStateVar #zoom sceneInfo.state
    originVar = case AF.lookupVariable "origin" sceneInfo.vars of
        Just (AF.Variable _ (AF.ControllerVec2 v _ _) _) -> v
        _ -> AF.makeSceneStateVar #origin sceneInfo.state
    eventVars = Events.EventVars zoomVar originVar sceneInfo.keyFrames

    scenes =
        [ -- Setup handlers
          mempty
            { Stage.scBeforeLoop = do
                env <- asks appEnv
                liftIO do
                    GLFW.setWindowAttrib env.ghWindow GLFW.WindowAttrib'Decorated False
                    GLFW.setWindowAttrib env.ghWindow GLFW.WindowAttrib'Floating True
                    GLFW.setWindowAttrib env.ghWindow GLFW.WindowAttrib'Resizable False
                    GLFW.setWindowTitle env.ghWindow "Animation Fractal"
                    GLFW.setWindowOpacity env.ghWindow 1.0

                cursorWindow <- gets rsCursorPos
                screenshotToggle <- gets rsScreenshot
                let evars = eventVars screenshotToggle sceneInfo ctx.staticScript
                void $! Region.local do
                    Events.spawn (Events.handler evars) (Events.callbacks kh cursorWindow)
            }
        , -- , -- XXX: this does not work, see the comment in the recordCommands
          --  mempty{Stage.scUpdateBuffers = \rs fr -> AF.observe rs.rsSceneP fr.frScene}
          --
          -- Final draw
          mempty
            { Stage.scUpdateBuffers = \_rs fr -> do
                frame <- asks snd
                let
                    -- XXX: ugh...
                    basicSceneBinds = Tagged [unTagged AFPipeline.mkBindings]
                External.observeGraphics
                    frame.fRenderpass.rpOffscreen
                    sSamples
                    basicSceneBinds
                    frame.fPipelines.pFractalExternalProc
                    fr.frFractalExternalObs
            , Stage.scRecordCommands = recordCommands ctx sceneInfo
            , Stage.scBeforeLoop = ImGui.allocateLoop True
            }
        ]

initialRunState :: AF.AFContext -> AF.SceneInfo -> Keid.StageSetupRIO (Resource.ReleaseKey, RunState)
initialRunState ctx sceneInfo = Region.run do
    rsFPS <- newTVarIO 42
    rsLastFrameCreatedAt <- newTVarIO Nothing
    rsFrameCount <- newTVarIO 0
    let rsScene = sceneInfo.state

    -- Create the initial scene
    screen <- Engine.askScreenVar
    rsCursorPos <- Worker.newVar 0

    void $ Worker.spawnTimed_ True 1_000_000 () do
        -- Every second, reads and reset the frameCount
        atomically do
            fc <- stateTVar rsFrameCount \fc -> (fc, 0)
            writeTVar rsFPS (fromIntegral fc)

    -- Update the final scene when the resolution or the params change
    rsSceneP <-
        Worker.spawnMerge2
            ( \Vk.Extent2D{width, height} r ->
                let fheight = height - dearImGuiHeight
                    fwidth = width - dearImGuiWidth
                 in r
                        { AF.screenRatio =
                            -1 * fromIntegral fheight / fromIntegral fwidth
                        , AF.screenResolution =
                            uvec2 fwidth fheight
                        }
            )
            screen
            sceneInfo.state

    rsUpdate <- Worker.newVar ()

    let mFile = AF.mediaFile <$> sceneInfo.mMediaPlayer
    rsRecorder <-
        newTVarIO =<< case ctx.render of
            Nothing -> pure Nothing
            Just (fp, _) -> Just <$> lift (VideoRecorder.createRecorder mFile fp)
    rsScreenshot <- newTVarIO False
    rsLastFrame <- newTVarIO =<< readTVarIO sceneInfo.sceneFrame
    pure RunState{..}

recordCommands :: AF.AFContext -> AF.SceneInfo -> Vk.CommandBuffer -> FrameResources -> Word32 -> StageFrameRIO ()
recordCommands ctx sceneInfo cb fr imageIndex = do
    Keid.Frame{fRenderpass, fPipelines} <- asks snd

    -- Read the scene state
    sceneP <- gets rsSceneP
    sceneState <- readTVarIO sceneP.mOutput
    let WithUVec2 fractalWidth fractalHeight = sceneState.vData.screenResolution

    -- Take a screenshot when requested
    do
        rsScreenshotV <- gets rsScreenshot
        whenM (readTVarIO rsScreenshotV) do
            atomically $ writeTVar rsScreenshotV False
            takeSingleScreenshot fRenderpass

    -- Update frame counters
    running <- readTVarIO sceneInfo.running
    currentFrame <- readTVarIO sceneInfo.sceneFrame
    frameTimeDelta <- if running then computeElapsedTime currentFrame else pure 0
    do
        frameCountV <- gets rsFrameCount
        atomically do
            modifyTVar' frameCountV (+ 1)

            when running do
                modifyTVar' sceneInfo.sceneTime (+ frameTimeDelta)
                modifyTVar' sceneInfo.sceneFrame (+ 1)
                modifyTVar' sceneInfo.clipTime (+ frameTimeDelta)

    -- Apply static script
    lastFrameV <- gets rsLastFrame
    applyStaticScript running currentFrame =<< readTVarIO lastFrameV
    atomically $ writeTVar lastFrameV currentFrame

    -- Load next clip when requested
    loadNextClip

    when running do
        inps <- readTVarIO sceneInfo.inputs
        forM_ inps \inp -> liftIO (inp.advance currentFrame)
    mods <- reverse <$> atomically (AF.modulationGroupList sceneInfo.modulations)
    AF.applyModulations frameTimeDelta (AF.sceneMods sceneInfo) mods

    recorderV <- gets rsRecorder
    mRecorder <- readTVarIO recorderV

    -- Render controllers
    dear <-
        snd <$> ImGui.mkDrawData do
            -- dock the controller window to the top left
            let cond = DearImGui.ImGuiCond 0
            winPos <- newIORef $ DearImGui.ImVec2 0 0
            DearImGui.setNextWindowPos winPos cond Nothing
            DearImGui.setNextWindowCollapsed False cond

            -- maximize the window to full height and hardcoded dearImGuiWidth
            winSize <- newIORef $ DearImGui.ImVec2 dearImGuiWidth (fromIntegral fractalHeight + dearImGuiHeight)
            DearImGui.setNextWindowSize winSize cond

            let ctrlFlags =
                    DearImGui.ImGuiWindowFlags_NoCollapse
                        .|. DearImGui.ImGuiWindowFlags_NoMove
            -- .|. DearImGui.ImGuiWindowFlags_NoDecoration
            _ <- dearImGui'beginWithFlags "Variables and modulations" ctrlFlags
            AF.renderVariables sceneInfo.vars
            AF.renderInputs sceneInfo.inputs
            AF.renderModulations (sceneMods sceneInfo) mods
            AF.renderAddModulationButton sceneInfo.modulations ctx.modulations
            AF.renderAddInputButton ctx.inputs sceneInfo.modulations sceneInfo.inputs
            DearImGui.end

            -- dock scene controller to the bottom
            sceneControllerPos <- newIORef $ DearImGui.ImVec2 dearImGuiWidth (fromIntegral fractalHeight)
            DearImGui.setNextWindowPos sceneControllerPos cond Nothing
            DearImGui.setNextWindowCollapsed False cond
            sceneControllerSize <- newIORef $ DearImGui.ImVec2 (fromIntegral fractalWidth) dearImGuiHeight
            DearImGui.setNextWindowSize sceneControllerSize cond
            let sceneCtrlFlags =
                    DearImGui.ImGuiWindowFlags_NoCollapse
                        .|. DearImGui.ImGuiWindowFlags_NoMove
                        .|. DearImGui.ImGuiWindowFlags_NoDecoration
            -- .|. DearImGui.ImGuiWindowFlags_NoBackground
            _ <- dearImGui'beginWithFlags "##sceneCtrl" sceneCtrlFlags
            whenM (DearImGui.button "|<") do
                -- Reset the scene time
                atomically do
                    writeTVar sceneInfo.sceneTime 0
                    writeTVar sceneInfo.sceneFrame 0
                    traverse_ (.reset) mods

            DearImGui.sameLine

            let playPause = bool "|>" "||" running
            whenM (DearImGui.button playPause) do
                -- TODO: abort modulation?
                -- Toggle running state
                atomically $ modifyTVar' sceneInfo.running not
                when running do
                    forM_ sceneInfo.mMediaPlayer AF.stopMediaPlayer
            DearImGui.sameLine

            -- show scene time
            sceneFrame <- readTVarIO sceneInfo.sceneFrame
            DearImGui.text (AF.showMSec (frame2time sceneFrame))
            DearImGui.sameLine
            DearImGui.text (from $ show sceneFrame)
            DearImGui.sameLine

            -- show fps
            fps <- readTVarIO =<< gets rsFPS
            DearImGui.text (from (show fps) <> " fps")
            DearImGui.sameLine

            recorderButton <- do
                case mRecorder of
                    Nothing -> pure "rec"
                    Just recorder -> do
                        (status, backlog) <- VideoRecorder.recorderStatus recorder
                        let inf = case backlog of
                                0 -> ""
                                _ -> "-" <> from (show backlog)

                        pure $ case status of
                            VideoRecorder.Running -> "stop-rec" <> inf
                            VideoRecorder.Stopping -> "stopping" <> inf
                            VideoRecorder.Done -> "done"

            whenM (DearImGui.button recorderButton) do
                case mRecorder of
                    Just recorder -> do
                        VideoRecorder.stopRecorder recorder
                    Nothing -> do
                        atomically . writeTVar recorderV . Just =<< VideoRecorder.createRecorder Nothing "recording.mp4"
            DearImGui.sameLine

            -- scene controller
            AF.renderSceneInfo ctx sceneInfo

            DearImGui.end

    -- Read pipeline state and check if update is necessary
    uvPipeline <- readIORef fr.frFractalExternalObs
    let sceneVersion = sceneState.vVersion <> uvPipeline.vVersion
    updateVarRef <- gets rsUpdate
    updateVar <- readTVarIO updateVarRef

    when (sceneVersion > updateVar.vVersion) do
        atomically $ writeTVar updateVarRef (updateVar{Worker.vVersion = sceneVersion})
        -- Note: this should be done in the updateBuffer, but there are currently some ordering issue when doing so.
        AFPipeline.observe sceneP fr.frScene

        Pass.usePass fRenderpass.rpOffscreen imageIndex cb do
            let viewport = fRenderpass.rpOffscreen.oRenderArea
            Swapchain.setDynamic cb viewport viewport

            let (_, pAnimationFractal) = uvPipeline.vData
            AFPipeline.withBoundSet0 fr.frScene pAnimationFractal cb do
                Graphics.bind cb pAnimationFractal do
                    Draw.triangle_ cb
        -- logInfo $ "Rendered the fractal!: " <> displayShow sceneVersion <> ", " <> displayShow updateVar.vVersion
        pure ()

    forM_ mRecorder $ \recorder -> do
        (status, backlog) <- VideoRecorder.recorderStatus recorder
        when (status == VideoRecorder.Running) do
            recordScreenshot fRenderpass recorder
            logInfo $ "Recorded " <> displayShow currentFrame
            when (backlog > 100) do
                logInfo "Pausing rendering to clear backlog"
                threadDelay 5_000_000

        forM_ ctx.render $ \(_, endFrame) -> do
            when (currentFrame >= endFrame && status == VideoRecorder.Running) do
                VideoRecorder.stopRecorder recorder
            when (status == VideoRecorder.Done) do
                logInfo "All good (<insert-encoding-speed>)"
                exitSuccess

    -- Draw the framebuffer on screen
    Pass.usePass fRenderpass.rpForwardMsaa imageIndex cb do
        -- Swapchain.setDynamicFullscreen cb fSwapchainResources
        -- let current = fSwapchainResources.srInfo.siImageExtent
        let viewport =
                Vk.Rect2D
                    { -- draw the final texture after the dear-imgui controller
                      extent = Vk.Extent2D fractalWidth fractalHeight
                    , offset = Vk.Offset2D dearImGuiWidth 0
                    }
        Swapchain.setDynamic cb viewport viewport

        Fullscreen.withBoundSet0 fr.frTexture fPipelines.pFullscreen cb do
            Graphics.bind cb fPipelines.pFullscreen do
                Draw.triangle_ cb

        ImGui.draw dear cb
  where
    takeSingleScreenshot fRenderpass = do
        let srcImage = fRenderpass.rpOffscreen.oColor.aiImage
        -- Screenshot.makeScreenshot "screenshot" (oWidth, oHeight) srcImage
        begin <- AF.getMonotonicTimeMS
        Screenshot.recordScreenshotCommand fr.frScreenshotImage srcImage
        timeName <- formatTime defaultTimeLocale "%F_%T" <$> getCurrentTime
        liftIO do
            img <- Screenshot.readImageMassiv fr.frScreenshotImage
            bs <- Screenshot.encodeImageMassiv img
            BSL.writeFile ("screenshot-" <> timeName <> ".bmp") bs
            pure ()
        end <- AF.getMonotonicTimeMS
        logInfo $ "Wrote screenshot in " <> displayShow (end - begin)

    recordScreenshot fRenderpass recorder = do
        let srcImage = fRenderpass.rpOffscreen.oColor.aiImage
        -- Screenshot.makeScreenshot "screenshot" (oWidth, oHeight) srcImage
        begin <- AF.getMonotonicTimeMS
        Screenshot.recordScreenshotCommand fr.frScreenshotImage srcImage
        liftIO do
            img <- Screenshot.readImageMassiv fr.frScreenshotImage
            recorder `VideoRecorder.recordFrame` img
        end <- AF.getMonotonicTimeMS
        logInfo $ "Recorded screenshot in " <> displayShow (end - begin)

    computeElapsedTime currentFrame = case sceneInfo.mMediaPlayer of
        Nothing -> do
            now <- AF.getMonotonicTimeMS
            lastFrameCreatedAtV <- gets rsLastFrameCreatedAt
            atomically do
                prev <- readTVar lastFrameCreatedAtV
                writeTVar lastFrameCreatedAtV (Just now)
                pure $ now - fromMaybe now prev
        Just mediaPlayer -> do
            when (isNothing ctx.render) do
                AF.playFrameSample mediaPlayer currentFrame
            -- We are driven by the player, the frame time is fixed
            pure (1_000 / 60)

    applyStaticScript running currentFrame lastFrame = do
        forM_ ctx.staticScript $ \staticScript -> do
            scriptUpdated <- isScriptUpdated staticScript
            when (running || currentFrame /= lastFrame || scriptUpdated) do
                varsValues <- readScriptVarsValues staticScript currentFrame
                let outVars = AF.sceneMods sceneInfo
                forM_ varsValues $ \(name, value) -> case AF.lookupOutVar name outVars of
                    Nothing -> putTextLn $ "Unknown outVar: " <> name
                    Just outVar -> outVar.current $= value

    loadNextClip = do
        mNextClip <- atomically do
            clipNext <- readTVar sceneInfo.clipNext
            clipTime <- readTVar sceneInfo.clipTime
            pure $ case clipNext of
                Just (clipLength, nextClip)
                    | clipTime >= clipLength ->
                        Just nextClip
                _ -> Nothing
        traverse_ (AF.applyClip ctx sceneInfo) mNextClip

instance RenderPass RenderPasses where
    updateRenderpass swapchain RenderPasses{..} =
        -- XXX: make sure to also update the refcountRenderpass impl
        RenderPasses
            <$> ForwardMsaa.updateMsaa swapchain rpForwardMsaa
            <*> updateRenderpass swapchain rpOffscreen
    refcountRenderpass RenderPasses{..} = do
        refcountRenderpass rpForwardMsaa
        refcountRenderpass rpOffscreen

shaderDir :: FilePath
shaderDir = "data" </> "shaders"

compileCode :: (HasProcessContext env, HasLogFunc env) => SceneInfo -> RIO env ()
compileCode sceneInfo = do
    code <-
        Code <$> case sceneInfo.mFragPath of
            Nothing -> pure sceneInfo.frag.unCode
            Just fp -> mappend sceneInfo.frag.unCode <$> liftIO (Text.readFile fp)
    Compile.glslStages (Just shaderDir) sceneInfo.name (Graphics.basicStages AFPipeline.vert code)
