-- | The Prelude for Animation Fractal
module Frelude (
    -- * constant
    dearImGuiWidth,
    dearImGuiHeight,
    dearImGui'beginWithFlags,
    dearImGui'inputTextFlags,
    dearYellow,
    oDim,
    oWidth,
    oHeight,

    -- * rio + extra
    module RIO,
    gets,
    module RIO.App,
    module RIO.Directory,
    module RIO.FilePath,
    module RIO.Time,
    module GHC.Float,
    module GHC.Bits,
    module Foreign.C.Types,
    module Witch,
    getMonotonicTimeNSec,
    zero,
    module Data.Tagged,
    module System.Environment,
    print,
    putStrLn,
    putTextLn,
    module Data.StateVar,
    ResourceT,
    (Control.Lens.?~),
    decodeUtf8,

    -- * scientific
    module Data.Scientific,

    -- * aeson
    module Data.Aeson,
    _Integer,
    _Integral,
    _Double,
    _Bool,
    aesonQQ,
    fromJSONE,
    fromJSON',

    -- * keid
    module Geomancy,
    StackStage (..),
    StageRIO,
    Code (..),
    glsl,
    compileFrag,
    compileVert,

    -- * test
    module Test.HUnit.Base,
) where

import Test.HUnit.Base

import Control.Lens hiding (from)
import Control.Monad.Trans.Resource (ResourceT)
import Data.Scientific hiding (normalize)
import Data.StateVar
import Data.Tagged
import Data.Text.Encoding (decodeUtf8)
import Data.Text.IO qualified as Text
import Foreign.C.Types
import GHC.Bits
import GHC.Clock qualified
import GHC.Float
import System.Environment
import Vulkan.Zero (zero)
import Prelude qualified

import Data.Generics.Labels ()

import RIO hiding (assert)
import RIO.App
import RIO.Directory
import RIO.FilePath
import RIO.State (gets)
import RIO.Time
import Witch

import Engine.Types (StackStage (..), StageRIO)
import Geomancy
import Render.Code (Code (..), compileFrag, compileVert, glsl)

import Data.Aeson
import Data.Aeson.Lens
import Data.Aeson.QQ (aesonQQ)
import GHC.IO qualified
import Geomancy.AesonOrphan ()

import DearImGui qualified
import DearImGui.Internal.Text qualified as Text
import DearImGui.Raw qualified as DearImGuiRaw
import Foreign (callocBytes, copyBytes)
import Foreign qualified as Foreign
import Foreign.C (CStringLen)
import Foreign.Marshal (free)

-- TODO: make these command line parameters
oDim :: (Word32, Word32)
oWidth, oHeight :: Word32
oDim@(oWidth, oHeight) = (1920, 1080)

getMonotonicTimeNSec :: (MonadIO m) => m Word64
getMonotonicTimeNSec = liftIO GHC.Clock.getMonotonicTimeNSec

dearImGuiWidth, dearImGuiHeight :: (Num a) => a
dearImGuiWidth = 800
dearImGuiHeight = 34

dearYellow :: IORef DearImGui.ImVec4
dearYellow = GHC.IO.unsafePerformIO (newIORef $ DearImGui.ImVec4 1 1 0 1)

print :: (MonadIO m) => (Show a) => a -> m ()
print = liftIO . Prelude.print

putStrLn :: (MonadIO m) => String -> m ()
putStrLn = liftIO . Prelude.putStrLn

putTextLn :: (MonadIO m) => (From a Text) => a -> m ()
putTextLn = liftIO . Text.putStrLn . from

fromJSONE :: (FromJSON a) => Value -> Either Text a
fromJSONE v = case fromJSON v of
    Error s -> Left (from s)
    Success a -> Right a

fromJSON' :: (FromJSON a) => Value -> a
fromJSON' v = case fromJSON v of
    Error s -> error s
    Success a -> a

-- | Create a dear-imgui window with options
--  TODO: contribute this to dear-imgui.hs
dearImGui'beginWithFlags :: (MonadIO m) => Text -> DearImGui.ImGuiWindowFlags -> m Bool
dearImGui'beginWithFlags name opts = liftIO do
    Text.withCString name \namePtr ->
        Foreign.with (CBool 1) \openPtr ->
            DearImGuiRaw.begin namePtr (Just openPtr) (Just opts)

dearImGui'inputTextFlags :: (MonadIO m) => Text -> TVar Text -> Int -> DearImGui.ImGuiInputTextFlags -> m Bool
dearImGui'inputTextFlags name ref size flags =
    withInputString ref size \bufPtrLen ->
        Text.withCString name \namePtr ->
            DearImGuiRaw.inputText namePtr bufPtrLen flags

withInputString ::
    (MonadIO m, HasSetter ref Text, HasGetter ref Text) =>
    ref ->
    Int ->
    (CStringLen -> IO Bool) ->
    m Bool
withInputString ref bufSize action = liftIO do
    input <- get ref
    Text.withCStringLen input \(refPtr, refSize) ->
        -- XXX: Allocate and zero buffer to receive imgui updates.
        bracket (mkBuf refSize) free \bufPtr -> do
            -- XXX: Copy the original input.
            copyBytes bufPtr refPtr refSize

            changed <- action (bufPtr, bufSize)

            -- XXX: Assuming Imgui wouldn't write over the bump stop so peekCString would finish.
            newValue <- Text.peekCString bufPtr
            ref $=! newValue

            return changed
  where
    mkBuf refSize =
        callocBytes
            $ max refSize bufSize
            + 5 -- XXX: max size of UTF8 code point + NUL terminator
