-- | The CLI entrypoint
module AnimationFractal (main, compile) where

import Frelude

import Data.List qualified
import Data.Map.Strict qualified as Map
import Data.Text qualified as Text
import GHC.Profiling qualified
import System.FSNotify qualified as FSNotify

import Engine.App (engineMain)

import AnimationFractal.Config (loadDemo)
import AnimationFractal.Input
import AnimationFractal.Input.AudioFile qualified as InputAudioFile
import AnimationFractal.Input.MidiFile qualified as InputMidiFile
import AnimationFractal.Modulation
import AnimationFractal.Options
import AnimationFractal.Scene
import AnimationFractal.Script
import AnimationFractal.Time
import AnimationFractal.Variable

import Stage.Fractal.Setup qualified

import AnimationFractal.Test (testImGUI)
import DearImGui qualified

-- demos
import Demo.Blank qualified as Blank
import Demo.Juicer qualified as Juicer
import Demo.Kaleidoscope1 qualified
import Demo.Kaleidoscope2 qualified
import Demo.Mandelbrot qualified as Mandelbrot
import Demo.Mandelbulb qualified as Mandelbulb
import Demo.MicroOrg qualified as MicroOrg
import Demo.SummerDawn qualified as SummerDawn
import Demo.Truchet qualified as Truchet
import Demo.UnderwaterFractalCreature qualified

import AnimationFractal.Context

import AnimationFractal.Input.Key qualified as Key

import AnimationFractal.Player qualified

demos :: [(Text, IO SceneInfo)]
demos =
    [ Blank.sceneInfo
    , Mandelbrot.sceneInfo
    , Mandelbulb.sceneInfo
    , Juicer.sceneInfo
    , Truchet.sceneInfo
    , Demo.UnderwaterFractalCreature.sceneInfo
    , MicroOrg.sceneInfo
    , Demo.Kaleidoscope1.sceneInfo
    , Demo.Kaleidoscope2.sceneInfo
    ]

compile :: String -> IO ()
compile name = case lookup (from name) demos of
    Nothing -> error $ "Unknown demo: " <> name
    Just mkScene -> do
        sceneInfo <- mkScene
        runSimpleApp $ Stage.Fractal.Setup.compileCode sceneInfo

main :: IO ()
main = do
    GHC.Profiling.stopProfTimer >> GHC.Profiling.stopHeapProfTimer
    kh <- atomically Key.newKeyHandlers

    options <-
        getArgs >>= \case
            -- special cases to run non-scene demo.
            ("audio-player" : fp : extras) -> do
                let mkState = AnimationFractal.Player.newStandalonePlayer fp extras
                testImGUI kh (liftIO mkState) AnimationFractal.Player.drawStandalonePlayer
                exitSuccess
            ["just-modulation"] -> do
                -- A scene without shader, just variables and controllers
                testImGUI kh (fst $ modulationDemo kh) (snd $ modulationDemo kh)
                exitSuccess
            ["summer"] -> do
                SummerDawn.main
                exitSuccess
            args -> do
                when ("--debug" `elem` args) do
                    setEnv "VK_INSTANCE_LAYERS" "VK_LAYER_KHRONOS_validation"
                parseOptions args

    mScene <- case options.config of
        Nothing -> pure Nothing
        Just cfg -> Just . atomically <$> loadDemo cfg

    case mScene <|> lookup options.prog demos of
        Nothing ->
            error
                . unlines
                $ ("Unknown scene: " <> show options.prog)
                : "Must be one of"
                : map (from . fst) demos
        Just found -> do
            -- create the scene
            baseScene <- found

            -- validate shader
            runSimpleApp do
                Stage.Fractal.Setup.compileCode baseScene `onException` do
                    putStrLn $ "Failed to compile shader code " <> show baseScene.mFragPath
                    putStrLn $ from $ baseScene.frag.unCode

            scene <- case baseScene.medias <> options.media of
                [] -> pure baseScene
                fp : extras -> do
                    mediaPlayer <- AnimationFractal.Player.newMediaPlayer baseScene.midiTimeScale fp extras
                    pure $ baseScene & #mMediaPlayer ?~ mediaPlayer

            let
                staticInputs = case scene.mMediaPlayer of
                    Just mp ->
                        ("audio-out", InputAudioFile.mkInput True ("audio-out", mp.samples))
                            : ( map (\extraSample -> (fst extraSample, InputAudioFile.mkInput False extraSample)) (Map.toList mp.extraSamples)
                                    <> InputMidiFile.mkInputs mp.midiEvents
                              )
                    Nothing -> []

            let
                fileReloader = case scene.mFragPath of
                    Nothing -> id
                    Just fp -> \inner -> FSNotify.withManager \mgr -> do
                        let isFrag = \case
                                FSNotify.Modified ofp _ _ -> name `Data.List.isSuffixOf` ofp
                                _ -> False
                            count = length $ Text.lines scene.frag.unCode
                            name = takeFileName fp
                            dir = takeDirectory fp
                            doCompile _ev = do
                                logInfo $ "Reloading " <> displayShow fp <> " (with " <> displayShow count <> " extra lines)"
                                Stage.Fractal.Setup.compileCode scene
                        _ <- FSNotify.watchDir mgr dir isFrag (runSimpleApp . doCompile)
                        inner

            staticScript <- case options.script of
                Just fp -> Just <$> newStaticScript fp
                Nothing -> pure Nothing

            let mMediaSize = AnimationFractal.Player.getMediaLength <$> scene.mMediaPlayer
            forM_ mMediaSize \m -> putStrLn $ "Media end frame: " <> show m
            render <- pure $ case (options.renderDemo, options.render, options.end) of
                (True, Nothing, Nothing) -> Just (from scene.name <> ".mp4", fromMaybe (error "no media size") mMediaSize)
                (True, _, _) -> error "--render-demo conflicts with --render/--end"
                (False, Just fp, Just end) -> Just (fp, from end)
                (False, Just fp, Nothing) -> Just (fp, fromMaybe (time2Frame 13_000) mMediaSize)
                (False, Nothing, _) -> Nothing

            let ctx = AFContext (allInputs kh <> staticInputs) (allModulations scene.sceneTime) staticScript render
            loadSave ctx scene options.clip

            -- TODO: support dynamic size
            let (width, height) = (oWidth `div` 2, oHeight `div` 2)

            -- adjust the window for dear-imgui controller
            let sizeStr = show (width + dearImGuiWidth) <> "x" <> show (height + dearImGuiHeight)

            -- start the engine
            let rest
                    | options.debug = ["--verbose"]
                    | otherwise = []

            when (options.debug) do
                putStrLn $ from $ baseScene.frag.unCode

            fileReloader $ withArgs ("--size" : sizeStr : rest) do
                GHC.Profiling.startProfTimer >> GHC.Profiling.startHeapProfTimer
                engineMain (StackStage $ Stage.Fractal.Setup.stage ctx scene kh)

modulationDemo :: (MonadUnliftIO m) => Key.KeyHandlers -> (m (SceneInfo, TVar MSec), (SceneInfo, TVar MSec) -> m ())
modulationDemo kh = (createState, drawState)
  where
    createState = do
        now <- getMonotonicTimeMS
        let vars =
                [ newFloatVar "v1" Nothing #var1
                , newVec2Var "vec" #origin
                ]
        atomically do
            scene <-
                newSceneInfo "just-controller" (Code mempty) [] vars
            prevV <- newTVar now
            pure $ (scene, prevV)

    drawState (sceneInfo, prevV) = runSimpleApp do
        let ctx = AFContext (allInputs kh) (allModulations sceneInfo.sceneTime) Nothing Nothing
        prev <- readTVarIO prevV
        now <- getMonotonicTimeMS
        let dt = now - prev
        atomically $ writeTVar prevV now
        mods <- reverse <$> atomically (modulationGroupList sceneInfo.modulations)
        applyModulations dt (sceneMods sceneInfo) mods
        renderVariables sceneInfo.vars
        renderModulations (sceneMods sceneInfo) mods
        renderAddModulationButton sceneInfo.modulations ctx.modulations

        renderInputs sceneInfo.inputs
        renderAddInputButton (allInputs kh) sceneInfo.modulations sceneInfo.inputs

        DearImGui.spacing
        DearImGui.textColored dearYellow "Scene:"
        renderSceneInfo ctx sceneInfo
