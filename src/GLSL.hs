-- | A GLSL library
module GLSL (
    complex,
    smoothHsv,
    turboColormap,
    palettes,
    hgSdf,
    distfunctions2d,
    camera,
) where

import Data.FileEmbed
import Frelude

embedCode :: ByteString -> Code
embedCode = Code . decodeUtf8

complex :: Code
complex = embedCode $(embedFile "./src/GLSL/complex.glsl")

smoothHsv :: Code
smoothHsv = embedCode $(embedFile "./src/GLSL/smooth_hsv.glsl")

turboColormap :: Code
turboColormap = embedCode $(embedFile "./src/GLSL/turbo_colormap.glsl")

palettes :: Code
palettes = embedCode $(embedFile "./src/GLSL/palettes.glsl")

hgSdf :: Code
hgSdf = embedCode $(embedFile "./src/GLSL/hg_sdf.glsl")

distfunctions2d :: Code
distfunctions2d = embedCode $(embedFile "./src/GLSL/distfunctions2d.glsl")

camera :: Code
camera = embedCode $(embedFile "./src/GLSL/camera.glsl")
