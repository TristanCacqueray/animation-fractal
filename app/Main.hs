module Main where

import Prelude
import AnimationFractal (main)

main :: IO ()
main = AnimationFractal.main
