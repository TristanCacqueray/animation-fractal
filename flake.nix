{
  inputs = {
    hspkgs.url =
      "github:podenv/hspkgs/4750e01093a76c15eef7aa43bab8cd6e285c3fac";
    simple-dsp.url =
      "github:TristanCacqueray/simple-dsp/c60505dca61bf8ad0df6ab56fb3d16e87a59b10b";
    keid-vkguide.url =
      "gitlab:TristanCacqueray/keid-vkguide/bb13d8ef2e7dd3bd8db158ca38cba6e0d7faf316";
  };
  outputs = { self, simple-dsp, hspkgs, keid-vkguide }:
    let
      pkgs = hspkgs.pkgs;
      gfx-pkgs = with pkgs; [
        vulkan-headers
        vulkan-tools
        vulkan-loader
        vulkan-validation-layers
        glslang

        xorg.xrandr
        xorg.libXcursor
        xorg.libXxf86vm
        xorg.libXinerama
        xorg.libXrandr
        xorg.libXi
        xorg.libXext
      ];

      haskellExtend = hpFinal: hpPrev: {
        animation-fractal = hpPrev.callCabal2nix "animation-fractal" self { };
        shelly = pkgs.haskell.lib.doJailbreak hpPrev.shelly;
        simple-dsp = let pkg = hpPrev.callCabal2nix "simple-dsp" simple-dsp { };
        in pkgs.haskell.lib.dontCheck (pkgs.haskell.lib.setBuildTargets
          (pkgs.haskell.lib.overrideCabal pkg {
            isExecutable = false;
            libraryHaskellDepends =
              [ hpPrev.bindings-GLFW hpPrev.GLFW-b hpPrev.vulkan ];
          }) [ "lib:simple-dsp" ]);
        ffmpeg-light = pkgs.haskell.lib.doJailbreak
          (pkgs.haskell.lib.overrideCabal hpPrev.ffmpeg-light {
            broken = false;
            patches = [
              (pkgs.fetchpatch {
                url =
                  "https://github.com/TristanCacqueray/ffmpeg-light/commit/6c54372922ea2e225c6ba216a9e1db4bb84fdde2.patch";
                sha256 = "sha256-mmnQ9YEStIQ6sfJGaznbIqx7o7B+Z9q6ZqpQftNpafE=";
              })
            ];
          });
        # failure reported: https://github.com/lehins/pvar/issues/4
        pvar = pkgs.haskell.lib.dontCheck hpPrev.pvar;
      };
      hsPkgs =
        (pkgs.hspkgs.extend haskellExtend).extend keid-vkguide.haskellExtend;

      # ghc = hsPkgs.ghcWithPackages (p: [ p.vulkan ]);
      ghc = hsPkgs.shellFor {
        packages = p: [ p.animation-fractal ];
        buildInputs = with pkgs;
          [
            hpack
            cabal-install
            ghcid
            haskell-language-server
            pkg-config
            xorg.libXdmcp
            ffmpeg
            nixVulkanIntel
          ] ++ gfx-pkgs;
      };

    in { devShell.x86_64-linux = ghc; };
}
