# Animation Fractal

> Note that this is a work in progress, please get in touch if you are interested.

Introduction blog post and demo: https://tristancacqueray.github.io/blog/introducing-animation-fractal

## Overview and scope

The primary goal of this project is to produce visualisations akin to [demoscene][demoscene].
The secondary goal is to demonstrate that Haskell is great at computer graphics.

The user provides:

- A shadertoy (fullscreen fragment), e.g. an [IFS][ifs] or [SDF ray marching][sdf-ray-marching].
- Uniform parameters, e.g. complex seeds, zoom, orbits location.
- Input modulations, to animate the parameters.

The project enables:

- Running the animation in real time, with dear-imgui controller to adjust the parameters.
- Render pre-recorded inputs into frame perfect high quality video.

```mermaid
graph LR
  subgraph "Main Engine"
    INP(Collect Input) --> MOD(Modulation Matrix)
    UI(Controller) --> MOD
    MOD --> UBO(Update scene)
    UBO --> DRAW(Draw)
  end
  subgraph "Animation"
    Scene(Play input) --> MOD
    DRAW --> Record
  end
```


## Features

The current implemention is a work in progress, here is a tentative roadmap:

- Engine:
  - [x] Render shader offscreen and present texture fullscreen.
  - [ ] Render "map" shader to adjust julia seed location.
  - [x] Hot reload the SPIRV code on change.
  - [x] Take screenshot.
  - [x] Render final video.
- Shader:
  - [x] Normalized UV for IFS, based on window resolution and aspect ratio.
  - [ ] Generate glsl code using external process (see hy2glsl).
  - [ ] Support FIR code.
- User interface:
  - [x] IFS center on click and zoom on scroll.
  - [x] Add params save and restore.
  - [ ] Ray march camera position.
  - [x] Fine grain sliders to adjust the parameters.
- Inputs:
  - [x] Simple trigger.
  - [x] Midi events.
  - [ ] Audio frequencies.
- Modulations:
  - [x] Easing function, e.g. to handle smooth input jumps.
  - [x] Second order dynamics, based on [Giving Personality to Procedural Animations using Math](https://www.youtube.com/watch?v=KPoeNZZ6H4s).



## History

Previous implementation are available:

- [demo-code][demo-code]: using glumpy and tkinter. Example demo: [underwater-fractal-creature][underwater-fractal-creature] produces [ufc-video][ufc-video].
- [hy2glsl][hy2glsl]: poc to generate shader code using lisp. Example demo: [livet][livet] produces [livet-video][livet-video].
- [fir-toy-examples][fir-toy-examples]: generate shader code using an EDSL. Explanation video: [FIR animation][fir-video].

The python implementations are fast to reload, but hard to compose, and processing audio inputs in realtime often cause segfaults.
The fir examples are ideal because the dear-imgui controllers are defined in the shader code, but the display engine is too low-level.
This new project is based on the [keid][keid] engine.

## Usage

Run a demo: `cabal run animation-fractal -- --help`

Watch shader code: `ghcid -W --test 'AnimationFractal.compile "demo-name"'`

Grab vulkan wrapper: `NIXPKGS_ALLOW_UNFREE=1 nix shell --impure github:nix-community/nixGL#nixVulkanNvidia`

[demoscene]: https://en.wikipedia.org/wiki/Demoscene
[ifs]: https://en.wikipedia.org/wiki/Iterated_function_system
[sdf-ray-marching]: https://en.wikipedia.org/wiki/Ray_marching
[demo-code]: https://github.com/TristanCacqueray/demo-code
[underwater-fractal-creature]: https://github.com/TristanCacqueray/demo-render/blob/master/animations/underwater-fractal-creature.py
[ufc-video]: https://www.youtube.com/watch?v=jCR_ZARjwuk
[hy2glsl]: https://github.com/TristanCacqueray/hy2glsl
[livet]: https://github.com/TristanCacqueray/demo-render/blob/master/animations/livet-nediser.hy
[livet-video]: https://www.youtube.com/watch?v=HUxUw4589lk
[fir-toy-examples]: https://gitlab.com/sheaf/fir/-/tree/master/fir-examples#shader-toy
[fir-video]: https://www.youtube.com/watch?v=Mvbu_PXJOIc&t=272
[keid]: https://keid.haskell-game.dev/
