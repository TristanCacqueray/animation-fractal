#ifndef ANIMATION_FRACTAL
// Simulate the pitch inputs using a smoothed stairs function STEP
#define STEP1(x) ((x) - sin(x))
#define STEP(x, offset, amp) (STEP1(STEP1(offset + x * amp)) * .15)
#define is (iTime)

float pitches[9];
void genPitches() {
  pitches[0] = STEP(is, 1., 1.);
  pitches[1] = STEP(is, 2., 2.);
  pitches[2] = STEP(is, 3., 3.);
  pitches[3] = STEP(is, 4., 4.);
  pitches[4] = STEP(is, 5., 1.);
  pitches[5] = STEP(is, 6., 2.);
  pitches[6] = STEP(is, 7., 3.);
  pitches[7] = STEP(is, 8., 4.);
  pitches[8] = STEP(is, 1., 5.);
}
#define ipitch_1 pitches[0]
#define ipitch_2 pitches[1]
#define ipitch_3 pitches[2]
#define ipitch_4 pitches[3]
#define ipitch_5 pitches[4]
#define ipitch_6 pitches[5]
#define ipitch_7 pitches[6]
#define ipitch_8 pitches[7]
#define ipitch_9 pitches[8]

#define icolor (iTime*.5)
#define irot (iTime)
#endif
#define irot (imoveFWD + iTime * .05)
#define PI 3.141592
#define orbs 20.

vec2 kale(vec2 uv, vec2 offset, float sides) {
  float angle = atan(uv.y, uv.x);
  angle = ((angle / PI) + 1.0) * 0.5;
  angle = mod(angle, 1.0 / sides) * sides;
  angle = -abs(2.0 * angle - 1.0) + 1.0;
  angle = angle;
  float y = length(uv);
  angle = angle * (y);
  return vec2(angle, y) - offset;
}

vec4 orb(vec2 uv, float size, vec2 position, vec3 color, float contrast) {
  return pow(vec4(size / length(uv + position) * color, 1.), vec4(contrast));
}

mat2 rotate(float angle) {
  return mat2(cos(angle), -sin(angle), sin(angle), cos(angle));
}

// The MIT License
// See https://iquilezles.org/articles/palettes for more information
vec3 pal( in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d )
{
    return a + b*cos( 6.28318*(c*t+d) );
}
vec3 palette ( float t ) {
 // return pal( t, vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(1.0,1.0,0.5),vec3(0.8,0.90,0.30) );
 return pal( t, vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(1.0,1.0,1.0),vec3(0.0,0.10,0.20) );
 // return pal( t, vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(2.0,1.0,0.0),vec3(0.5,0.20,0.25) );
}

float modMirror1(inout float p, float size) {
  float halfsize = size*0.5;
  float c = floor((p + halfsize)/size);
  p = mod(p + halfsize,size) - halfsize;
  p *= mod(c, 2.0)*2.0 - 1.0;
  return c;
}

#ifdef ANIMATION_FRACTAL
void main() {
    vec2 uv = -1. + 2*inUV;
    uv *= 42.;
    uv.y *= scene.screenRatio;
#else
void mainImage( out vec4 oColor, in vec2 fragCoord ) {
    vec2 uv = 23.09 * (2. * fragCoord - iResolution.xy) / iResolution.y;
    genPitches();
#endif
  float dist = length(uv);
  oColor = vec4(0.);
  uv *= rotate(irot);
  uv = kale(uv, vec2(6.97), 8.);
  uv *= rotate(irot);
  float id = modMirror1(uv.x, 12. + 8*sin(ipitch_2));
  for (float i = 0.; i < orbs; i++) {
    if (i > 10.)
    uv *= rotate((ipitch_7*.02 + ipitch_2*.005));
    else
    uv *= rotate(ipitch_1*.02);
    uv.x += 0.57 * sin(0.3 * uv.y + ipitch_3);
    uv.y -= 0.63 * cos(0.53 * uv.x + ipitch_4);
    float t = i * PI / orbs * 2.;
    float x = 4.02 * tan(t - ipitch_5 * -.1);
    float y = 4.02 * cos(t - ipitch_6 * .5);
    vec2 position = vec2(x, y);
    position *= rotate(ipitch_8*.1);
    vec3 color = .15 + palette(icolor + i / orbs) * .25; // vec3(0.,0.3,0.8) * 0.25 + 0.25;
    oColor += orb(uv, 1.39, position, color, 1.37);

    // uv.y *= .8 + .3 * sin(imoveFWD*2.);
  }
#ifdef ANIMATION_FRACTAL
    oColor = pow(oColor, vec4(1.2, .9, .9, 1.) * 3.);
#endif
}
