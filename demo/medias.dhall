let AF = ../package.dhall

let Prelude =
      https://prelude.dhall-lang.org/v17.0.0/package.dhall
        sha256:10db3c919c25e9046833df897a8ffe2701dc390fa0893d958c3430524be5a43e

let TrackDef =
      { Type =
          { name : Text
          , track : Natural
          , base : Natural
          , count : Natural
          , kind : AF.ModulationKind
          , cap : Double
          , pulse : Bool
          }
      , default = { base = 1, count = 1, cap = 1.0, pulse = False }
      }

let mkTrack =
      \(pulse : Bool) ->
      \(name : Text) ->
      \(track : Natural) ->
      \(base : Natural) ->
      \(count : Natural) ->
      \(kind : AF.ModulationKind) ->
        TrackDef::{ name, track, base, count, kind, pulse }

let midiMod =
      \(kind : AF.ModulationKind) ->
      \(name : Text) ->
      \(pitch : Natural) ->
        AF.Modulation::{ var = "${name}:${Natural/show pitch}", kind }

let midiMods =
      \(track : TrackDef.Type) ->
        Prelude.List.generate
          track.count
          AF.Modulation.Type
          ( \(pitch : Natural) ->
              midiMod track.kind track.name (pitch + track.base)
          )

let midiInput =
      \(track : TrackDef.Type) ->
        AF.Input.Midi
          { name = track.name
          , mods = midiMods track
          , cap = track.cap
          , pulse = track.pulse
          }

let midiTrackInputs = Prelude.List.map TrackDef.Type AF.Input midiInput

let midiVar =
      \(name : Text) ->
      \(track : Natural) ->
      \(pitch : Natural) ->
        AF.Variable::{
        , name = "${name}:${Natural/show pitch}"
        , uniformName = "n${Natural/show track}_${Natural/show pitch}"
        , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
        }

let midiVars =
      \(track : TrackDef.Type) ->
        Prelude.List.generate
          track.count
          AF.Variable.Type
          ( \(pitch : Natural) ->
              midiVar track.name track.track (pitch + track.base)
          )

let midiTrackVars =
      Prelude.List.concatMap TrackDef.Type AF.Variable.Type midiVars

let AudioModDef =
      { Type = { mod : AF.Modulation.Type, gate : Double, uniformName : Text }
      , default.gate = 0.0
      }

let AudioModFilterDef =
      { Type = { filter : AF.Filter.Type, uniformName : Text }, default = {=} }

let AudioDef =
      { Type =
          { name : Text
          , dry : AudioModDef.Type
          , filters : List AudioModFilterDef.Type
          }
      , default.filters = [] : List AudioModFilterDef.Type
      }

let audioVars =
      \(audio : AudioDef.Type) ->
          [ AF.Variable::{
            , name = audio.dry.mod.var
            , uniformName = audio.dry.uniformName
            , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
            }
          ]
        # Prelude.List.map
            AudioModFilterDef.Type
            AF.Variable.Type
            ( \(amf : AudioModFilterDef.Type) ->
                AF.Variable::{
                , name = amf.filter.mod.var
                , uniformName = amf.uniformName
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
            )
            audio.filters

let MediaKind = < Midi : List TrackDef.Type | Audio : AudioDef.Type >

let mkAudioF =
      \(dir : Text) ->
      \(file : Text) ->
      \(dry : AudioModDef.Type) ->
      \(filters : List AudioModFilterDef.Type) ->
        { path = "${dir}/${file}.mp3"
        , kind = MediaKind.Audio AudioDef::{ name = file, dry, filters }
        }

let mkAudio =
      \(dir : Text) ->
      \(file : Text) ->
      \(dry : AudioModDef.Type) ->
        mkAudioF dir file dry ([] : List AudioModFilterDef.Type)

let MediaDef = { path : Text, kind : MediaKind }

let sceneMedias
    : List MediaDef -> AF.Scene.Type -> AF.Scene.Type
    = \(medias : List MediaDef) ->
      \(scene : AF.Scene.Type) ->
            scene
        //  { medias =
                  scene.medias
                # Prelude.List.map
                    MediaDef
                    Text
                    (\(md : MediaDef) -> md.path)
                    medias
            , vars =
                  scene.vars
                # Prelude.List.concatMap
                    MediaDef
                    AF.Variable.Type
                    ( \(md : MediaDef) ->
                        merge
                          { Midi =
                              \(tracks : List TrackDef.Type) ->
                                midiTrackVars tracks
                          , Audio = \(audio : AudioDef.Type) -> audioVars audio
                          }
                          md.kind
                    )
                    medias
            , inputs =
                  scene.inputs
                # Prelude.List.concatMap
                    MediaDef
                    AF.Input
                    ( \(md : MediaDef) ->
                        merge
                          { Midi =
                              \(tracks : List TrackDef.Type) ->
                                midiTrackInputs tracks
                          , Audio =
                              \(audio : AudioDef.Type) ->
                                [ AF.Input.AudioFile
                                    { name = audio.name
                                    , mod = AF.AudioMod::{
                                      , dry = audio.dry.mod
                                      , gate = audio.dry.gate
                                      }
                                    }
                                ]
                          }
                          md.kind
                    )
                    medias
            }

in  { TrackDef
    , sceneMedias
    , MediaDef
    , Midi = MediaKind.Midi
    , mkAudio
    , AudioDef
    , AudioModDef
    , mkTrack
    }
