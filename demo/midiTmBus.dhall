let AF = ../package.dhall

let Medias = ./medias.dhall

let mkTrack = Medias.mkTrack False

let mkIncr = \(speed : Double) -> AF.ModulationKind.Incr { speed }

let tracks =
      [ mkTrack "BASS" 1 1 2 (mkIncr 500.0)
      , mkTrack "MELO" 1 5 2 (mkIncr 500.0)
      , mkTrack "BETEL" 1 9 1 (mkIncr 500.0)
      , mkTrack "KICK" 4 1 1 (mkIncr 300.0)
      , mkTrack "SNARE" 4 2 1 (mkIncr 300.0)
      , mkTrack "TOM" 4 3 1 (mkIncr 450.0)
      , mkTrack "HAT" 4 4 1 (mkIncr 200.0)
      ]

let medias =
      \(dir : Text) ->
      \(name : Text) ->
        [ Medias.mkAudio
            dir
            "${name}-left"
            Medias.AudioModDef::{
            , mod = AF.Modulation::{
              , var = "imoveFWD"
              , kind = AF.ModulationKind.Incr { speed = 900.0 }
              }
            , uniformName = "var2"
            }
        , Medias.mkAudio
            dir
            "${name}-right"
            Medias.AudioModDef::{
            , mod = AF.Modulation::{
              , var = "icolor"
              , kind = AF.ModulationKind.Incr { speed = 900.0 }
              }
            , uniformName = "var3"
            }
        , { path = "${dir}/Media/${name}.mid", kind = Medias.Midi tracks }
        ]

let mkScene =
      \(dir : Text) ->
      \(name : Text) ->
      \(midiTimeScale : Double) ->
        Medias.sceneMedias
          (medias dir name)
          AF.Scene::{
          , name
          , midiTimeScale
          , author = "tubeman"
          , url = "https://www.shadertoy.com/view/XfV3Dy"
          , license = "CC-BY-NC-SA-3.0"
          , shaderPath = "./demo/midiTmBus.glsl"
          , medias = [ "${dir}/${name}.mp3" ]
          , vars =
            [ AF.Variable::{
              , name = "iTime"
              , uniformName = "var4"
              , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
              }
            ]
          }

in  mkScene "/home/fedora/reaper/microOST" "RA-HellMarch" 0.9755757575757577
