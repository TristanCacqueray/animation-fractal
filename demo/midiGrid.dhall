let AF = ../package.dhall

let Medias = ./medias.dhall

let TrackDef = Medias.TrackDef

let mkADSR =
      \(attack : Natural) ->
      \(release : Natural) ->
        AF.ModulationKind.ADSR { attack, decay = 500, sustain = 0.9, release }

let mkIncr = \(speed : Double) -> AF.ModulationKind.Incr { speed }

let mkTrack = Medias.mkTrack True

let mkTrackP = Medias.mkTrack False

let tracks =
      [ mkTrack "BASS" 1 1 10 (mkADSR 100 3000)
      , mkTrack "DOUX" 1 11 6 (mkADSR 100 5000)
      , mkTrack "KEYS" 2 1 10 (mkADSR 100 3000)
      , mkTrack "FAR" 2 11 6 (mkADSR 300 3000)
      , mkTrack "KEYS2" 3 1 10 (mkADSR 100 3000)
      , mkTrack "NAUTICA" 3 11 6 (mkADSR 100 3000)
      , mkTrack "MONKEY ISLAND" 4 9 8 (mkADSR 100 3000)
      , mkTrack "KICK" 4 1 1 (mkADSR 100 2000)
      , mkTrack "CONGA" 4 2 1 (mkADSR 150 2000)
      , mkTrack "TOM1" 4 3 1 (mkADSR 150 2000)
      , mkTrack "TOM2" 4 4 1 (mkADSR 150 2000)
      , mkTrack "HAT" 4 6 1 (mkADSR 10 1000)
      ]

let medias =
      \(dir : Text) ->
      \(name : Text) ->
        [ Medias.mkAudio
            dir
            "${name}-left"
            Medias.AudioModDef::{
            , mod = AF.Modulation::{ var = "imoveFWD" }
            , uniformName = "var2"
            }
        , Medias.mkAudio
            dir
            "${name}-right"
            Medias.AudioModDef::{
            , mod = AF.Modulation::{
              , var = "icolor"
              , kind = AF.ModulationKind.Incr { speed = 900.0 }
              }
            , uniformName = "var3"
            }
        , Medias.mkAudio
            dir
            "${name}-doux"
            Medias.AudioModDef::{
            , mod = AF.Modulation::{
              , var = "idoux"
              , kind = AF.ModulationKind.Incr { speed = 2000.0 }
              }
            , uniformName = "var4"
            }
        , { path = "${dir}/Media/${name}.mid", kind = Medias.Midi tracks }
        ]

let mkScene =
      \(dir : Text) ->
      \(name : Text) ->
      \(midiTimeScale : Double) ->
        Medias.sceneMedias
          (medias dir name)
          AF.Scene::{
          , name
          , midiTimeScale
          , author = "Shane"
          , url = "https://www.shadertoy.com/view/lfKXzh"
          , license = "CC-BY-NC-SA-3.0"
          , shaderPath = "./demo/midiGrid.glsl"
          , medias = [ "${dir}/${name}.mp3" ]
          , vars =
            [ AF.Variable::{
              , name = "iTime"
              , uniformName = "var1"
              , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
              }
            ]
          }

in  mkScene "/home/fedora/reaper/microIsland" "MI-intro" 1.0
