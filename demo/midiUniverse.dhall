let AF = ../package.dhall

let Prelude =
      https://prelude.dhall-lang.org/v17.0.0/package.dhall
        sha256:10db3c919c25e9046833df897a8ffe2701dc390fa0893d958c3430524be5a43e

let mkMidiName =
      \(track : Natural) ->
      \(pitch : Natural) ->
        "i${Natural/show track}pitch_${Natural/show (pitch + 1)}"

let mkPulseName =
      \(pitch : Natural) -> "ipitch_pulse_${Natural/show (pitch + 1)}"

let mkMidiMods =
      \(speed : Double) ->
      \(track : Natural) ->
      \(count : Natural) ->
      \(base_pitch : Natural) ->
        Prelude.List.generate
          count
          AF.Modulation.Type
          ( \(pitch : Natural) ->
              AF.Modulation::{
              , var = mkMidiName track (base_pitch + pitch)
              , speed
              }
          )

let mkMidiModsD = mkMidiMods 600.0

let mkMidiPulse =
      \(count : Natural) ->
      \(base_pitch : Natural) ->
        Prelude.List.generate
          count
          AF.Modulation.Type
          ( \(pitch : Natural) ->
              AF.Modulation::{ var = mkPulseName pitch, prog = "sec-pulse" }
          )

let pitchVars =
      \(track : Natural) ->
      \(count : Natural) ->
        Prelude.List.generate
          count
          AF.Variable.Type
          ( \(pitch : Natural) ->
              AF.Variable::{
              , name = mkMidiName track pitch
              , uniformName =
                  "n${Natural/show track}_${Natural/show (pitch + 1)}"
              , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
              }
          )

let pulseVars =
      \(count : Natural) ->
        Prelude.List.generate
          count
          AF.Variable.Type
          ( \(pitch : Natural) ->
              AF.Variable::{
              , name = mkPulseName pitch
              , uniformName = "pulse_${Natural/show (pitch + 1)}"
              , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
              }
          )

let mkScene =
      \(dir : Text) ->
      \(name : Text) ->
        AF.Scene::{
        , name
        , author = "BigWings"
        , url = "https://www.shadertoy.com/view/XfVGzG"
        , license = "CC-BY-NC-SA-3.0"
        , shaderPath = "./demo/midiUniverse.glsl"
        , medias =
          [ "/home/fedora/reaper/${dir}/${name}.mp3"
          , "/home/fedora/reaper/${dir}/Media/${name}.mid"
          , "/home/fedora/reaper/${dir}/${name}-left.mp3"
          , "/home/fedora/reaper/${dir}/${name}-right.mp3"
          ]
        , vars =
              [ AF.Variable::{
                , name = "imoveFWD"
                , uniformName = "var2"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              , AF.Variable::{
                , name = "icolor"
                , uniformName = "var3"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              , AF.Variable::{
                , name = "iTime"
                , uniformName = "var4"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              ]
            # pitchVars 1 12
            # pitchVars 2 12
            # pulseVars 12
        , inputs =
          [ AF.Input.Midi
              { name = "MELO"
              , mods = mkMidiModsD 1 6 0
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.Midi
              { name = "ETHER"
              , mods = mkMidiModsD 1 6 6
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.Midi
              { name = "BASS"
              , mods = mkMidiMods 400.0 2 12 0
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.Midi
              { name = "PULSE"
              , mods = mkMidiPulse 12 0
              , cap = 0.8
              , pulse = True
              }
          , AF.Input.AudioFile
              { name = "${name}-left"
              , mod = AF.AudioMod::{
                , gate = 0.0
                , dry = AF.Modulation::{ var = "imoveFWD", speed = 1200.0 }
                }
              }
          , AF.Input.AudioFile
              { name = "${name}-right"
              , mod = AF.AudioMod::{
                , gate = 0.0
                , dry = AF.Modulation::{ var = "icolor" }
                }
              }
          ]
        }

in  mkScene "microDebussy" "Petite Suite, pour piano a 4 mains"
