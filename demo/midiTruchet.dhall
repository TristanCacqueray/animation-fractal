let AF = ../package.dhall

let Prelude =
      https://prelude.dhall-lang.org/v17.0.0/package.dhall
        sha256:10db3c919c25e9046833df897a8ffe2701dc390fa0893d958c3430524be5a43e

let mkMidiName =
      \(track : Natural) ->
      \(pitch : Natural) ->
        "n${Natural/show track}_${Natural/show (pitch + 1)}"

let mkMidiMods =
      \(speed : Double) ->
      \(count : Natural) ->
      \(base_pitch : Natural) ->
        Prelude.List.generate
          count
          AF.Modulation.Type
          ( \(pitch : Natural) ->
              AF.Modulation::{ speed, var = mkMidiName 1 (base_pitch + pitch) }
          )

let mkMidiVars =
      \(track : Natural) ->
        Prelude.List.generate
          14
          AF.Variable.Type
          ( \(pitch : Natural) ->
              AF.Variable::{
              , name = mkMidiName track pitch
              , uniformName = mkMidiName track pitch
              , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
              }
          )

let mkScene =
      \(name : Text) ->
        AF.Scene::{
        , name
        , author = "mrange"
        , license = "CC0"
        , url = "https://www.shadertoy.com/view/lcGGDW"
        , shaderPath = "./demo/midiTruchet.glsl"
        , medias =
          [ "/home/fedora/reaper/microChopin/${name}.mp3"
          , "/home/fedora/reaper/microChopin/Media/${name}.mid"
          , "/home/fedora/reaper/microChopin/${name}-left.mp3"
          , "/home/fedora/reaper/microChopin/${name}-right.mp3"
          ]
        , vars =
              [ AF.Variable::{
                , name = "moveFWD"
                , uniformName = "var2"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              , AF.Variable::{
                , name = "icolor"
                , uniformName = "var3"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              ]
            # mkMidiVars 1
        , inputs =
          [ AF.Input.Midi
              { name = "BASS"
              , mods = mkMidiMods 600.0 5 0
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.Midi
              { name = "MELO"
              , mods = mkMidiMods 550.0 5 5
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.AudioFile
              { name = "${name}-left"
              , mod = AF.AudioMod::{
                , gate = 0.0
                , dry = AF.Modulation::{ var = "moveFWD", speed = 1200.0 }
                }
              }
          , AF.Input.AudioFile
              { name = "${name}-right"
              , mod = AF.AudioMod::{
                , gate = 0.0
                , dry = AF.Modulation::{ var = "icolor" }
                }
              }
          ]
        }

let op57 = mkScene "Berceuse Op57"

let op23 = mkScene "Ballade No. 1 in g minor, Op. 23"

let op30 = mkScene "MazurkaOp30"

in  mkScene "Op67"
