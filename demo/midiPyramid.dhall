let AF = ../package.dhall

let Prelude =
      https://prelude.dhall-lang.org/v17.0.0/package.dhall
        sha256:10db3c919c25e9046833df897a8ffe2701dc390fa0893d958c3430524be5a43e

let mkMidiName =
      \(track : Natural) ->
      \(pitch : Natural) ->
        "t${Natural/show track}p${Natural/show (pitch + 1)}"

let mkMidiMods =
      \(track : Natural) ->
      \(speed : Double) ->
      \(count : Natural) ->
      \(base_pitch : Natural) ->
        Prelude.List.generate
          count
          AF.Modulation.Type
          ( \(pitch : Natural) ->
              AF.Modulation::{
              , var = mkMidiName track (base_pitch + pitch)
              , speed
              }
          )

let mkMidiModsD = mkMidiMods 1 800.0

let pitchVars =
      \(track : Natural) ->
      \(count : Natural) ->
        Prelude.List.generate
          count
          AF.Variable.Type
          ( \(pitch : Natural) ->
              AF.Variable::{
              , name = mkMidiName track pitch
              , uniformName =
                  "n${Natural/show track}_${Natural/show (pitch + 1)}"
              , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
              }
          )

let mkScene =
      \(dir : Text) ->
      \(name : Text) ->
      \(midiTimeScale : Double) ->
        AF.Scene::{
        , name
        , midiTimeScale
        , author = "bradjamesgrant"
        , url = "https://www.shadertoy.com/view/4cKGDG"
        , license = "CC-BY-NC-SA-3.0"
        , shaderPath = "./demo/midiPyramid.glsl"
        , medias =
          [ "/home/fedora/reaper/${dir}/${name}.mp3"
          , "/home/fedora/reaper/${dir}/Media/${name}.mid"
          , "/home/fedora/reaper/${dir}/${name}-left.mp3"
          , "/home/fedora/reaper/${dir}/${name}-right.mp3"
          ]
        , vars =
              [ AF.Variable::{
                , name = "imoveFWD"
                , uniformName = "var2"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              , AF.Variable::{
                , name = "icolor"
                , uniformName = "var3"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              , AF.Variable::{
                , name = "iTime"
                , uniformName = "var4"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              ]
            # pitchVars 1 15
            # pitchVars 2 5
        , inputs =
          [ AF.Input.Midi
              { name = "HarpChord"
              , mods = mkMidiModsD 5 0
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.Midi
              { name = "Bass"
              , mods = mkMidiModsD 5 5
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.Midi
              { name = "Beat"
              , mods = mkMidiMods 1 200.0 5 10
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.Midi
              { name = "Strings"
              , mods = mkMidiMods 2 800.0 5 0
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.Midi
              { name = "Guitar"
              , mods = mkMidiMods 2 800.0 5 0
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.AudioFile
              { name = "${name}-left"
              , mod = AF.AudioMod::{
                , gate = 0.0
                , dry = AF.Modulation::{ var = "imoveFWD", speed = 1200.0 }
                }
              }
          , AF.Input.AudioFile
              { name = "${name}-right"
              , mod = AF.AudioMod::{
                , gate = 0.0
                , dry = AF.Modulation::{ var = "icolor" }
                }
              }
          ]
        }

in  mkScene "microRap" "SlimShady" 1.1428333333333331
