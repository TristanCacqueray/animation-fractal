let AF = ../package.dhall

let Prelude =
      https://prelude.dhall-lang.org/v17.0.0/package.dhall
        sha256:10db3c919c25e9046833df897a8ffe2701dc390fa0893d958c3430524be5a43e

let mkMidiName = \(pitch : Natural) -> "ipitch_${Natural/show (pitch + 1)}"

let mkMidiMods =
      \(speed : Double) ->
      \(count : Natural) ->
      \(base_pitch : Natural) ->
        Prelude.List.generate
          count
          AF.Modulation.Type
          ( \(pitch : Natural) ->
              AF.Modulation::{ var = mkMidiName (base_pitch + pitch), speed }
          )

let mkMidiModsD = mkMidiMods 700.0

let pitchVars =
      \(count : Natural) ->
        Prelude.List.generate
          count
          AF.Variable.Type
          ( \(pitch : Natural) ->
              AF.Variable::{
              , name = mkMidiName pitch
              , uniformName = "n1_${Natural/show (pitch + 1)}"
              , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
              }
          )

let mkScene =
      \(dir : Text) ->
      \(name : Text) ->
      \(midiTimeScale : Double) ->
        AF.Scene::{
        , name
        , midiTimeScale
        , author = "Flopine"
        , url = "https://www.shadertoy.com/view/MfK3Wt"
        , license = "CC-BY-NC-SA-3.0"
        , shaderPath = "./demo/midiChanel.glsl"
        , medias =
          [ "/home/fedora/reaper/${dir}/${name}.mp3"
          , "/home/fedora/reaper/${dir}/Media/${name}.mid"
          , "/home/fedora/reaper/${dir}/${name}-left.mp3"
          , "/home/fedora/reaper/${dir}/${name}-right.mp3"
          ]
        , vars =
              [ AF.Variable::{
                , name = "imoveFWD"
                , uniformName = "var2"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              , AF.Variable::{
                , name = "icolor"
                , uniformName = "var3"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              , AF.Variable::{
                , name = "iTime"
                , uniformName = "var4"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              ]
            # pitchVars 10
        , inputs =
          [ AF.Input.Midi
              { name = "MELO"
              , mods = mkMidiModsD 5 0
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.Midi
              { name = "BASS"
              , mods = mkMidiModsD 3 5
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.Midi
              { name = "WARM"
              , mods = mkMidiModsD 1 8
              , cap = 0.8
              , pulse = False
              }
          , AF.Input.Midi
              { name = "LOW", mods = mkMidiModsD 1 9, cap = 0.8, pulse = False }
          , AF.Input.AudioFile
              { name = "${name}-left"
              , mod = AF.AudioMod::{
                , gate = 0.0
                , dry = AF.Modulation::{ var = "imoveFWD", speed = 700.0 }
                }
              }
          , AF.Input.AudioFile
              { name = "${name}-right"
              , mod = AF.AudioMod::{
                , gate = 0.0
                , dry = AF.Modulation::{ var = "icolor" }
                }
              }
          ]
        }

in  mkScene "microChopin" "MazurkaOp50" 1.0
