// Forked from https://www.shadertoy.com/view/ll3BDN
// Created by Flopine in 2018-10-15
// Default licence: CC-BY-NC-SA-3.0

// Code by Flopine

// Thanks to wsmind, leon, XT95, lsdlive, lamogui and Coyhot for teaching me
// Thanks LJ for giving me the love of shadercoding :3

// Cookie Collective rulz

// The shader has been adapted^Wbutchered to support new parameters
// to be controlled from midi/audio inputs with animation-fractal.
#ifndef ANIMATION_FRACTAL
#define STEP1(x) ((x) - sin(x))
#define STEP(x, offset, amp) (STEP1(STEP1(offset + x * amp)) * .15)
#define is (iTime)
float pitches[10];
void genPitches() {
  pitches[0] = STEP(is, 1., 1.);
  pitches[1] = STEP(is, 2., 3.);
  pitches[2] = STEP(is, 3., 2.);
  pitches[3] = STEP(is, 4., 4.);
  pitches[4] = STEP(is, 9., 1.);
  pitches[5] = STEP(is, 3., 2.);
  pitches[6] = STEP(is, 4., 2.);
  pitches[7] = STEP(is, 9., 2.);
  pitches[8] = STEP(is, 4., 2.);
  pitches[9] = STEP(is, 9., 2.);
}
#define ipitch_1 pitches[0]
#define ipitch_2 pitches[1]
#define ipitch_3 pitches[2]
#define ipitch_4 pitches[3]
#define ipitch_5 pitches[4]
#define ipitch_6 pitches[5]
#define ipitch_7 pitches[6]
#define ipitch_8 pitches[7]
#define ipitch_9 pitches[8]
#define ipitch_10 pitches[9]

#define icolor (iTime*.1)
#define imoveFWD (iTime*.1)
#endif

#define imove (im)

#define ITER 64.
#define PI 3.141592
#define time iTime
#define BPM 25./2.
#define tempo 1.

vec3 palette (float t, vec3 a, vec3 b, vec3 c, vec3 d)
{return a+b*cos(2.*PI*(c*t+d));}

float random (vec2 st)
{return fract(sin(dot(st.xy, vec2(12.2544, 35.1571)))*5418.548416);}

vec2 moda (vec2 p, float per)
{
    float a = atan(p.y, p.x);
    float l = length(p);
    a = mod(a-per/2., per)-per/2.;
    return vec2(cos(a),sin(a))*l;
}

vec2 mo(vec2 p, vec2 d)
{
    p = abs(p)-d;
    if (p.y > p.x) p.xy = p.yx;
    return p;
}

float stmin(float a, float b, float k, float n)
{
    float st = k/n;
    float u = b-k;
    return min(min(a,b), 0.5 * (u+a+abs(mod(u-a+st,2.*st)-st)));
}

float smin( float a, float b, float k )
{
    float h = max( k-abs(a-b), 0.0 );
    return min( a, b ) - h*h*0.25/k;
}

mat2 rot(float a)
{return mat2(cos(a),sin(a),-sin(a),cos(a));}

vec2 path (float t)
{
    float a = sin(t*0.2 + 1.5), b = sin(t*0.2);
    return vec2(a, a*b);
}

float sphe (vec3 p, float r)
{return length(p)-r;}

float od (vec3 p, float d)
{return dot(p, normalize(sign(p)))-d;}

float cyl (vec2 p, float r)
{return length(p)-r;}

float box( vec3 p, vec3 b )
{
    vec3 d = abs(p) - b;
    return min(max(d.x,max(d.y,d.z)),0.0) + length(max(d,0.0));
}

float sc (vec3 p, float d)
{
    p = abs(p);
    p = max(p.xyz, p.yzx);
    return min(p.x, min(p.y,p.z)) - d;
}

float prim1 (vec3 p)
{
    float c = cyl(p.xz, 0.1);
    float per = 2.;
    p.y += ipitch_5;
    p.y = mod (p.y-per/2., per)-per/2.;
    return smin(sphe(p, 0.3), c, 0.5);

}

float prim2 (vec3 p)
{
    float s = sphe(p,.9);
    float o = od(p,.9);
    p.xz *= rot(ipitch_4 + p.y*0.7);
    p.xz = moda(p.xz, 2.*PI/5.);
    p.x -= 1.;
    return smin(prim1(p), max(-o,s), 0.1);
}

float prim3 (vec3 p)
{
    p.xz *= rot(sin(ipitch_6*.5)*.7 + p.x*0.1);
    p.xy = mo(p.xy, vec2(2.));
    p.yz *= rot(sin(ipitch_10)*.3 + p.x*0.1);
    p.xz *= rot(ipitch_7 + p.y*0.7);
    p *= 1.2 + (.1+.1*sin(ipitch_6));
    return prim2(p);
}

float prim4 (vec3 p)
{
    p.yz *= rot(sin(ipitch_3*.5)*.7 + p.x*0.1);
    p.xy = moda(p.xy, 2.*PI/4.);
    p.x -= 4.;
    return prim3(p);
}

float prim5 (vec3 p)
{
    float per = 17.;
    p.xy *= rot(p.z*0.2);
    p.z += ipitch_8;
    p.z = mod(p.z-per/2., per) -per/2.;
    return prim4(p);
}

#define moveScale 20.
float primCenter (vec3 p)
{
    p.z -= imoveFWD*moveScale;
    p.xz *= rot(ipitch_1*tempo);
    p.xy *= rot(ipitch_2*tempo);
    p *= 1.4;
    return stmin(od(p, 1.), sphe(p,1.), 0.5, 6. + 2.*sin(ipitch_9));
}



float g = 0.;
float SDF(vec3 p)
{
  float d = prim5(p);
  d = smin(d, primCenter(p), 0.5);
  g+=0.1/(0.1+d*d);
  return d;
}

// The MIT License
// See https://iquilezles.org/articles/palettes for more information
vec3 pal( in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d )
{
    return a + b*cos( 6.28318*(c*t+d) );
}
vec3 palette ( float t ) {
 // return pal( t, vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(1.0,1.0,0.5),vec3(0.8,0.90,0.30) );
 return pal( t, vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(1.0,1.0,1.0),vec3(0.0,0.10,0.20) );
 // return pal( t, vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(2.0,1.0,0.0),vec3(0.5,0.20,0.25) );
}


#ifdef ANIMATION_FRACTAL
void main() {
    vec2 uv = -.5 + inUV;
    uv.y *= scene.screenRatio;
    uv *= 1.4;
#else
void mainImage( out vec4 oColor, in vec2 fragCoord ) {
    vec2 uv = fragCoord/iResolution.xy;
    uv -= 0.5;
    uv /= vec2(iResolution.y / iResolution.x, 1);
    genPitches();
#endif
    vec3 ro = vec3(0.0,0.0,-10. + imoveFWD*moveScale); vec3 p = ro;
    vec3 rd = normalize(vec3(uv,1.));

    float shad = 0.;
    float dither = random(uv);

    for (float i=0.; i<ITER; i++)
    {
        float d = SDF(p);
        if (d<0.001)
        {
            shad = i/ITER;
            break;
        }
        d *= 0.9 + dither*0.1;
        p += d*rd * .5;
    }

    float t = length(ro-p);

    vec3 pal = palette(icolor + length(uv));

    vec3 c = vec3(shad) * pal;
    c = mix(c, vec3(0.,0.,0.2), 1.-exp(-0.001 *t *t));
    c += g* 0.025 * (1.-length(uv));

#ifdef ANIMATION_FRACTAL
    c = pow(c, vec3(1.2, .9, .9) * 1.7);
#endif
    oColor = vec4(c,1.);
}
