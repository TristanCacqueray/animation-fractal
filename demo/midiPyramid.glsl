#define PI              3.141592654
#define TAU             (2.0*PI)
#define LESS(a,b,c)     mix(a,b,step(0.,c))
#define SABS(x,k)       LESS((.5/(k))*(x)*(x)+(k)*.5,abs(x),abs(x)-(k))
float hash(float co) {
  return fract(sin(co*12.9898) * 13758.5453);
}

vec2 toPolar(vec2 p) {
  return vec2(length(p), atan(p.y, p.x));
}

vec2 toRect(vec2 p) {
  return vec2(p.x*cos(p.y), p.x*sin(p.y));
}

float modMirror1(inout float p, float size) {
  float halfsize = size*0.5;
  float c = floor((p + halfsize)/size);
  p = mod(p + halfsize,size) - halfsize;
  p *= mod(c, 2.0)*2.0 - 1.0;
  return c;
}

float smoothKaleidoscope(inout vec2 p, float sm, float rep) {
  vec2 hp = p;

  vec2 hpp = toPolar(hp);
  float rn = modMirror1(hpp.y, TAU/rep);

  float sa = PI/rep - SABS(PI/rep - abs(hpp.y), sm);
  hpp.y = sign(hpp.y)*(sa);

  hp = toRect(hpp);

  p = hp;

  return rn;
}


// Forked from https://www.shadertoy.com/view/tsXBzS
// Created by Created by bradjamesgrant in 2020-05-03
// Default licence: CC-BY-NC-SA-3.0

// The shader has been adapted^Wbutchered to support new parameters
// to be controlled from midi/audio inputs with animation-fractal.

#ifndef ANIMATION_FRACTAL
#define STEP1(x) ((x) - sin(x))
#define STEP(x, offset, amp) (STEP1(STEP1(offset + x * amp)) * .15)
#define is (iTime * .5)
#define t1p1 (STEP(is, 1., 1.))
#define t1p2 (STEP(is, 2., 3.))
#define t1p3 (STEP(is, 3., 2.))
#define t1p4 (STEP(is, 4., 4.))
#define t1p5 (STEP(is, 9., 1.))
#define t1p6 (STEP(is, 3., 2.))
#define t1p7 (STEP(is, 4., 2.))
#define t1p8 (STEP(is, 9., 2.))
#define t1p9 (STEP(is, 4., 2.))
#define t1p10 (STEP(is, 9., 2.))

#define icolor (iTime*.1)
#define imoveFWD (iTime*.1)
#endif

float pitches[15];

vec3 _palette(float d){
    return mix(vec3(0.2,0.7,0.9),vec3(1.,0.,1.),d);
}

// The MIT License
// See https://iquilezles.org/articles/palettes for more information
vec3 pal( in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d )
{
    return a + b*cos( 6.28318*(c*t+d) );
}
vec3 palette ( float t ) {
 return pal( t, vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(1.0,1.0,0.5),vec3(0.8,0.90,0.30) );
 // return pal( t, vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(1.0,1.0,1.0),vec3(0.0,0.10,0.20) );
 // return pal( t, vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(2.0,1.0,0.0),vec3(0.5,0.20,0.25) );
}


vec2 rotate(vec2 p,float a){
    float c = cos(a);
    float s = sin(a);
    return p*mat2(c,s,-s,c);
}

float map(vec3 p, int id){
    for( int i = 0; i<8; ++i){
        float t = iTime*0.2;

        float px = 0., py = 0., pz = 0.;
        float pf = 1.0;
        if (id == 10) {
          pf = .2;
        }
        px = pf * .5 * (pitches[(id + 1) % 15] + t2p1);
        py = pf * .5 * (pitches[(id + 2) % 15] + t2p2);
        pz = pf * .5 * (pitches[(id + 3) % 15] + t2p3);
        p.xz = rotate(p.xz, px);
        p.xy = rotate(p.xy, py);
        p.yz = rotate(p.yz, pz);
        p.xz = abs(p.xz);
        p.xz-=.4;
        p.yz-= .15+pf*.15*sin(pitches[(id + 4) % 15] + t2p4);
        p.xy-= .15+pf*.15*sin(pitches[(id + 5) % 15] + t2p5);
    }
    return dot(sign(p),p)/5.;
}
float smin(float a, float b) {
    float k = 12.;
    float res = exp(-k*a) + exp(-k*b);
    return -log(res) / k;
}
vec3 rm (vec3 ro, vec3 rd, float id){
    float t = 0.;
    vec3 col = vec3(0.);
    float d;
    for(float i =0.; i<64.; i++){
        vec3 p = ro + rd*t;
        float d0 = map(p + vec3(-2.5, -.5, 0.), 0)*.5;
        float d1 = map(p + vec3( 2.5, -.5, 0.), 5)*.5;
        float d2 = map(p + vec3( 0.0, 1., 0.), 10)*.5;
        d = min(min(d0, d1), d2);
        if(d<0.02){
            break;
        }
        if(d>100.){
            break;
        }
        // col+=vec3(0.6,0.8,0.8)/(400.*(d));
        col+=palette(icolor*2. + length(p)*.1)/(400.*(d));
        t+=d;
    }
    return col;
}

#ifdef ANIMATION_FRACTAL
void main() {
    vec2 uv = -.5 + inUV;
    uv.y *= scene.screenRatio;
#else
void mainImage( out vec4 oColor, in vec2 fragCoord ) {
    vec2 uv = (fragCoord-(iResolution.xy/2.))/iResolution.x;
#endif
    uv *= 2.;
    vec3 ro = vec3(0.,0.,-50.);
    uv.xy = rotate(uv.xy, imoveFWD);

    float t = iTime * .01;
    pitches[0] = t+t1p1;
    pitches[1] = t+t1p2;
    pitches[2] = t+t1p3;
    pitches[3] = t+t1p4;
    pitches[4] = t+t1p5;
    pitches[5] = t+t1p6;
    pitches[6] = t+t1p7;
    pitches[7] = t+t1p8;
    pitches[8] = t+t1p9;
    pitches[9] = t+t1p10;
    pitches[10] = t+t1p11;
    pitches[11] = t+t1p12;
    pitches[12] = t+t1p13;
    pitches[13] = t+t1p14;
    pitches[14] = t+t1p15;

    // uv.x += 0.5;
    // uv *= 1.5;
    // vec2 id2=pMod2(uv, vec2(1., 0.));
    // float id = pModPolar(uv, 8.); // pMod3(ro, vec3(2., 1., 1.));
    ro.xy = rotate(ro.xy,iTime*.1);
    vec3 cf = normalize(-ro);
    vec3 cs = normalize(cross(cf,vec3(0.,1.,0.)));
    vec3 cu = normalize(cross(cf,cs));

    vec3 uuv = ro+cf*3. + uv.x*cs + uv.y*cu;

    vec3 rd = normalize(uuv-ro);

    float id = 0.;
    vec3 col = rm(ro,rd, id);

    col = pow(col, vec3(1.2, .9, .9) * 2.);
    oColor = vec4(col, 1.0);
}
