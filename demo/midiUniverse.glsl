// Forked from https://www.shadertoy.com/view/lscczl
// The Universe Within, Created by BigWIngs in 2018-06-20
// SPDX-License-Identifier: CC-BY-NC-SA-3.0

// The shader has been adapted to support new parameters
// to be controlled from midi/audio inputs with animation-fractal.
// See the GetMod call site.

// Tweaks the inputs for shadertoy release
#ifndef ANIMATION_FRACTAL
// Simulate the pitch inputs using a smoothed stairs function STEP
#define STEP1(x) ((x) - sin(x))
#define STEP(x, offset, amp) (STEP1(STEP1(offset + x * amp)) * .15)
#define is (iTime)

#define ipitch_1 (STEP(is, 1., 1.))
#define ipitch_2 (STEP(is, 2., 1.))
#define ipitch_3 (STEP(is, 3., 1.))
#define ipitch_4 (STEP(is, 4., 2.))
#define ipitch_5 (STEP(is, 5., 2.))
#define ipitch_6 (STEP(is, 1., 2.))
#define ipitch_7 (STEP(is, 2., 3.))
#define ipitch_8 (STEP(is, 3., 3.))
#define ipitch_9 (STEP(is, 4., 3.))

#define icolor (iTime*.1)
#define imoveFWD (iTime)
#endif

float GetMod(vec2 uv, int planem2) {
  int xmod = int(mod(uv.y, 3.));
  int ymod = int(mod(uv.x, 4.));
  if (planem2 == 0) {
    if      (xmod == 0 && ymod == 0) return i1pitch_1;
    else if (xmod == 1 && ymod == 0) return i1pitch_4;
    else if (xmod == 2 && ymod == 0) return i1pitch_6;
    else if (xmod == 0 && ymod == 1) return i1pitch_9;
    else if (xmod == 1 && ymod == 1) return i1pitch_2;
    else if (xmod == 2 && ymod == 1) return i1pitch_5;
    else if (xmod == 0 && ymod == 2) return i1pitch_7;
    else if (xmod == 1 && ymod == 2) return i1pitch_8;
    else if (xmod == 2 && ymod == 2) return i1pitch_3;
    else if (xmod == 0 && ymod == 3) return i1pitch_10;
    else if (xmod == 1 && ymod == 3) return i1pitch_11;
    else if (xmod == 2 && ymod == 3) return i1pitch_12;
  } else {
    if      (xmod == 0 && ymod == 0) return i2pitch_1;
    else if (xmod == 1 && ymod == 0) return i2pitch_4;
    else if (xmod == 2 && ymod == 0) return i2pitch_6;
    else if (xmod == 0 && ymod == 1) return i2pitch_9;
    else if (xmod == 1 && ymod == 1) return i2pitch_2;
    else if (xmod == 2 && ymod == 1) return i2pitch_5;
    else if (xmod == 0 && ymod == 2) return i2pitch_7;
    else if (xmod == 1 && ymod == 2) return i2pitch_8;
    else if (xmod == 2 && ymod == 2) return i2pitch_3;
    else if (xmod == 0 && ymod == 3) return i2pitch_10;
    else if (xmod == 1 && ymod == 3) return i2pitch_11;
    else if (xmod == 2 && ymod == 3) return i2pitch_12;
  }
}

float GetPulse(vec2 uv, int planem2) {
  int xmod = int(mod(uv.x, 3.));
  int ymod = int(mod(uv.y, 2.));
  if (planem2 == 0) {
    if      (xmod == 0 && ymod == 0) return ipitch_pulse_1;
    else if (xmod == 1 && ymod == 0) return ipitch_pulse_2;
    else if (xmod == 2 && ymod == 0) return ipitch_pulse_3;
    else if (xmod == 0 && ymod == 1) return ipitch_pulse_4;
    else if (xmod == 1 && ymod == 1) return ipitch_pulse_5;
    else if (xmod == 2 && ymod == 1) return ipitch_pulse_6;
  } else {
    if      (xmod == 0 && ymod == 0) return ipitch_pulse_7;
    else if (xmod == 1 && ymod == 0) return ipitch_pulse_8;
    else if (xmod == 2 && ymod == 0) return ipitch_pulse_9;
    else if (xmod == 0 && ymod == 1) return ipitch_pulse_10;
    else if (xmod == 1 && ymod == 1) return ipitch_pulse_11;
    else if (xmod == 2 && ymod == 1) return ipitch_pulse_12;
  }
}

// The Universe Within - by Martijn Steinrucken aka BigWings 2018
// Email:countfrolic@gmail.com Twitter:@The_ArtOfCode
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

// After listening to an interview with Michael Pollan on the Joe Rogan
// podcast I got interested in mystic experiences that people seem to
// have when using certain psycoactive substances.
//
// For best results, watch fullscreen, with music, in a dark room.
//
// I had an unused 'blockchain effect' lying around and used it as
// a base for this effect. Uncomment the SIMPLE define to see where
// this came from.
//
// Use the mouse to get some 3d parallax.

// Music - Terrence McKenna Mashup - Jason Burruss Remixes
// https://soundcloud.com/jason-burruss-remixes/terrence-mckenna-mashup
//
// YouTube video of this effect:
// https://youtu.be/GAhu4ngQa48
//
// YouTube Tutorial for this effect:
// https://youtu.be/3CycKKJiwis


#define S(a, b, t) smoothstep(a, b, t)
#define NUM_LAYERS 4.

// #define SIMPLE


float N21(vec2 p) {
    vec3 a = fract(vec3(p.xyx) * vec3(213.897, 653.453, 253.098));
    a += dot(a, a.yzx + 82.76);
    return fract((a.x + a.y) * a.z);
}

vec2 GetPos(vec2 id, vec2 offs, float t) {
    float n = N21(id+offs);
    float n1 = fract(n*10.);
    float n2 = fract(n*100.);
    float a = t+n;
    return offs + vec2(sin(a*n1), cos(a*n2))*.4;
}

float GetT(vec2 ro, vec2 rd, vec2 p) {
    return dot(p-ro, rd);
}

float LineDist(vec3 a, vec3 b, vec3 p) {
    return length(cross(b-a, p-a))/length(p-a);
}

float df_line( in vec2 a, in vec2 b, in vec2 p)
{
    vec2 pa = p - a, ba = b - a;
    float h = clamp(dot(pa,ba) / dot(ba,ba), 0., 1.);
    return length(pa - ba * h);
}

float line(vec2 a, vec2 b, vec2 uv) {
    float r1 = .04;
    float r2 = .01;

    float d = df_line(a, b, uv);
    float d2 = length(a-b);
    float fade = S(1.4, .5, d2);

    fade += S(.05, .02, abs(d2-.75));
    return S(r1, r2, d)*fade;
}

float NetLayer(vec2 st, float n, float t, int planem2) {
    vec2 id = floor(st)+n;

    st = fract(st)-.5;

    vec2 p[9];
    float pulses[9];
    int i=0;
    for(float y=-1.; y<=1.; y++) {
        for(float x=-1.; x<=1.; x++) {
            vec2 offs = vec2(x, y);
            float pi = iTime * .3 + 4.2 + GetMod(id + offs, planem2) * 4.;
            pulses[i] = smoothstep(0., .8, GetPulse((id + offs), planem2));
            p[i++] = GetPos(id, offs, pi);
        }
    }

    float m = 0.;
    float sparkle = 0.;

    for(int i=0; i<9; i++) {
        m += line(p[4], p[i], st);
        float d = length(st-p[i]);

        float s = (.005/(d*d));
        s *= S(1., .7, d);
        float pulse = (min(pulses[i]*3.,1.0))*.4+.55;
        pulse = pow(pulse, 20.);

        s *= pulse;
        sparkle += s;
    }

    m += line(p[1], p[3], st);
    m += line(p[1], p[5], st);
    m += line(p[7], p[5], st);
    m += line(p[7], p[3], st);

    float sPhase = (sin(t+n)+sin(t*.1))*.25+.5;
    sPhase += pow(sin(t*.1)*.5+.5, 50.)*5.;
    m += sparkle*sPhase;//(*.5+.5);

    return m;
}

#ifdef ANIMATION_FRACTAL
void main() {
    vec2 uv = -1 + 2 * inUV;
    uv.y *= scene.screenRatio;
    // uv *= ;
    vec2 M = vec2(0.);
    uv.y *= -1;
    float glow = -.005; // 4.*(uv.y - .3)*iglow * (1 - abs(uv.x));
#else
void mainImage( out vec4 oColor, in vec2 fragCoord ) {
    vec2 uv = (fragCoord-iResolution.xy*.5)/iResolution.y;
    vec2 M = iMouse.xy/iResolution.xy-.5;
    float fft  = texelFetch( iChannel0, ivec2(uv.x,0), 0 ).x;
    float glow = -uv.y*fft*2.;
#endif
    float t = iTime * .01 + 42. + imoveFWD;

    float s = sin(t);
    float c = cos(t);
    mat2 rot = mat2(c, -s, s, c);
    vec2 st = uv*rot;
    M *= rot*2.;

    float m = 0.;
    int planem2 = 0;
    float pcolor = 3.;
    for(float i=0.; i<1.; i+=1./NUM_LAYERS) {
        float z = fract(t+i);
        float size = mix(15., 1., z);
        float fade = S(0., .6, z)*S(1., .8, z);
        planem2 = int(mod(planem2 + 1, 2));
        float pm = fade * NetLayer(st*size-M*z, i, imoveFWD, planem2);
        m += pm;
        pcolor += pm * (i*3.);
    }

    vec3 baseCol = vec3(s, cos(pcolor + icolor*3.9), -sin(pcolor + icolor*2.7))*.4+.6;
    vec3 col = baseCol*m;
    col += baseCol*glow;

    #ifdef SIMPLE
    uv *= 10.;
    col = vec3(1)*NetLayer(uv, 0., t);
    uv = fract(uv);
    // if(uv.x>.98 || uv.y>.98) col += 1.;
    #else
    col *= 1.-dot(uv*.95,uv*.95);
    // t = .2 + iTime; // mod(iTime, 230.);
    // col *= S(0., 20., t)*S(224., 200., t);
    #endif

    oColor = vec4(col,1);
}
