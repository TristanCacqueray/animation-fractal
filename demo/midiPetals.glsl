// Forked from https://www.shadertoy.com/view/Xs3yRM
// Created by lsdlive in 2018-02-08
// Default licence: CC-BY-NC-SA-3.0

// The shader has been adapted^Wbutchered to support new parameters
// to be controlled from midi/audio inputs with animation-fractal.

#ifndef ANIMATION_FRACTAL
// Simulate the pitch inputs using a smoothed stairs function STEP
#define STEP1(x) ((x) - sin(x))
#define STEP(x, offset, amp) (STEP1(STEP1(offset + x * amp)) * .15)
#define is (iTime)

#define ipitch_1 (STEP(is, 1., 1.))
#define ipitch_2 (STEP(is, 2., 2.))
#define ipitch_3 (STEP(is, 3., 3.))
#define ipitch_4 (STEP(is, 4., 4.))
#define ipitch_5 (STEP(is, 5., 1.))
#define ipitch_6 (STEP(is, 6., 2.))
#define ipitch_7 (STEP(is, 7., 3.))
#define ipitch_8 (STEP(is, 8., 4.))
#define ipitch_9 (STEP(is, 4., 3.))

#define icolor (iTime*.1)
#define imoveFWD (iTime*.1)
#endif

vec3 glow = vec3(0.);
float glow_intensity = .007;
vec3 glow_color = vec3(.5, .8, .5);

float smin(float a, float b) {
    float k = 10.;
    float res = exp(-k*a) + exp(-k*b);
    return -log(res) / k;
}

mat2 r2d(float a) {
    float c = cos(a), s = sin(a);
    return mat2(c, s, -s, c);
}

vec2 amod(vec2 p, float m) {
    float a = mod(atan(p.x, p.y) -m*.5, m) - m * .5;
    return vec2(cos(a), sin(a)) * length(p);
}

#define sph(p, r) (length(p) - r)
#define cyl sph
#define cube(p, b) length(max(abs(p) - vec3(b), 0.))
#define PI              3.141592654

float de(vec3 p) {
    float d = 1e9;
    p.xy *= r2d(p.z);
    p.xz *= r2d(3.14/2.);
    p.zy = amod(p.zy, .785); // 0.785 * (1 + .0 * sin(icolor)));
    // float id = amod(p.zy, 0.785 * (1 + .3 * sin(icolor)));
    // if (id == 0.) return d;

    p.y = abs(p.y) - .45 + .06 * sin(icolor);
    p.z = abs(p.z) - .45 + .05 * cos(PI + icolor);
    if (p.z > p.y) p.yz = p.zy;


    vec3 q = p;
    float ips = 1.5;
    p.xy *= r2d(-3.14 / 3.);
    p.xz *= r2d(ipitch_6 * ips);
    p.x += cos(sin(ipitch_8*.1)*p.y*8.)*.2;
    p.z += sin(cos(ipitch_5*.2)*p.y*4.)*.2;
    d = smin(d, cyl(p.xz, .05));

    p = q * 1.2;
    p.xy *= r2d(-3.14 / 4.);
    p.xz *= r2d(ipitch_4 * ips *2.);
    p.x += cos(sin((ipitch_6 + ipitch_7)*.1)*p.y*8.)*.2;
    p.z += sin(cos(ipitch_9*.1)*p.y*4.)*.2;
    d = smin(d, cyl(p.xz, .02));

    p = q * 1.2;
    p.xy *= r2d(-3.14 / 2.);
    p.xz *= r2d(ipitch_5 * ips);
    p.x += cos(p.y*8.)*.2;
    p.z += sin(p.y*4.)*.2;
    d = smin(d, cyl(p.xz, .03));

    p = q;
    p.xy *= r2d(3.14 / 2.);
    p.xz *= r2d(ipitch_2 * ips);
    p.x += cos(p.y*8.)*.2;
    p.z += sin(.5 + ipitch_1 * ips + p.y*4.)*.2;
    d = smin(d, cyl(p.xz, .05));

    p = q;
    p.xy *= 1.4;
    p.xy *= r2d(3.14 / 2.);
    p.xz *= r2d(icolor + ipitch_1*ips);
    p.x += cos((ipitch_3)*ips + p.y*8.)*.2;
    p.z += sin(p.y*4.)*.2;
    d = smin(d, cyl(p.xz, .1));

    // trick extracted from balkhan https://www.shadertoy.com/view/4t2yW1
    glow += glow_color * .025 / (.01 + d*d);
    return d;
}

#ifdef ANIMATION_FRACTAL
void main() {
    vec2 uv = -.5 + inUV;
    uv.y *= scene.screenRatio;
    uv *= 3.;
#else
void mainImage( out vec4 oColor, in vec2 fragCoord ) {
    vec2 uv = ( fragCoord - .5*iResolution.xy ) / iResolution.y;
#endif

    uv.xy *= r2d(icolor);
    vec3 ro = vec3(0., 0, 6.), p;
    ro.z -= 2. + imoveFWD * 8.;
    vec3 rd = normalize(vec3(uv, -1));
    p = ro;

    float t = 0.;
    float d = 1e9;
    for (float i = 0.; i < 1.; i += .01) {
        p = ro + rd * t;
        p.z = mod(p.z, 7.);
        d = de(p);
        if (d < .001 || t > 8.) break;
        t += d * .2; // avoid clipping, enhance the glow
    }

    vec3 c = vec3(.5, .05, .4);
    // if (d > .001) c = pow(c, 1.0/vec3(1.2, 1.2, 1.4));
    // if (d > .001) c *= 1.2;
    c.r *= p.y + cos(p.z);
    c += glow * glow_intensity;
    oColor = vec4(c, 1.);
}
