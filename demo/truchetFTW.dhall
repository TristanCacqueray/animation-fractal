let AF = ../package.dhall

in  AF.Scene::{
    , name = "truchetFTW"
    , shaderPath = "./demo/truchetFTW.glsl"
    , vars =
      [ { name = "iTime"
        , uniformName = "var1"
        , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
        }
      ]
    , inputs =
      [ AF.Input.AudioIn
          AF.AudioMod::{
          , dry = { var = "iTIme", kind = AF.ModulationKind.Incr }
          }
      ]
    }
