let AF = ../package.dhall

let Prelude =
      https://prelude.dhall-lang.org/v17.0.0/package.dhall
        sha256:10db3c919c25e9046833df897a8ffe2701dc390fa0893d958c3430524be5a43e

let mkMidiName = \(pitch : Natural) -> "NOTES:${Natural/show (pitch + 1)}"

let mkMidiMods =
      \(count : Natural) ->
      \(base_pitch : Natural) ->
        Prelude.List.generate
          count
          AF.Modulation.Type
          ( \(pitch : Natural) ->
              AF.Modulation::{ var = mkMidiName (base_pitch + pitch) }
          )

let pitchVars =
      \(count : Natural) ->
        Prelude.List.generate
          count
          AF.Variable.Type
          ( \(pitch : Natural) ->
              AF.Variable::{
              , name = mkMidiName pitch
              , uniformName = "n1_${Natural/show (pitch + 1)}"
              , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
              }
          )

let mkScene =
      \(dir : Text) ->
      \(name : Text) ->
      \(midiTimeScale : Double) ->
        AF.Scene::{
        , name
        , midiTimeScale
        , author = "mrange"
        , license = "CC0"
        , url = "https://www.shadertoy.com/view/XcyGRm"
        , shaderPath = "./demo/midiApollian.glsl"
        , medias =
          [ "/home/fedora/reaper/${dir}/${name}.mp3"
          , "/home/fedora/reaper/${dir}/Media/${name}.mid"
          , "/home/fedora/reaper/${dir}/${name}-left.mp3"
          , "/home/fedora/reaper/${dir}/${name}-right.mp3"
          ]
        , vars =
              [ AF.Variable::{
                , name = "moveFWD"
                , uniformName = "var1"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              , AF.Variable::{
                , name = "icolor"
                , uniformName = "var2"
                , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
                }
              ]
            # pitchVars 8
        , inputs =
          [ AF.Input.Midi
              { name = "BASS", mods = mkMidiMods 4 0, cap = 1.0, pulse = False }
          , AF.Input.Midi
              { name = "MELO", mods = mkMidiMods 4 4, cap = 1.0, pulse = False }
          , AF.Input.Midi
              { name = "ETHER"
              , mods = mkMidiMods 2 4
              , cap = 1.0
              , pulse = False
              }
          , AF.Input.Midi
              { name = "WARM", mods = mkMidiMods 4 2, cap = 1.0, pulse = False }
          , AF.Input.AudioFile
              { name = "${name}-left"
              , mod = AF.AudioMod::{
                , gate = 0.0
                , dry = AF.Modulation::{
                  , var = "moveFWD"
                  , kind = AF.ModulationKind.Incr { speed = 1200.0 }
                  }
                }
              }
          , AF.Input.AudioFile
              { name = "${name}-right"
              , mod = AF.AudioMod::{
                , gate = 0.0
                , dry = AF.Modulation::{
                  , var = "icolor"
                  , kind = AF.ModulationKind.Incr { speed = 500.0 }
                  }
                }
              }
          ]
        }

in  mkScene "microChopin" "MazurkaOp17" 1.25
