let AF = ../package.dhall

let Medias = ./medias.dhall

let mkTrack = Medias.mkTrack False

let mkIncr = \(speed : Double) -> AF.ModulationKind.Incr { speed }

let tracks =
      [ mkTrack "ETHER" 1 1 3 (mkIncr 500.0)
      , mkTrack "WATER" 1 4 3 (mkIncr 500.0)
      , mkTrack "DOUX" 1 7 3 (mkIncr 500.0)
      , mkTrack "FROST" 1 10 3 (mkIncr 500.0)
      , mkTrack "BASS" 1 13 3 (mkIncr 500.0)
      , mkTrack "KICK" 4 1 1 (mkIncr 300.0)
      , mkTrack "SNARE" 4 2 1 (mkIncr 300.0)
      , mkTrack "TOM" 4 3 1 (mkIncr 450.0)
      ]

let medias =
      \(dir : Text) ->
      \(name : Text) ->
        [ Medias.mkAudio
            dir
            "${name}-left"
            Medias.AudioModDef::{
            , mod = AF.Modulation::{
              , var = "imoveFWD"
              , kind = AF.ModulationKind.Incr { speed = 900.0 }
              }
            , uniformName = "var2"
            }
        , Medias.mkAudio
            dir
            "${name}-right"
            Medias.AudioModDef::{
            , mod = AF.Modulation::{
              , var = "icolor"
              , kind = AF.ModulationKind.Incr { speed = 900.0 }
              }
            , uniformName = "var3"
            }
        , { path = "${dir}/Media/${name}.mid", kind = Medias.Midi tracks }
        ]

let mkScene =
      \(dir : Text) ->
      \(name : Text) ->
      \(midiTimeScale : Double) ->
        Medias.sceneMedias
          (medias dir name)
          AF.Scene::{
          , name
          , midiTimeScale
          , author = "mrange"
          , license = "CC0"
          , url = "https://www.shadertoy.com/view/MfVSzR"
          , shaderPath = "./demo/midiMandalas.glsl"
          , medias = [ "${dir}/${name}.mp3" ]
          , vars =
            [ AF.Variable::{
              , name = "iTime"
              , uniformName = "var1"
              , controller = AF.Controller.Float { min = 0.0, max = 42.0 }
              }
            ]
          }

in  mkScene "/home/fedora/reaper/microOST" "SoM-always-together" 1.3825
