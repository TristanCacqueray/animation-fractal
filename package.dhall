let Controller = < Float : { min : Double, max : Double } | SciFloat >

let Variable =
      { Type = { name : Text, controller : Controller, uniformName : Text }
      , default = {=}
      }

let ModulationKind =
      < Incr : { speed : Double }
      | ADSR :
          { attack : Natural
          , decay : Natural
          , sustain : Double
          , release : Natural
          }
      >

let Modulation =
      { Type = { var : Text, kind : ModulationKind }
      , default.kind = ModulationKind.Incr { speed = 600.0 }
      }

let FilterKind = < LowPass | BandPass | HighPass >

let Filter =
      { Type = { freq : Double, kind : FilterKind, mod : Modulation.Type } }

let AudioMod =
      { Type =
          { dry : Modulation.Type, gate : Double, filters : List Filter.Type }
      , default = { filters = [] : List Filter.Type, gate = 10.0 }
      }

let Input =
      < AudioIn : AudioMod.Type
      | AudioFile : { name : Text, mod : AudioMod.Type }
      | Midi :
          { name : Text
          , mods : List Modulation.Type
          , cap : Double
          , pulse : Bool
          }
      >

let Scene =
      { Type =
          { name : Text
          , author : Text
          , url : Text
          , license : Text
          , vars : List Variable.Type
          , shaderPath : Text
          , medias : List Text
          , inputs : List Input
          , midiTimeScale : Double
          , keyframes : List Natural
          }
      , default =
        { medias = [] : List Text
        , inputs = [] : List Input
        , keyframes = [] : List Natural
        , midiTimeScale = 1.0
        , vars = [] : List Variable.Type
        }
      }

in  { Controller
    , Variable
    , ModulationKind
    , Modulation
    , FilterKind
    , Filter
    , AudioMod
    , Input
    , Scene
    }
